package com.katarzynakrzemien.finance.categories.fixedoutgoings;

import com.katarzynakrzemien.finance.categories.CategoriesContract;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.FixedOutgoing;

import java.util.Calendar;

public class CategorizedFixedOutgoingsContract {

    public interface View extends CategoriesContract.View {

        void setTimeRemindFixedOutgoingsAndFixedOutgoing(Calendar timeRemindFixedOutgoings, FixedOutgoing fixedOutgoing);

        void setRemindFixedOutgoings(boolean remindFixedOutgoings);

    }

    public interface Presenter extends CategoriesContract.Presenter<View> {
        void fetchTimeRemindFixedOutgoingsAndFixedOutgoing(PreferenceStore preferenceStore, DatabaseStore databaseStore, int idFixedOutgoing);

        void fetchRemindFixedOutgoings(PreferenceStore preferenceStore);

    }
}
