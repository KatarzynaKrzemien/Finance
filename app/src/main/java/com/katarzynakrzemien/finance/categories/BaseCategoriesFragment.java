package com.katarzynakrzemien.finance.categories;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.database.DatabaseQuery;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.models.Category;
import com.katarzynakrzemien.finance.dialog.DialogReaction;
import com.katarzynakrzemien.finance.dialog.DialogTextCreator;
import com.katarzynakrzemien.finance.managing.ManagingActivity;
import com.katarzynakrzemien.finance.utils.Keyboard;
import com.katarzynakrzemien.finance.utils.recycler.ItemClickListener;
import com.katarzynakrzemien.finance.utils.recycler.swipe.ItemSwipeListener;
import com.katarzynakrzemien.finance.utils.recycler.swipe.UndoSnackbar;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class BaseCategoriesFragment<T extends CategoriesContract.Presenter> extends Fragment implements CategoriesContract.View, ItemClickListener, ItemSwipeListener<Category> {

    protected T presenter;
    protected String titleAddDialog;
    protected String emptyItemAddDialog;
    protected int deleteItemInfoText;
    protected List<Category> list = new ArrayList<>();
    protected DatabaseStore databaseStore;
    private CategoriesAdapter adapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        databaseStore = new DatabaseQuery(context);
        presenter.attachView(this);
    }

    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container,
                                          Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_with_add, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        android.view.View view = getView();
        if (view != null) {
            RecyclerView recyclerView = view.findViewById(R.id.recycler_view);

            adapter = new CategoriesAdapter(list);
            adapter.setItemClickListener(this);
            adapter.setItemSwipeListener(this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new SwipeCategoriesItemCallback(adapter, getContext()));
            itemTouchHelper.attachToRecyclerView(recyclerView);
            presenter.fetchList(databaseStore);
            setupAddItemFab(view);
        }
    }

    private void setupAddItemFab(View view) {
        view.findViewById(R.id.fab_add).setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(final android.view.View view) {
                final DialogTextCreator dialogAddItem = new DialogTextCreator(getContext(), titleAddDialog);
                dialogAddItem.setPositiveButton(new DialogReaction() {
                    @Override
                    public void onClick() {
                        String categoryName = dialogAddItem.getText();
                        if (categoryName.length() > 0) {
                            presenter.addItemList(databaseStore, new Category(null, categoryName, false));
                            presenter.fetchList(databaseStore);
                        } else {
                            Snackbar.make(view, emptyItemAddDialog, Snackbar.LENGTH_LONG).show();
                        }
                        Keyboard.hide(getContext());
                    }

                });
                dialogAddItem.showDialog();
                Keyboard.show(getContext());
            }
        });

    }

    @Override
    public void renderList(List<? extends Category> list) {
        this.list.clear();
        this.list.addAll(list);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }

    @Override
    public void onItemClick(View view, int position) {
        Category clickedItem = list.get(position);
        if (clickedItem.getActive()) {
            itemChangeTonNoActive(view, position);
            clickedItem.setActive(false);
            presenter.updateItemList(databaseStore, clickedItem);

        } else {
            itemChangeToActive(view, position);
            clickedItem.setActive(true);
            presenter.updateItemList(databaseStore, clickedItem);
        }
        adapter.notifyItemChanged(position);

    }

    @Override
    public void onItemLongClick(View view, int position) {

    }

    protected abstract void itemChangeToActive(View view, int position);

    protected abstract void itemChangeTonNoActive(View view, int position);

    @Override
    public void deleteItem(Category model) {
        presenter.deleteItemList(databaseStore, model.getId());
        presenter.fetchList(databaseStore);
    }

    @Override
    public void changeItem(Category model) {

    }

    @Override
    public void showSnackbarDeletedItem() {
        UndoSnackbar.show((ManagingActivity) Objects.requireNonNull(getActivity()), deleteItemInfoText, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.undoDelete();
            }
        });
    }

    @Override
    public void showSnackbarChangeItem() {

    }

    @Override
    public void undoDeleteItem(Category model) {
        presenter.addItemList(databaseStore, model);
        presenter.fetchList(databaseStore);
    }

    @Override
    public void undoChangeItem(Category model) {

    }
}
