package com.katarzynakrzemien.finance.categories.spending;

import android.content.Context;
import android.view.View;

import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.categories.BaseCategoriesFragment;
import com.katarzynakrzemien.finance.categories.CategoriesContract;

public class CategorizedSpendingFragment extends BaseCategoriesFragment<CategoriesContract.Presenter> {

    public static final String TAG = "CATEGORIES";

    @Override
    public void onAttach(Context context) {
        this.presenter = new CategorizedSpendingPresenter();
        this.titleAddDialog = getString(R.string.addCategory);
        this.emptyItemAddDialog = getString(R.string.emptyCategory);
        this.deleteItemInfoText = R.string.deletedCategory;
        super.onAttach(context);
    }

    @Override
    protected void itemChangeToActive(View view, int position) {

    }

    @Override
    protected void itemChangeTonNoActive(View view, int position) {

    }
}
