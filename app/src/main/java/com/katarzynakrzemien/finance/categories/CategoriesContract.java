package com.katarzynakrzemien.finance.categories;

import com.katarzynakrzemien.finance.BaseContract;
import com.katarzynakrzemien.finance.BaseRecyclerContract;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.models.Category;

import java.util.List;

public class CategoriesContract {

    public interface View extends BaseContract.View {
        void renderList(List<? extends Category> list);
    }

    public interface Presenter<T extends View> extends BaseContract.Presenter<T> {
        void fetchList(DatabaseStore databaseStore);

        void addItemList(DatabaseStore databaseStore, Category item);

        void updateItemList(DatabaseStore databaseStore, Category item);

        void deleteItemList(DatabaseStore databaseStore, int id);
    }
}
