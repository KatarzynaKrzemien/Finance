package com.katarzynakrzemien.finance.categories.spending;

import com.katarzynakrzemien.finance.categories.CategoriesContract;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.models.Category;
import com.katarzynakrzemien.finance.database.models.CategoryAndIncome;

public class CategorizedSpendingPresenter implements CategoriesContract.Presenter<CategoriesContract.View> {

    protected CategoriesContract.View view;

    @Override
    public void attachView(CategoriesContract.View view) {
        this.view = view;
    }

    @Override
    public void fetchList(DatabaseStore databaseStore) {

        view.renderList(databaseStore.getSpendingsCategories());
    }

    @Override
    public void addItemList(DatabaseStore databaseStore, Category item) {
        databaseStore.insertSpendingCategory(new CategoryAndIncome(item.getId(), item.getName(), item.getActive(), 0.0));

    }

    @Override
    public void updateItemList(DatabaseStore databaseStore, Category item) {
        databaseStore.updateSpendingCategory(new CategoryAndIncome(item.getId(), item.getName(), item.getActive(), null));
    }

    @Override
    public void deleteItemList(DatabaseStore databaseStore, int id) {
        databaseStore.deleteSpendingCategory(id);
    }

    @Override
    public void detachView() {
        this.view = null;
    }
}
