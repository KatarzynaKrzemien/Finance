package com.katarzynakrzemien.finance.categories;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.database.models.Category;
import com.katarzynakrzemien.finance.utils.recycler.ItemClickListener;
import com.katarzynakrzemien.finance.utils.recycler.swipe.ItemSwipeListener;

import java.util.List;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoriesViewHolder> {

    private List<Category> categoriesList;
    private ItemClickListener itemClickListener;
    private ItemSwipeListener<Category> itemSwipeListener;
    private Category recentlyChangedItem;


    CategoriesAdapter(List<Category> categoriesList) {
        this.categoriesList = categoriesList;
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_text, parent, false);
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CategoriesViewHolder holder, final int position) {
        final Category category = categoriesList.get(position);
        final String name = category.getName();
        holder.name.setText(name);
        if (category.getActive()) {
            holder.itemView.setBackgroundResource(R.color.active);
        } else {
            holder.itemView.setBackgroundResource(R.color.noactive);
        }
    }

    @Override
    public int getItemCount() {
        return categoriesList.size();
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    void setItemSwipeListener(ItemSwipeListener<Category> itemSwipeListener) {
        this.itemSwipeListener = itemSwipeListener;
    }

    void deleteItem(int position) {
        recentlyChangedItem = categoriesList.get(position);
        itemSwipeListener.deleteItem(recentlyChangedItem);
        itemSwipeListener.showSnackbarDeletedItem();
    }

    void undoDelete() {
        itemSwipeListener.undoDeleteItem(recentlyChangedItem);
    }

    public class CategoriesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name;

        CategoriesViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            name.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (itemClickListener != null) {
                itemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }
}