package com.katarzynakrzemien.finance.categories.fixedoutgoings;

import android.content.Context;
import android.view.View;

import com.katarzynakrzemien.finance.App;
import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.categories.BaseCategoriesFragment;
import com.katarzynakrzemien.finance.database.PreferenceFromDatabase;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.FixedOutgoing;
import com.katarzynakrzemien.finance.fixedoutgoings.reminder.FixedOutgoingsWorkerManager;
import com.katarzynakrzemien.finance.utils.formatter.amount.AmountFormatter;

import java.util.Calendar;

public class CategorizedFixedOutgoingsFragment extends BaseCategoriesFragment<CategorizedFixedOutgoingsContract.Presenter> implements CategorizedFixedOutgoingsContract.View {

    public static final String TAG = "FIXED_OUTGOINGS_LIST";
    private FixedOutgoingsWorkerManager fixedOutgoingsWorkerManager = FixedOutgoingsWorkerManager.getInstance();
    private PreferenceStore preferenceStore;
    private int idFixedOutgoing;
    private AmountFormatter amountFormatter;

    @Override
    public void onAttach(Context context) {
        this.presenter = new CategorizedFixedOutgoingsSpendingPresenter();
        this.titleAddDialog = getString(R.string.addFixedOutgoings);
        this.emptyItemAddDialog = getString(R.string.emptyFixedOutgoings);
        this.deleteItemInfoText = R.string.deletedFixedOutgoing;
        super.onAttach(context);
        preferenceStore = new PreferenceFromDatabase(context);
        amountFormatter = ((App) context.getApplicationContext()).amountFormatter;
    }

    @Override
    protected void itemChangeToActive(View view, int position) {
        idFixedOutgoing = list.get(position).getId();
        presenter.fetchRemindFixedOutgoings(preferenceStore);
    }

    @Override
    protected void itemChangeTonNoActive(View view, int position) {
        fixedOutgoingsWorkerManager.cancel(list.get(position).getId());
    }

    @Override
    public void setTimeRemindFixedOutgoingsAndFixedOutgoing(Calendar timeRemindFixedOutgoings, FixedOutgoing fixedOutgoing) {
        if (!fixedOutgoing.getIsPaid() && fixedOutgoing.getDueDate() != null) {
            fixedOutgoingsWorkerManager.create(fixedOutgoing.getId(),
                    fixedOutgoing.getDueDate(),
                    timeRemindFixedOutgoings,
                    fixedOutgoing.getName(),
                    amountFormatter.format(fixedOutgoing.getOutcome()));
        }
    }

    @Override
    public void setRemindFixedOutgoings(boolean remindFixedOutgoings) {
        if (remindFixedOutgoings) {
            presenter.fetchTimeRemindFixedOutgoingsAndFixedOutgoing(preferenceStore, databaseStore, idFixedOutgoing);
        }
    }
}
