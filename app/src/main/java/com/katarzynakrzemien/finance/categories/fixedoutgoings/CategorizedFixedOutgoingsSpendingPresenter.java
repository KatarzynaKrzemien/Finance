package com.katarzynakrzemien.finance.categories.fixedoutgoings;

import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.Category;
import com.katarzynakrzemien.finance.database.models.FixedOutgoing;

import java.util.List;

public class CategorizedFixedOutgoingsSpendingPresenter implements CategorizedFixedOutgoingsContract.Presenter {
    protected CategorizedFixedOutgoingsContract.View view;

    @Override
    public void attachView(CategorizedFixedOutgoingsContract.View view) {
        this.view = view;
    }

    @Override
    public void fetchList(DatabaseStore databaseStore) {
        view.renderList(databaseStore.getFixedOutgoingsCategories());
    }

    @Override
    public void addItemList(DatabaseStore databaseStore, Category item) {
        databaseStore.insertFixedOutgoingCategory(item);
    }

    @Override
    public void deleteItemList(DatabaseStore databaseStore, int id) {
        databaseStore.deleteFixedOutgoingCategory(id);
        deleteFixedOutgoings(databaseStore, id);
    }

    private void deleteFixedOutgoings(DatabaseStore databaseStore, int idFixedOutgoingCategory) {
        List<FixedOutgoing> fixedOutgoingList = databaseStore.getFixedOutgoings();
        for (int i = 0; i < fixedOutgoingList.size(); i++) {
            if (fixedOutgoingList.get(i).getIdCategoryOfFO() == idFixedOutgoingCategory) {
                databaseStore.deleteFixedOutgoing(fixedOutgoingList.get(i).getId());
            }
        }
    }

    @Override
    public void updateItemList(DatabaseStore databaseStore, Category item) {
        databaseStore.updateFixedOutgoingCategory(item);
        if (item.getActive()) {
            insertFixedOutgoingIfNotExist(databaseStore, item);
        }
    }

    private void insertFixedOutgoingIfNotExist(DatabaseStore databaseStore, Category item) {
        boolean isFixedOutgoingExist = false;
        List<FixedOutgoing> fixedOutgoingList = databaseStore.getFixedOutgoings();
        for (int i = 0; i < fixedOutgoingList.size(); i++) {
            if (fixedOutgoingList.get(i).getIdCategoryOfFO() == item.getId()) {
                isFixedOutgoingExist = true;
            }
        }
        if (!isFixedOutgoingExist) {
            databaseStore.insertFixedOutgoing(new FixedOutgoing(null, item.getId(), item.getName(), 0.0, null, null, false));
        }
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void fetchTimeRemindFixedOutgoingsAndFixedOutgoing(PreferenceStore preferenceStore, DatabaseStore databaseStore, int idFixedOutgoing) {
        view.setTimeRemindFixedOutgoingsAndFixedOutgoing(preferenceStore.getRemindingTimeFixedOutgoings(),
                databaseStore.getFixedOutgoing(idFixedOutgoing));
    }

    @Override
    public void fetchRemindFixedOutgoings(PreferenceStore preferenceStore) {
        view.setRemindFixedOutgoings(preferenceStore.getRemindingFixedOutgoings());
    }
}
