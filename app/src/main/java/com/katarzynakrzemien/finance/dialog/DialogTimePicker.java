package com.katarzynakrzemien.finance.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TimePicker;

import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.utils.formatter.date.DateFormatPattern;
import com.katarzynakrzemien.finance.utils.formatter.date.DateFormatter;
import com.katarzynakrzemien.finance.utils.formatter.date.DateManger;
import com.katarzynakrzemien.finance.utils.formatter.date.SimpleDateFormatter;

import java.util.Calendar;

public class DialogTimePicker extends DialogCreator {

    private View view;
    private DateFormatter timeFormatter = new SimpleDateFormatter(DateFormatPattern.TIME);

    public DialogTimePicker(Context context, String title) {
        super(context, title, R.drawable.ic_alarm);
        this.view = LayoutInflater.from(this.context).inflate(R.layout.dialog_time_picker, null);
        this.dialog.setView(this.view);
        setNegativeButton();
    }

    public Integer getHour() {
        return getTimePicker().getCurrentHour();
    }

    public void setHour(int hour) {
        getTimePicker().setCurrentHour(hour);
    }

    public Integer getMinute() {
        return getTimePicker().getCurrentMinute();
    }

    public void setMinute(int minute) {
        getTimePicker().setCurrentMinute(minute);
    }

    public String getTextTime() {
        return timeFormatter.formatToString(getCalendarTime());
    }

    public Calendar getCalendarTime() {
        return DateManger.createTimeCalendar(getHour(), getMinute(), 0);
    }

    public void setTime(Calendar time) {
        getTimePicker().setCurrentHour(time.get(Calendar.HOUR));
        getTimePicker().setCurrentMinute(time.get(Calendar.MINUTE));
    }


    private TimePicker getTimePicker() {
        return (TimePicker) this.view.findViewById(R.id.dialog_time_picker);
    }


}
