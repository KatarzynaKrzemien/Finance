package com.katarzynakrzemien.finance.dialog;


import android.content.Context;
import com.google.android.material.textfield.TextInputEditText;
import android.view.LayoutInflater;
import android.view.View;

import com.katarzynakrzemien.finance.utils.Keyboard;
import com.katarzynakrzemien.finance.R;

public class DialogTextCreator extends DialogCreator {

    private View view;

    public DialogTextCreator(final Context context, String title) {
        super(context, title, context.getString(R.string.enterName), R.drawable.ic_add);
        this.view = LayoutInflater.from(this.context).inflate(R.layout.dialog_edit_text, null);
        this.dialog.setView(this.view);
        getEditText().requestFocus();
        setNegativeButton(new DialogReaction() {
            @Override
            public void onClick() {
                Keyboard.hide(context);
            }
        });
    }

    public String getText() {
        return getEditText().getText().toString();
    }

    private TextInputEditText getEditText() {
        return (TextInputEditText) this.view.findViewById(R.id.edit);
    }


}
