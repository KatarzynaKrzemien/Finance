package com.katarzynakrzemien.finance.dialog;


import android.content.Context;
import com.google.android.material.textfield.TextInputEditText;
import android.view.LayoutInflater;
import android.view.View;

import com.katarzynakrzemien.finance.utils.Keyboard;
import com.katarzynakrzemien.finance.R;

public class DialogQuotaCreator extends DialogCreator {
    protected View view;

    public DialogQuotaCreator(final Context context, String title) {
        super(context, title, context.getString(R.string.enterQuota), R.drawable.ic_money);
        this.view = LayoutInflater.from(context).inflate(R.layout.dialog_edit_text_quota, null);
        this.dialog.setView(this.view);
        getDialogEditText().requestFocus();
        setNegativeButton(new DialogReaction() {
            @Override
            public void onClick() {
                Keyboard.hide(context);
            }
        });
    }

    public View getDialogLayout() {
        return this.view;
    }

    public TextInputEditText getDialogEditText() {
        return (TextInputEditText) this.view.findViewById(R.id.edit);
    }
}