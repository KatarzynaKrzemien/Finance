package com.katarzynakrzemien.finance.dialog;

import android.content.Context;
import android.content.DialogInterface;

import com.katarzynakrzemien.finance.R;

public class DialogQuestionCreator extends DialogCreator {

    public DialogQuestionCreator(final Context context, String title, String message) {
        super(context, title, message, R.drawable.ic_help);
        setNegativeButton(R.string.no);
    }

    @Override
    public void setPositiveButton(DialogReaction positiveReaction) {
        super.setPositiveButton(R.string.yes, positiveReaction);
    }
}