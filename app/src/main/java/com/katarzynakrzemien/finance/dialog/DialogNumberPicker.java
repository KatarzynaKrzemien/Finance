package com.katarzynakrzemien.finance.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

import com.katarzynakrzemien.finance.R;

public class DialogNumberPicker extends DialogCreator {

    private View view;

    public DialogNumberPicker(Context context, String title, String message, Integer idIcon, int minValue, int maxValue) {
        super(context, title, message, idIcon);
        this.view = LayoutInflater.from(this.context).inflate(R.layout.dialog_number_picker, null);
        this.dialog.setView(this.view);
        setNegativeButton();
        getNumberPicker().setMinValue(minValue);
        getNumberPicker().setMaxValue(maxValue);
    }

    private NumberPicker getNumberPicker() {
        return (NumberPicker) this.view.findViewById(R.id.dialog_number_picker);
    }

    public int getNumber() {
        return getNumberPicker().getValue();
    }

    public void setNumber(int number) {
        getNumberPicker().setValue(number);
    }
}
