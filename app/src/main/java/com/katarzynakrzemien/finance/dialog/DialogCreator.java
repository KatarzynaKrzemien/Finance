package com.katarzynakrzemien.finance.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.katarzynakrzemien.finance.R;

public class DialogCreator {
    protected Context context;
    protected String title;
    private String message;
    private Integer idIcon;
    protected AlertDialog.Builder dialog;

    public DialogCreator(Context context, String title, String message, Integer idIcon) {

        this.context = context;
        this.title = title;
        this.message = message;
        this.idIcon = idIcon;
        this.dialog = new AlertDialog.Builder(context).setTitle(this.title).setMessage(this.message).setIcon(this.idIcon);
    }

    public DialogCreator(Context context, String title, int idIcon) {

        this.context = context;
        this.title = title;
        this.message = null;
        this.idIcon = idIcon;
        this.dialog = new AlertDialog.Builder(context).setTitle(this.title).setIcon(this.idIcon);


    }

    public DialogCreator(Context context, String title) {

        this.context = context;
        this.title = title;
        this.message = null;
        this.idIcon = null;
        this.dialog = new AlertDialog.Builder(context).setTitle(this.title).setIcon(this.idIcon);

    }

    public void setExitButton() {
        this.dialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                cancelDialog(dialog);
            }

        });
    }

    public void setPositiveButton(final DialogReaction positiveReaction) {
        this.dialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                positiveReaction.onClick();
            }

        });
    }

    public void setPositiveButton(int buttonText, final DialogReaction positiveReaction) {
        this.dialog.setPositiveButton(buttonText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                positiveReaction.onClick();
            }

        });
    }

    public void setNeutralButton(int buttonText, final DialogReaction neutralReaction) {
        this.dialog.setNeutralButton(buttonText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                neutralReaction.onClick();
            }

        });
    }

    public void setNegativeButton(final DialogReaction negativeReaction) {
        this.dialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                negativeReaction.onClick();
                cancelDialog(dialog);
            }

        });
    }

    public void setNegativeButton(int buttonText) {
        this.dialog.setNegativeButton(buttonText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                cancelDialog(dialog);
            }

        });
    }

    public void setNegativeButton(int buttonText, final DialogReaction negativeReaction) {
        this.dialog.setNegativeButton(buttonText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                negativeReaction.onClick();
                cancelDialog(dialog);
            }

        });
    }

    public void setNegativeButton() {
        this.dialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                cancelDialog(dialog);
            }

        });
    }

    public void showDialog() {
        this.dialog.show();
    }

    public void cancelDialog(DialogInterface dialog) {
        dialog.cancel();
    }

}
