package com.katarzynakrzemien.finance.dialog;

public interface DialogReaction {

    public void onClick();
}
