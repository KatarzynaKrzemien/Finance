package com.katarzynakrzemien.finance.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.katarzynakrzemien.finance.R;

public class DialogLargeAndSmallText extends DialogCreator {

    private View view;

    public DialogLargeAndSmallText(final Context context, String title, int idIcon) {
        super(context, title, idIcon);
        this.view = LayoutInflater.from(this.context).inflate(R.layout.dialog_two_text, null);
        this.dialog.setView(this.view);
    }

    public View getLayout() {
        return this.view;
    }

    public void setLargeTextView(String text) {
        ((TextView) this.view.findViewById(R.id.large_text)).setText(text);
    }

    public void setSmallTextView(String text) {
        ((TextView) this.view.findViewById(R.id.small_text)).setText(text);
    }
}
