package com.katarzynakrzemien.finance.dialog;

import android.content.Context;
import androidx.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;

import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.themes.Themes;

public class DialogThemePicker extends DialogCreator implements View.OnClickListener {
    private int DARK_PREF = Themes.DARK.getIdPref();
    private int LIGHT_PREF = Themes.LIGHT.getIdPref();
    private int DARK_BLUE_PREF = Themes.DARK_BLUE.getIdPref();

    private int idImageView;

    private View view;

    public DialogThemePicker(final Context context, String title) {
        super(context, title, R.drawable.ic_style);
        this.view = LayoutInflater.from(this.context).inflate(R.layout.dialog_color_picker, null);
        this.dialog.setView(this.view);
        this.idImageView = getIdImageView(PreferenceManager.getDefaultSharedPreferences(context).
                getInt("style", DARK_PREF));
        setCheckItem(idImageView);
        setButtonsListener();
        setNegativeButton();
    }

    private void setCheckItem(int idItem) {
        view.findViewById(idImageView).setBackground(context.getDrawable(R.drawable.circle_background));
        view.findViewById(idItem).setBackground(context.getDrawable(R.drawable.circle_background_check));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.darkStyleLayout:
                setCheckItem(R.id.darkStyle);
                idImageView = R.id.darkStyle;
                break;
            case R.id.lightStyleLayout:
                setCheckItem(R.id.lightStyle);
                idImageView = R.id.lightStyle;
                break;
            case R.id.darkBlueStyleLayout:
                setCheckItem(R.id.darkBlueStyle);
                idImageView = R.id.darkBlueStyle;
                break;
        }
    }

    private int getIdImageView(int idPref) {

        if (LIGHT_PREF == idPref) {
            return R.id.lightStyle;
        } else if (DARK_BLUE_PREF == idPref) {
            return R.id.darkBlueStyle;
        } else {
            return R.id.darkStyle;
        }

    }

    public Themes getThemes() {

        if (R.id.lightStyle == idImageView) {
            return Themes.LIGHT;
        } else if (R.id.darkBlueStyle == idImageView) {
            return Themes.DARK_BLUE;
        } else {
            return Themes.DARK;
        }

    }

    private void setButtonsListener() {
        view.findViewById(R.id.darkStyleLayout).setOnClickListener(this);
        view.findViewById(R.id.lightStyleLayout).setOnClickListener(this);
        view.findViewById(R.id.darkBlueStyleLayout).setOnClickListener(this);
    }
}
