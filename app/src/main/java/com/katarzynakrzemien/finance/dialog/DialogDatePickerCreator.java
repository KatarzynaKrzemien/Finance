package com.katarzynakrzemien.finance.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;

import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.utils.formatter.date.DateManger;

import java.util.Calendar;

public class DialogDatePickerCreator extends DialogCreator {
    protected View view;

    public DialogDatePickerCreator(final Context context, String title) {
        super(context, title, R.drawable.ic_new_month);
        this.view = LayoutInflater.from(context).inflate(R.layout.dialog_date_picker, null);
        this.dialog.setView(this.view);
        setNegativeButton();
    }

    public DatePicker getDatePicker() {
        return (DatePicker) this.view.findViewById(R.id.date_picker);
    }

    public int getYear() {
        return getDatePicker().getYear();
    }

    public int getMonth() {
        return getDatePicker().getMonth() + 1;
    }

    public int getDayOfMonth() {
        return getDatePicker().getDayOfMonth();
    }

    public Calendar getDate() {
        return DateManger.createDateCalendar(getDayOfMonth(), getMonth(), getYear());
    }
}