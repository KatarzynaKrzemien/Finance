package com.katarzynakrzemien.finance.utils.formatter.date;

public enum DateFormatPattern {

    DMYTIME("dd.MM.yyyy HH:MM:SS"),
    TIME("HH:MM:SS"),
    DATE_WITH_DAY_OF_WEEK("EEE  |  dd MMM yyyy"),
    DAY_MONTH("dd.MM"),
    DAY_MONTH_YEAR("dd.MM.yyyy"),
    SQLITE("yyyy-MM-dd");

    private String pattern;

    DateFormatPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getPattern() {
        return pattern;
    }
}
