package com.katarzynakrzemien.finance.utils.recycler.swipe;

import android.view.View;

import com.google.android.material.snackbar.Snackbar;
import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.managing.ManagingActivity;

public class UndoSnackbar {
    public static void show(ManagingActivity activity, int infoText, View.OnClickListener onClickListener) {
        View view = activity.findViewById(R.id.nav_host_fragment);
        Snackbar snackbar = Snackbar.make(view, infoText, Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.undo, onClickListener);
        snackbar.show();
    }
}
