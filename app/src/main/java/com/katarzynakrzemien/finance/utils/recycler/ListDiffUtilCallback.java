package com.katarzynakrzemien.finance.utils.recycler;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

import com.katarzynakrzemien.finance.database.models.BaseModel;

import java.util.List;

public class ListDiffUtilCallback<T extends BaseModel> extends DiffUtil.Callback {
    List<T> oldList;
    List<T> newList;

    public ListDiffUtilCallback(List<T> newList, List<T> oldList) {
        this.newList = newList;
        this.oldList = oldList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).getId() == newList.get(newItemPosition).getId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).equals(newList.get(newItemPosition));
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }

}
