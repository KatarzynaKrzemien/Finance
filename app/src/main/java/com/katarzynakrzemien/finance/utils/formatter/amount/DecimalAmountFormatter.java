package com.katarzynakrzemien.finance.utils.formatter.amount;

import java.text.DecimalFormat;

public class DecimalAmountFormatter implements AmountFormatter {
    private DecimalFormat decimalformat = new DecimalFormat("0.00");

    @Override
    public String format(double amount) {
        return decimalformat.format(amount);
    }
}
