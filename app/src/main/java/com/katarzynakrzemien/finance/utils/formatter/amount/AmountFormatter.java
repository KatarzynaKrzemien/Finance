package com.katarzynakrzemien.finance.utils.formatter.amount;

public interface AmountFormatter {
    String format(double amount);
}
