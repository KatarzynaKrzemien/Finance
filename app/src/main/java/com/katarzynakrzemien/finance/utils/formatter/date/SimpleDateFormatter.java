package com.katarzynakrzemien.finance.utils.formatter.date;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class SimpleDateFormatter implements DateFormatter {

    DateFormat dateFormat;

    public SimpleDateFormatter(DateFormatPattern formatPattern) {
        dateFormat = new SimpleDateFormat(formatPattern.getPattern(), Locale.getDefault());
    }

    @Override
    public String reformat(String date, DateFormatter oldFormat) {
        return formatToString(oldFormat.parseToCalendar(date));
    }

    @Override
    public String formatToString(Calendar date) {
        return dateFormat.format(date.getTime());
    }

    @Override
    public Calendar parseToCalendar(String date) {
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(dateFormat.parse(date));
        } catch (ParseException e) {
            calendar.getTime();
        }
        return calendar;
    }

    @Override
    public String createFormattedDate(int day, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        return formatToString(calendar);
    }


    @Override
    public String createFormattedDate(int day, int month, int year, int hour, int minute) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day, hour, minute);
        return formatToString(calendar);
    }

}
