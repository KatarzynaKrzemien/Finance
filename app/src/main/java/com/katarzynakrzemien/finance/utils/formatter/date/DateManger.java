package com.katarzynakrzemien.finance.utils.formatter.date;

import com.katarzynakrzemien.finance.R;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class DateManger {

    public static boolean compareDates(Calendar c1, Calendar c2) {
        return (c1.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH)
                && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH)
                && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR));
    }

    public static Calendar createTimeCalendar(int hour, int minute, int second) {
        return createDateTimeCalendar(0, 0, 0, hour, minute, second);
    }

    public static Calendar createDateTimeCalendar(int day, int month, int year, int hour, int minute, int second) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day, hour, minute, second);
        return calendar;
    }

    public static Calendar createDateCalendar(int day, int month, int year) {
        return createDateTimeCalendar(day, month, year, 0, 0, 0);
    }

    public static int getDayTitleStringId(long days) {
        if (days == 1) {
            return R.string.day;
        }
        return R.string.days;
    }

    public static long getRemainingDaysToNextMonth(int nextPeriodStartDay) {
        Calendar today = Calendar.getInstance();
        Calendar nextPeriodEndDate = Calendar.getInstance();
        nextPeriodEndDate.set(Calendar.DAY_OF_MONTH, nextPeriodStartDay);
        if (nextPeriodStartDay <= today.get(Calendar.DAY_OF_MONTH)) {
            nextPeriodEndDate.add(Calendar.MONTH, 1);
            nextPeriodEndDate.add(Calendar.DAY_OF_MONTH, -1);
        }
        long timeToNextMonth = nextPeriodEndDate.getTimeInMillis() - today.getTimeInMillis();
        return TimeUnit.DAYS.convert(timeToNextMonth, TimeUnit.MILLISECONDS);
    }
}
