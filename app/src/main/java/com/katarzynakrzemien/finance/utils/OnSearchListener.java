package com.katarzynakrzemien.finance.utils;

public interface OnSearchListener {
    void setFilterText(String filterText);
}