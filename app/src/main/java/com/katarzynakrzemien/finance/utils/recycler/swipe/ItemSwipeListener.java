package com.katarzynakrzemien.finance.utils.recycler.swipe;

public interface ItemSwipeListener<T> {

    void deleteItem(T model);

    void changeItem(T model);

    void showSnackbarDeletedItem();

    void showSnackbarChangeItem();

    void undoDeleteItem(T model);

    void undoChangeItem(T model);
}
