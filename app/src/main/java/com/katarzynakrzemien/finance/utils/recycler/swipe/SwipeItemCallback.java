package com.katarzynakrzemien.finance.utils.recycler.swipe;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.katarzynakrzemien.finance.R;

public abstract class SwipeItemCallback extends ItemTouchHelper.SimpleCallback {

    protected ColorDrawable backgroundRight;
    protected Drawable rightIcon;
    protected ColorDrawable backgroundLeft;
    protected Drawable leftIcon;
    private int backgroundCornerOffset = 20;

    public SwipeItemCallback(Context context) {
        super(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
        backgroundLeft = new ColorDrawable(ContextCompat.getColor(context, R.color.delete));
        leftIcon = ContextCompat.getDrawable(context, R.drawable.ic_delete);
        backgroundRight = backgroundLeft;
        rightIcon = leftIcon;
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        View itemView = viewHolder.itemView;
        if (dX > 0) {
            setupIconAndBackgroundOnSwipingToRight(itemView, dX);
            backgroundRight.draw(c);
            rightIcon.draw(c);

        } else if (dX < 0) {
            setupIconAndBackgroundOnSwipingToLeft(itemView, dX);
            backgroundLeft.draw(c);
            leftIcon.draw(c);
        } else {
            setBackgroundUnSwiped();
        }
    }

    private void setupIconAndBackgroundOnSwipingToLeft(View itemView, float dX) {
        int iconMargin = (itemView.getHeight() - leftIcon.getIntrinsicHeight()) / 2;
        int iconTop = itemView.getTop() + (itemView.getHeight() - leftIcon.getIntrinsicHeight()) / 2;
        int iconBottom = iconTop + leftIcon.getIntrinsicHeight();
        int iconLeft = itemView.getRight() - iconMargin - leftIcon.getIntrinsicWidth();
        int iconRight = itemView.getRight() - iconMargin;
        leftIcon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
        backgroundLeft.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset,
                itemView.getTop(), itemView.getRight(), itemView.getBottom());
    }

    private void setupIconAndBackgroundOnSwipingToRight(View itemView, float dX) {
        int iconMargin = (itemView.getHeight() - rightIcon.getIntrinsicHeight()) / 2;
        int iconTop = itemView.getTop() + (itemView.getHeight() - rightIcon.getIntrinsicHeight()) / 2;
        int iconBottom = iconTop + rightIcon.getIntrinsicHeight();
        int iconLeft = itemView.getLeft() + iconMargin;
        int iconRight = iconLeft + rightIcon.getIntrinsicWidth();
        rightIcon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
        backgroundRight.setBounds(itemView.getLeft(), itemView.getTop(),
                itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
    }

    private void setBackgroundUnSwiped() {
        backgroundRight.setBounds(0, 0, 0, 0);
        backgroundLeft.setBounds(0, 0, 0, 0);
    }

}
