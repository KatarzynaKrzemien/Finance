package com.katarzynakrzemien.finance.utils.formatter.date;

import java.util.Calendar;

public interface DateFormatter {

    String reformat(String date, DateFormatter oldFormat);

    String formatToString(Calendar date);

    Calendar parseToCalendar(String date);

    String createFormattedDate(int day, int month, int year);

    String createFormattedDate(int day, int month, int year, int hour, int minute);
}