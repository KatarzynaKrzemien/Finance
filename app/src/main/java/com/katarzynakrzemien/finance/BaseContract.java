package com.katarzynakrzemien.finance;


public class BaseContract {

    public interface View {
    }

    public interface Presenter<T extends BaseContract.View> {

        void attachView(T view);

        void detachView();


    }
}
