package com.katarzynakrzemien.finance.charts.archive.spending;

import com.github.mikephil.charting.data.BarEntry;
import com.katarzynakrzemien.finance.charts.archive.BaseArchiveChartContract;

import java.util.List;

public class ArchiveSpendingChartContract {

    public interface View extends BaseArchiveChartContract.View {
        void renderChartListsFromYear(List<BarEntry> entriesList, List<String> labelsList);
    }

    public interface Presenter extends BaseArchiveChartContract.Presenter<View> {
    }
}
