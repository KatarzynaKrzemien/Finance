package com.katarzynakrzemien.finance.charts.current;

import com.github.mikephil.charting.data.PieEntry;
import com.katarzynakrzemien.finance.BaseContract;
import com.katarzynakrzemien.finance.database.models.variants.Variants;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceStore;

import java.util.List;

public class CurrentChartContract {

    public interface View extends BaseContract.View {
        void renderChartsLists(List<PieEntry> entriesList, List<String> labelsList);

        void setVariant(Variants variant);

        void setSalary(double salary);

        void setAvailableFunds(double availableFunds);

        void setSpending(double spending);

    }

    public interface Presenter extends BaseContract.Presenter<View> {
        void fetchChartsLists(DatabaseStore databaseStore);

        void fetchVariant(PreferenceStore preferenceStore);

        void fetchSalary(DatabaseStore databaseStore);

        void fetchAvailableFunds(DatabaseStore databaseStore);

        void fetchSpending(DatabaseStore databaseStore);
    }
}
