package com.katarzynakrzemien.finance.charts.annual.fixedoutgoing;

import android.content.Context;

import com.katarzynakrzemien.finance.charts.annual.ArchiveAnnualChartFragment;

public class ArchiveAnnualFixedOutgoingsChartFragment extends ArchiveAnnualChartFragment {
    @Override
    public void onAttach(Context context) {
        presenter = new ArchiveAnnualFixedOutgoingsChartPresenter();
        super.onAttach(context);
    }
}
