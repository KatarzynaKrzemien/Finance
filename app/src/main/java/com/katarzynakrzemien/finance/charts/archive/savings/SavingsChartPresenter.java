package com.katarzynakrzemien.finance.charts.archive.savings;

import com.github.mikephil.charting.data.Entry;
import com.katarzynakrzemien.finance.charts.archive.BaseArchiveChartPresenter;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.models.Period;

import java.util.ArrayList;
import java.util.List;

public class SavingsChartPresenter extends BaseArchiveChartPresenter<SavingsChartContract.View> implements SavingsChartContract.Presenter {

    @Override
    public void fetchChartListsFromYear(DatabaseStore databaseStore, String year) {
        List<Period> periodList = databaseStore.getArchivePeriods();
        List<Entry> entryList = new ArrayList<>();
        List<String> labelsList = new ArrayList<>();
        for (int i = 0; i < periodList.size(); i++) {
            Period period = periodList.get(i);
            if (period.getStartYear() == Integer.parseInt(year)) {
                entryList.add(new Entry(i, (float) period.getSavings()));
                labelsList.add(i,getLabel(period));
            }
        }
        view.renderChartListsFromYear(entryList, labelsList);
    }
}