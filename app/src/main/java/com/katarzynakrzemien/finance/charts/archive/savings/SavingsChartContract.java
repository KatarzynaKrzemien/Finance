package com.katarzynakrzemien.finance.charts.archive.savings;

import com.github.mikephil.charting.data.Entry;
import com.katarzynakrzemien.finance.charts.archive.BaseArchiveChartContract;

import java.util.List;

public class SavingsChartContract {

    public interface View extends BaseArchiveChartContract.View {
        void renderChartListsFromYear(List<Entry> entriesList, List<String> labelsList);
    }

    public interface Presenter extends BaseArchiveChartContract.Presenter<View> {
    }
}
