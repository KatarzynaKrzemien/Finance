package com.katarzynakrzemien.finance.charts.archive.savings;


import android.content.Context;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.katarzynakrzemien.finance.App;
import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.database.DatabaseQuery;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.utils.formatter.amount.AmountFormatter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SavingsChartsFragment extends Fragment implements SavingsChartContract.View {

    private TextView dateText;
    private TextView savingsText;
    private ArrayList<Entry> entries = new ArrayList<>();
    private ArrayList<String> yearsList = new ArrayList<>();
    private List<String> labelsList = new ArrayList<>();
    private LineChart savingsChart;
    private Spinner spinnerYears;
    private TypedValue typedValue = new TypedValue();
    private AmountFormatter amountFormatter;
    private DatabaseStore databaseStore;
    private SavingsChartContract.Presenter presenter = new SavingsChartPresenter();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        amountFormatter = ((App) Objects.requireNonNull(getActivity()).getApplicationContext()).amountFormatter;
        databaseStore = new DatabaseQuery(context);
        presenter.attachView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_savings_charts, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        View view = getView();
        if (view != null) {
            savingsChart = view.findViewById(R.id.savings_line_chart);
            dateText = view.findViewById(R.id.text_date_savings);
            savingsText = view.findViewById(R.id.text_savings);
            spinnerYears = view.findViewById(R.id.spinnerYears);

            prepareSpinnerYears();
        }
    }

    private void prepareSpinnerYears() {
        presenter.fetchYearsList(databaseStore);
        ArrayAdapter<String> adapterYears = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, yearsList);
        spinnerYears.setAdapter(adapterYears);
        spinnerYears.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                presenter.fetchChartListsFromYear(databaseStore, spinnerYears.getSelectedItem().toString());
                prepareChart();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }


    private void checkData() {
        savingsChart.clear();
        savingsText.setText(R.string.empty);
        dateText.setText(R.string.empty);

        if (!entries.isEmpty()) {
            LineDataSet salaryDataSet = new LineDataSet(entries, getResources().getString(R.string.savings));
            salaryDataSet.setCircleColors(ColorTemplate.PASTEL_COLORS);
            salaryDataSet.setColors(ColorTemplate.PASTEL_COLORS);

            ArrayList<ILineDataSet> lines = new ArrayList<>();
            lines.add(salaryDataSet);

            LineData data = new LineData(lines);
            if (getContext().getTheme().resolveAttribute(android.R.attr.textColorPrimary, typedValue, true)) {
                data.setValueTextColor(typedValue.data);
            }
            savingsChart.setData(data);
            prepareAxis();

        }
    }

    private void prepareChart() {
        savingsChart.getLegend().setEnabled(false);
        savingsChart.animateY(2000);
        if (getContext().getTheme().resolveAttribute(android.R.attr.textColorPrimary, typedValue, true)) {
            savingsChart.getLegend().setTextColor(typedValue.data);
        }
        savingsChart.getDescription().setEnabled(false);
        savingsChart.setVisibleXRangeMaximum(24);
        savingsChart.setVisibleXRangeMinimum(0);
        savingsChart.setContentDescription(getString(R.string.savings));

        savingsChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, Highlight h) {
                dateText.setText(labelsList.get((int) e.getX()));
                savingsText.setText(amountFormatter.format((double) e.getY()));
            }

            @Override
            public void onNothingSelected() {
                savingsText.setText(R.string.empty);
                dateText.setText(R.string.empty);
            }
        });
        savingsChart.invalidate();
    }

    private void prepareAxis() {
        YAxis yl = savingsChart.getAxisLeft();
        YAxis yR = savingsChart.getAxisRight();
        if (getContext().getTheme().resolveAttribute(android.R.attr.textColorPrimary, typedValue, true)) {
            yl.setTextColor(typedValue.data);
            yR.setTextColor(typedValue.data);
        }
        savingsChart.getXAxis().setEnabled(false);
    }

    @Override
    public void renderYearsList(List<String> yearsList) {
        this.yearsList.clear();
        this.yearsList.addAll(yearsList);

    }

    @Override
    public void renderChartListsFromYear(List<Entry> entriesList, List<String> labelsList) {
        this.entries.clear();
        this.entries.addAll(entriesList);
        this.labelsList.clear();
        this.labelsList.addAll(labelsList);
        checkData();
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }

}
