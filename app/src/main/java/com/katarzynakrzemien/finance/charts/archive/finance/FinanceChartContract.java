package com.katarzynakrzemien.finance.charts.archive.finance;

import com.github.mikephil.charting.data.BarEntry;
import com.katarzynakrzemien.finance.charts.archive.BaseArchiveChartContract;

import java.util.List;

public class FinanceChartContract {

    public interface View extends BaseArchiveChartContract.View {

        void renderYearsList(List<String> yearsList);

        void renderArchiveSalaryChartListsFromYear(List<BarEntry> archiveSalaryList);

        void renderArchiveSpendingChartListsFromYear(List<BarEntry> archiveSpendingList);

        void renderArchiveRemainingFundsChartListsFromYear(List<BarEntry> archiveRemainingFundsList);

        void renderLabelsChartListsFromYear(List<String> labelsList);
    }

    public interface Presenter extends BaseArchiveChartContract.Presenter<View> {
    }
}
