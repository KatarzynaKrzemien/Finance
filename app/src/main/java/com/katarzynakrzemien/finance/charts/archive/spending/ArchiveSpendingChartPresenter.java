package com.katarzynakrzemien.finance.charts.archive.spending;

import com.github.mikephil.charting.data.BarEntry;
import com.katarzynakrzemien.finance.charts.archive.BaseArchiveChartPresenter;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.models.Period;

import java.util.ArrayList;
import java.util.List;

public class ArchiveSpendingChartPresenter extends BaseArchiveChartPresenter<ArchiveSpendingChartContract.View> implements ArchiveSpendingChartContract.Presenter {

    @Override
    public void fetchChartListsFromYear(DatabaseStore databaseStore, String year) {
        List<Period> periodList = databaseStore.getArchivePeriods();
        List<BarEntry> entryList = new ArrayList<>();
        List<String> labelsList = new ArrayList<>();
        for (int i = 0; i < periodList.size(); i++) {
            Period period = periodList.get(i);
            if (period.getStartYear() == Integer.parseInt(year)) {
                entryList.add(new BarEntry(i, (float) (databaseStore.getArchiveFixedOutgoingsSum(period.getId())
                        + databaseStore.getArchiveSpendingSum(period.getId()))));
                labelsList.add(i, getLabel(period));
            }
        }
        view.renderChartListsFromYear(entryList, labelsList);
    }
}
