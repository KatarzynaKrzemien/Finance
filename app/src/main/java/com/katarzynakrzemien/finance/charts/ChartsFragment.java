package com.katarzynakrzemien.finance.charts;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.charts.archive.spending.ArchiveSpendingChartFragment;
import com.katarzynakrzemien.finance.charts.archive.finance.FinanceChartFragment;
import com.katarzynakrzemien.finance.charts.archive.savings.SavingsChartsFragment;
import com.katarzynakrzemien.finance.utils.ViewPagerAdapter;

import static com.katarzynakrzemien.finance.charts.current.CurrentChartFragment.newInstance;

public class ChartsFragment extends Fragment {

    public static final String TAG = "CHARTS";

    private ViewPager viewPager;
    private TabLayout tabLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_pager, container, false);

        viewPager = view.findViewById(R.id.viewpager);
        tabLayout = view.findViewById(R.id.tab);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        viewPagerAdapter.addFragment(newInstance(false), getString(R.string.currentChart));
        viewPagerAdapter.addFragment(new ArchiveSpendingChartFragment(), getString(R.string.archiveChart));
        viewPagerAdapter.addFragment(new FinanceChartFragment(), getString(R.string.financeChart));
        viewPagerAdapter.addFragment(new SavingsChartsFragment(), getString(R.string.savings));

        viewPager.setAdapter(viewPagerAdapter);
    }

}


