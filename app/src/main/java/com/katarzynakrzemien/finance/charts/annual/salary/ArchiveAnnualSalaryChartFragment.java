package com.katarzynakrzemien.finance.charts.annual.salary;

import android.content.Context;

import com.katarzynakrzemien.finance.charts.annual.ArchiveAnnualChartFragment;

public class ArchiveAnnualSalaryChartFragment extends ArchiveAnnualChartFragment {

    @Override
    public void onAttach(Context context) {
        presenter = new ArchiveAnnualSalaryChartPresenter();
        super.onAttach(context);
    }
}
