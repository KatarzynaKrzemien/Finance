package com.katarzynakrzemien.finance.charts.current;

import com.github.mikephil.charting.data.PieEntry;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.CategorizedFixedOutgoing;
import com.katarzynakrzemien.finance.database.models.CategorizedSpending;
import com.katarzynakrzemien.finance.database.models.FixedOutgoing;

import java.util.ArrayList;
import java.util.List;

public class CurrentChartPresenter implements CurrentChartContract.Presenter {

    private CurrentChartContract.View view;

    @Override
    public void attachView(CurrentChartContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void fetchChartsLists(DatabaseStore databaseStore) {
        List<CategorizedSpending> categorizedSpendingsList = databaseStore.getCurrentSpendings();
        List<PieEntry> entryList = new ArrayList<>();
        List<String> labelsList = new ArrayList<>();

        for (int i = 0; i < categorizedSpendingsList.size(); i++) {
            entryList.add(new PieEntry((float) categorizedSpendingsList.get(i).getSum(), categorizedSpendingsList.get(i).getCategoryName()));
            labelsList.add(categorizedSpendingsList.get(i).getCategoryName());
        }
        view.renderChartsLists(entryList, labelsList);
    }

    @Override
    public void fetchVariant(PreferenceStore preferenceStore) {
        view.setVariant(preferenceStore.getVariant());
    }

    @Override
    public void fetchSalary(DatabaseStore databaseStore) {
        view.setSalary(databaseStore.getSalarySum());
    }

    @Override
    public void fetchAvailableFunds(DatabaseStore databaseStore) {
        List<FixedOutgoing> fixedOutgoingList = databaseStore.getFixedOutgoings();
        double fixedOutgoings = 0.0;
        for (int i = 0; i < fixedOutgoingList.size(); i++) {
            fixedOutgoings += fixedOutgoingList.get(i).getOutcome();
        }
        view.setAvailableFunds(databaseStore.getSalarySum() - databaseStore.getSpendingSum() - fixedOutgoings);
    }

    @Override
    public void fetchSpending(DatabaseStore databaseStore) {
        view.setSpending(getSpending(databaseStore.getCurrentSpendings()));
    }

    private double getSpending(List<CategorizedSpending> categorizedSpendingList) {
        double spending = 0.0;
        for (int i = 0; i < categorizedSpendingList.size(); i++) {
            spending += categorizedSpendingList.get(i).getSum();
        }
        return spending;
    }
}
