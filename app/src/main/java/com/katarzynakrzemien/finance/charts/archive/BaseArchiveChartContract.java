package com.katarzynakrzemien.finance.charts.archive;

import com.katarzynakrzemien.finance.BaseContract;
import com.katarzynakrzemien.finance.database.DatabaseStore;

import java.util.List;

public class BaseArchiveChartContract {

    public interface View extends BaseContract.View {
        void renderYearsList(List<String> yearsList);
    }

    public interface Presenter<T extends BaseContract.View> extends BaseContract.Presenter<T> {
        void fetchYearsList(DatabaseStore databaseStore);

        void fetchChartListsFromYear(DatabaseStore databaseStore, String year);
    }
}
