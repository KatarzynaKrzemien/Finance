package com.katarzynakrzemien.finance.charts.archive.spending;

import android.content.Context;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.katarzynakrzemien.finance.App;
import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.database.DatabaseQuery;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.utils.formatter.amount.AmountFormatter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ArchiveSpendingChartFragment extends Fragment implements ArchiveSpendingChartContract.View {

    private ArrayList<BarEntry> entries = new ArrayList<>();
    private ArrayList<String> yearsList = new ArrayList<>();
    private List<String> labelsList = new ArrayList<>();
    private HorizontalBarChart archiveChart;
    private Spinner spinnerYears;
    private TextView archiveDate, archiveQuota;
    private TypedValue typedValue = new TypedValue();
    private DatabaseStore databaseStore;
    private AmountFormatter amountFormatter;
    private ArchiveSpendingChartContract.Presenter presenter = new ArchiveSpendingChartPresenter();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        amountFormatter = ((App) Objects.requireNonNull(getActivity()).getApplicationContext()).amountFormatter;
        databaseStore = new DatabaseQuery(context);
        presenter.attachView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bar_chart, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        View view = getView();
        if (view != null) {

            archiveChart = view.findViewById(R.id.archive_bar_chart);
            spinnerYears = view.findViewById(R.id.spinnerYears);
            archiveDate = view.findViewById(R.id.text_date_archive);
            archiveQuota = view.findViewById(R.id.text_quota_archive);

            prepareSpinnerYears();
        }
    }

    private void prepareSpinnerYears() {
        presenter.fetchYearsList(databaseStore);
        final ArrayAdapter<String> adapterYears = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, yearsList);
        spinnerYears.setAdapter(adapterYears);
        spinnerYears.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                archiveChart.clear();
                presenter.fetchChartListsFromYear(databaseStore, spinnerYears.getSelectedItem().toString());
                prepareChart();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void prepareChart() {
        archiveChart.animateY(2000);
        archiveChart.getLegend().setEnabled(false);
        archiveChart.getDescription().setEnabled(false);
        archiveChart.setDrawValueAboveBar(false);
        archiveChart.setVisibleXRangeMaximum(24);
        archiveChart.setVisibleXRangeMinimum(0);
        archiveChart.setContentDescription(getString(R.string.archiveChart));
        archiveChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, Highlight h) {
                archiveDate.setText(labelsList.get((int) e.getX()));
                archiveQuota.setText(amountFormatter.format((double) e.getY()));
            }

            @Override
            public void onNothingSelected() {
                archiveDate.setText(R.string.empty);
                archiveQuota.setText(R.string.empty);
            }

        });
        archiveChart.invalidate();
    }

    private void prepareAxis() {
        YAxis yl = archiveChart.getAxisLeft();
        YAxis yR = archiveChart.getAxisRight();
        XAxis x = archiveChart.getXAxis();
        x.setPosition(XAxis.XAxisPosition.BOTTOM);
        x.setValueFormatter(new LabelAdapter(labelsList));
        x.setGranularity(1f);
        x.setGranularityEnabled(true);
        if (getContext().getTheme().resolveAttribute(android.R.attr.textColorPrimary, typedValue, true)) {
            yl.setTextColor(typedValue.data);
            x.setTextColor(typedValue.data);
            yR.setTextColor(typedValue.data);
        }
    }

    private void checkData() {
        archiveDate.setText(R.string.empty);
        archiveQuota.setText(R.string.empty);

        if (!entries.isEmpty()) {
            BarDataSet dataSet = new BarDataSet(entries, "");
            dataSet.setColors(ColorTemplate.PASTEL_COLORS);
            dataSet.setDrawValues(true);
            BarData data = new BarData(dataSet);
            if (getContext().getTheme().resolveAttribute(android.R.attr.textColorPrimary, typedValue, true)) {
                data.setValueTextColor(typedValue.data);
            }
            archiveChart.setData(data);
            prepareAxis();
        }
    }

    @Override
    public void renderYearsList(List<String> yearsList) {
        this.yearsList.clear();
        this.yearsList.addAll(yearsList);

    }

    @Override
    public void renderChartListsFromYear(List<BarEntry> entriesList, List<String> labelsList) {
        this.entries.clear();
        this.entries.addAll(entriesList);
        this.labelsList.clear();
        this.labelsList.addAll(labelsList);
        checkData();
    }

    private class LabelAdapter implements IAxisValueFormatter {
        private List<String> sValues;

        LabelAdapter(List<String> values) {
            this.sValues = values;
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            return sValues.get((int) value);
        }
    }

}
