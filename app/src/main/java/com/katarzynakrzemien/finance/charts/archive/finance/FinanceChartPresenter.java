package com.katarzynakrzemien.finance.charts.archive.finance;

import com.github.mikephil.charting.data.BarEntry;
import com.katarzynakrzemien.finance.charts.archive.BaseArchiveChartPresenter;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.models.Period;
import com.katarzynakrzemien.finance.database.models.variants.Variants;

import java.util.ArrayList;
import java.util.List;

public class FinanceChartPresenter extends BaseArchiveChartPresenter<FinanceChartContract.View> implements FinanceChartContract.Presenter {

    @Override
    public void fetchChartListsFromYear(DatabaseStore databaseStore, String year) {
        List<Period> periodList = databaseStore.getArchivePeriods();
        List<BarEntry> archiveSalaryList = new ArrayList<>();
        List<BarEntry> archiveSpendingList = new ArrayList<>();
        List<BarEntry> archiveRemainingFundsList = new ArrayList<>();
        List<String> labelsList = new ArrayList<>();
        for (int i = 0; i < periodList.size(); i++) {
            Period period = periodList.get(i);
            if (period.getStartYear() == Integer.parseInt(year)) {
                double archiveFixedOutgoingSum = databaseStore.getArchiveFixedOutgoingsSum(period.getId());
                double archiveSpendingSum = (databaseStore.getArchiveSalarySum(period.getId()) + archiveFixedOutgoingSum);
                double archiveSalarySum = databaseStore.getArchiveSpendingSum(period.getId());
                if (period.getVariant().equals(Variants.MONITORING)) {
                    archiveSpendingList.add(new BarEntry(i, (float) archiveSpendingSum));
                } else {
                    archiveSalaryList.add(new BarEntry(i, (float) archiveSalarySum));
                    archiveRemainingFundsList.add(new BarEntry(i, (float) (archiveSalarySum - archiveSpendingSum)));
                }
                labelsList.add(i, getLabel(period));
            }
        }
        view.renderArchiveSalaryChartListsFromYear(archiveSalaryList);
        view.renderArchiveSpendingChartListsFromYear(archiveSpendingList);
        view.renderArchiveRemainingFundsChartListsFromYear(archiveRemainingFundsList);
        view.renderLabelsChartListsFromYear(labelsList);
    }
}
