package com.katarzynakrzemien.finance.charts.annual.salary;

import com.github.mikephil.charting.data.PieEntry;
import com.katarzynakrzemien.finance.charts.annual.ArchiveAnnualChartContract;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.models.BaseModel;

import java.util.ArrayList;
import java.util.List;

public class ArchiveAnnualSalaryChartPresenter implements ArchiveAnnualChartContract.Presenter {

    ArchiveAnnualChartContract.View view;

    @Override
    public void fetchChartsLists(DatabaseStore databaseStore, int year) {
        List<BaseModel> archiveList = databaseStore.getAnnualArchiveSalarySums(year);
        List<PieEntry> chartList = new ArrayList<>();
        List<String> entryList = new ArrayList<>();
        for (int i = 0; i < archiveList.size(); i++) {
            chartList.add(new PieEntry((float) archiveList.get(i).getQuota(),
                    archiveList.get(i).getName()));
            entryList.add(archiveList.get(i).getName());
        }
        view.renderChartsLists(chartList, entryList);
    }

    @Override
    public void attachView(ArchiveAnnualChartContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }
}