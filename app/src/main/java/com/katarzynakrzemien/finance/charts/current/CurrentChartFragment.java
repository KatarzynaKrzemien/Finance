package com.katarzynakrzemien.finance.charts.current;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.katarzynakrzemien.finance.App;
import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.database.DatabaseQuery;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceFromDatabase;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.variants.Variants;
import com.katarzynakrzemien.finance.utils.formatter.amount.AmountFormatter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CurrentChartFragment extends Fragment implements CurrentChartContract.View {

    public static final String TAG = "CurrentChartFragment";
    private static final String FORWARD_BUTTON_REQUIRE_KEY = "forwardButtonRequire";
    OnCreateListener mCallback;
    private Boolean forwardButtonRequire;
    private List<String> labelsList = new ArrayList<>();
    private List<PieEntry> entries = new ArrayList<>();
    private PieChart pieChart;
    private ProgressBar remainingProgressBar;
    private TextView textProgressBar;
    private ImageButton forwardButton;
    private AmountFormatter amountFormatter;
    private DatabaseStore databaseStore;
    private PreferenceStore preferenceStore;
    private Variants variant;
    private CurrentChartContract.Presenter presenter = new CurrentChartPresenter();

    public static CurrentChartFragment newInstance(boolean forwardButtonRequire) {
        CurrentChartFragment fragment = new CurrentChartFragment();
        Bundle arguments = new Bundle();
        arguments.putBoolean(FORWARD_BUTTON_REQUIRE_KEY, forwardButtonRequire);
        fragment.setArguments(arguments);
        return fragment;
    }

    public void setOnCreateListener(Activity activity) {
        mCallback = (OnCreateListener) activity;
    }

    @Override
    public void renderChartsLists(List<PieEntry> entriesList, List<String> labelsList) {
        this.entries.clear();
        this.entries.addAll(entriesList);
        this.labelsList.clear();
        this.labelsList.addAll(labelsList);
/*
        if (forwardButtonRequire && zeroOutcome) { //zeroOutcome- powinno przejść jeśli suma wydatków jest<0.0
            mCallback.setMainFragment();
        }*/
    }

    @Override
    public void setVariant(Variants variant) {
        this.variant = variant;
    }

    @Override
    public void setSalary(double salary) {
        remainingProgressBar.setMax((int) salary);
    }

    @Override
    public void setAvailableFunds(double availableFunds) {
        remainingProgressBar.setProgress((int) availableFunds);
    }

    @Override
    public void setSpending(double spending) {
        textProgressBar.setText(getString(R.string.spending) + " " + amountFormatter.format(spending));

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        amountFormatter = ((App) Objects.requireNonNull(getActivity()).getApplicationContext()).amountFormatter;
        databaseStore = new DatabaseQuery(context);
        preferenceStore = new PreferenceFromDatabase(context);
        presenter.attachView(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            forwardButtonRequire = getArguments().getBoolean(FORWARD_BUTTON_REQUIRE_KEY);
        } else {
            throw (new IllegalArgumentException());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pie_chart, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        View view = getView();
        if (view != null) {
            presenter.fetchVariant(preferenceStore);
            forwardButton = view.findViewById(R.id.forwardButton);
            remainingProgressBar = view.findViewById(R.id.remaining_progress_bar);
            textProgressBar = view.findViewById(R.id.text_progress_bar);
            pieChart = view.findViewById(R.id.piechart);

            prepareForwardButton();
            prepareProgressBar();
            presenter.fetchChartsLists(databaseStore);
            checkData();
            prepareChart();
        }
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }

    private void prepareForwardButton() {
        if (forwardButtonRequire) {
            forwardButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.setMainFragment();
                }
            });
        } else {
            forwardButton.setVisibility(View.GONE);
        }
    }

    private void prepareProgressBar() {
        if ((getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) <=
                Configuration.SCREENLAYOUT_SIZE_LARGE
                && getResources().getConfiguration().orientation ==
                Configuration.ORIENTATION_LANDSCAPE) {
            remainingProgressBar.setVisibility(View.GONE);
            textProgressBar.setVisibility(View.GONE);
        } else {


            if (variant.equals(Variants.MONITORING)) {
                remainingProgressBar.setVisibility(View.GONE);
                presenter.fetchSpending(databaseStore);
            } else {
                textProgressBar.setText(getString(R.string.available_funds));
                presenter.fetchSalary(databaseStore);
                presenter.fetchAvailableFunds(databaseStore);
            }
        }
    }

    private void prepareChart() {
        pieChart.setUsePercentValues(true);
        pieChart.animateY(2000);
        pieChart.getLegend().setEnabled(false);
        pieChart.getDescription().setEnabled(false);

        pieChart.setHighlightPerTapEnabled(true);
        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if (e != null) {
                    String text = labelsList.get((int) h.getX()) + "\n" + amountFormatter.format((double) e.getY());
                    pieChart.setCenterText(text);
                }
            }

            @Override
            public void onNothingSelected() {
                pieChart.setCenterText("");
            }
        });
    }

    private void checkData() {
        if (entries.isEmpty()) {
            pieChart.clear();
            if (forwardButtonRequire) {
                mCallback.setMainFragment();
            }
        } else {
            PieDataSet dataSet = new PieDataSet(entries, "CurrentChart");
            PieData data = new PieData(dataSet);
            data.setValueFormatter(new PercentFormatter());
            dataSet.setColors(ColorTemplate.PASTEL_COLORS);
            dataSet.setValueTextColor(getResources().getColor(R.color.light));

            pieChart.setData(data);
        }
    }

    public interface OnCreateListener {
        void setMainFragment();

    }
}

