package com.katarzynakrzemien.finance.charts.annual;

import com.github.mikephil.charting.data.PieEntry;
import com.katarzynakrzemien.finance.BaseContract;
import com.katarzynakrzemien.finance.database.DatabaseStore;

import java.util.List;

public class ArchiveAnnualChartContract {

    public interface View extends BaseContract.View {
        void renderChartsLists(List<PieEntry> entriesList, List<String> labelsList);
    }

    public interface Presenter extends BaseContract.Presenter<View> {
        void fetchChartsLists(DatabaseStore databaseStore, int year);
    }
}
