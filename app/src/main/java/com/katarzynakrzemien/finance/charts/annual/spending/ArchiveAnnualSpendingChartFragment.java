package com.katarzynakrzemien.finance.charts.annual.spending;

import android.content.Context;

import com.katarzynakrzemien.finance.charts.annual.ArchiveAnnualChartFragment;

public class ArchiveAnnualSpendingChartFragment extends ArchiveAnnualChartFragment {

    @Override
    public void onAttach(Context context) {
        presenter = new ArchiveAnnualSpendingChartPresenter();
        super.onAttach(context);
    }
}
