package com.katarzynakrzemien.finance.charts.archive;

import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.models.Period;
import com.katarzynakrzemien.finance.utils.formatter.date.DateFormatPattern;
import com.katarzynakrzemien.finance.utils.formatter.date.DateFormatter;
import com.katarzynakrzemien.finance.utils.formatter.date.SimpleDateFormatter;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseArchiveChartPresenter<T extends BaseArchiveChartContract.View> implements BaseArchiveChartContract.Presenter<T> {

    protected T view;
    private DateFormatter dateFormatter = new SimpleDateFormatter(DateFormatPattern.DAY_MONTH);

    @Override
    public void attachView(T view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void fetchYearsList(DatabaseStore databaseStore) {
        List<String> yearsList = new ArrayList<>();
        for (Period period : databaseStore.getArchivePeriods()) {
            if (!yearsList.contains(String.valueOf(period.getStartYear()))) {
                yearsList.add(String.valueOf(period.getStartYear()));
            }
        }
        view.renderYearsList(yearsList);
    }

    protected String getLabel(Period period) {
        return dateFormatter.formatToString(period.getStartDate());

    }
}
