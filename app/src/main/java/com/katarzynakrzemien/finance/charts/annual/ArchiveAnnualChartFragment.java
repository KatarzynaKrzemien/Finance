package com.katarzynakrzemien.finance.charts.annual;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.katarzynakrzemien.finance.App;
import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.charts.annual.fixedoutgoing.ArchiveAnnualFixedOutgoingsChartFragment;
import com.katarzynakrzemien.finance.database.DatabaseQuery;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.utils.formatter.amount.AmountFormatter;

import java.util.ArrayList;
import java.util.List;

public abstract class ArchiveAnnualChartFragment extends Fragment implements ArchiveAnnualChartContract.View {

    private static String YEAR_KEY = "year";
    protected ArchiveAnnualChartContract.Presenter presenter;
    private int year;
    private List<PieEntry> entries = new ArrayList<>();
    private List<String> chartList = new ArrayList<>();
    private PieChart pieChart;
    private DatabaseStore databaseStore;
    private AmountFormatter amountFormatter;


    public static ArchiveAnnualFixedOutgoingsChartFragment newInstance(int year) {
        ArchiveAnnualFixedOutgoingsChartFragment fragment = new ArchiveAnnualFixedOutgoingsChartFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(YEAR_KEY, year);
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        databaseStore = new DatabaseQuery(context);
        presenter.attachView(this);
        amountFormatter = ((App) context.getApplicationContext()).amountFormatter;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            year = getArguments().getInt(YEAR_KEY);
        } else {
            throw (new IllegalArgumentException());
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pie_chart, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        View view = getView();
        if (view != null) {

            view.findViewById(R.id.progress_bar_layout).setVisibility(View.GONE);
            pieChart = view.findViewById(R.id.piechart);
            presenter.fetchChartsLists(databaseStore, year);
            checkData();
            prepareChart();
        }
    }

    private void prepareChart() {
        pieChart.setUsePercentValues(true);
        pieChart.animateY(2000);
        pieChart.getLegend().setEnabled(false);
        pieChart.getDescription().setEnabled(false);

        pieChart.setHighlightPerTapEnabled(true);
        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if (e != null) {
                    String text = chartList.get((int) h.getX()) + "\n" + amountFormatter.format((double) e.getY());
                    pieChart.setCenterText(text);
                }
            }

            @Override
            public void onNothingSelected() {
                pieChart.setCenterText("");
            }
        });
    }

    private void checkData() {
        if (entries.isEmpty()) {
            pieChart.clear();
        } else {
            PieDataSet dataSet = new PieDataSet(entries, "ArchiveAnnual");
            PieData data = new PieData(dataSet);
            data.setValueFormatter(new PercentFormatter());
            dataSet.setColors(ColorTemplate.PASTEL_COLORS);
            dataSet.setValueTextColor(getResources().getColor(R.color.light));
            pieChart.setData(data);
        }
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }

    @Override
    public void renderChartsLists(List<PieEntry> entriesList, List<String> labelsList) {
        chartList.clear();
        entries.clear();
        chartList.addAll(labelsList);
        entries.addAll(entriesList);

    }
}
