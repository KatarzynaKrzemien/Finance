package com.katarzynakrzemien.finance.charts.archive.finance;

import android.content.Context;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.katarzynakrzemien.finance.App;
import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.database.DatabaseQuery;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.utils.formatter.amount.AmountFormatter;

import java.util.ArrayList;
import java.util.List;

public class FinanceChartFragment extends Fragment implements FinanceChartContract.View {

    private ArrayList<BarEntry> salaryEntries = new ArrayList<>();
    private ArrayList<BarEntry> remainingFundsEntries = new ArrayList<>();
    private ArrayList<BarEntry> spendingEntries = new ArrayList<>();
    private ArrayList<String> yearsList = new ArrayList<>();
    private List<String> labelsList = new ArrayList<>();
    private BarChart financeChart;
    private Spinner spinnerYears;
    private TextView dateText;
    private TextView salaryView, spendingView, remainingFundsView;
    private TypedValue typedValue = new TypedValue();
    private AmountFormatter amountFormatter;
    private DatabaseStore databaseStore;
    private FinanceChartContract.Presenter presenter = new FinanceChartPresenter();


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        amountFormatter = ((App) context.getApplicationContext()).amountFormatter;
        databaseStore = new DatabaseQuery(context);
        presenter.attachView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_finance_chart, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        View view = getView();
        if (view != null) {
            financeChart = view.findViewById(R.id.remaining_line_chart);
            dateText = view.findViewById(R.id.date_archive);
            salaryView = view.findViewById(R.id.salary);
            spendingView = view.findViewById(R.id.spending);
            remainingFundsView = view.findViewById(R.id.remaining_funds);
            spinnerYears = view.findViewById(R.id.spinnerYears);

            prepareSpinnerYears();
        }
    }

    private void prepareSpinnerYears() {
        presenter.fetchYearsList(databaseStore);
        ArrayAdapter<String> adapterYears = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, yearsList);
        spinnerYears.setAdapter(adapterYears);
        financeChart.setSelected(false);
        spinnerYears.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                presenter.fetchChartListsFromYear(databaseStore, spinnerYears.getSelectedItem().toString());
                checkData();
                prepareChart();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void prepareAxis() {
        YAxis yl = financeChart.getAxisLeft();
        YAxis yR = financeChart.getAxisRight();
        if (getContext().getTheme().resolveAttribute(android.R.attr.textColorPrimary, typedValue, true)) {
            yl.setTextColor(typedValue.data);
            yR.setTextColor(typedValue.data);
        }
        financeChart.getXAxis().setEnabled(false);
    }

    private void prepareChart() {

        financeChart.animateY(2000);
        if (getContext().getTheme().resolveAttribute(android.R.attr.textColorPrimary, typedValue, true)) {
            financeChart.getLegend().setTextColor(typedValue.data);
        }
        financeChart.getDescription().setEnabled(false);
        financeChart.setVisibleXRangeMaximum(24);
        financeChart.setVisibleXRangeMinimum(0);
        financeChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, Highlight h) {
                cleanInfoViews();
                setupInfoViews((int) e.getX());
            }

            @Override
            public void onNothingSelected() {
                cleanInfoViews();
            }
        });
        financeChart.invalidate();

    }

    private void checkData() {
        financeChart.clear();
        cleanInfoViews();
        if ((!(salaryEntries.isEmpty() && remainingFundsEntries.isEmpty())) || !spendingEntries.isEmpty()) {

            int salaryColor = getResources().getColor(R.color.salaryChart);
            BarDataSet salaryDataSet = new BarDataSet(salaryEntries, getResources().getString(R.string
                    .archiveSalaryChartInfo));
            salaryDataSet.setColor(salaryColor);

            int remainingFundsColor = getResources().getColor(R.color.remainingChart);
            BarDataSet remainingFundsDataSet = new BarDataSet(remainingFundsEntries, getResources().getString(R.string.archiveRemainingAndSavingsChartInfo));
            remainingFundsDataSet.setColor(remainingFundsColor);

            int spendingColor = getResources().getColor(R.color.spendingChart);
            BarDataSet spendingDataSet = new BarDataSet(salaryEntries, getResources().getString(R.string
                    .archiveSalaryChartInfo));
            salaryDataSet.setColor(spendingColor);

            ArrayList<IBarDataSet> lines = new ArrayList<>();
            lines.add(salaryDataSet);
            lines.add(remainingFundsDataSet);
            lines.add(spendingDataSet);

            BarData data = new BarData(lines);
            if (getContext().getTheme().resolveAttribute(android.R.attr.textColorPrimary, typedValue, true)) {
                data.setValueTextColor(typedValue.data);
            }
            financeChart.setData(data);
            prepareAxis();
        }
    }

    private void cleanInfoViews() {
        salaryView.setText(R.string.empty);
        spendingView.setText(R.string.empty);
        remainingFundsView.setText(R.string.empty);
        dateText.setText(R.string.empty);
    }

    private void setupInfoViews(int index) {
        dateText.setText(labelsList.get(index));
        Double archiveSpendingSum = null;
        if (salaryEntries.get(index) != null && remainingFundsEntries.get(index) != null) {
            salaryView.setText(amountFormatter.format((double) salaryEntries.get(index).getY()));
            remainingFundsView.setText(amountFormatter.format((double) remainingFundsEntries.get(index).getY()));
            archiveSpendingSum = (double) salaryEntries.get(index).getY() - (double) remainingFundsEntries.get(index).getY();
        }
        if (spendingEntries.get(index) != null) {
            spendingView.setText(amountFormatter.format((double) spendingEntries.get(index).getY()));
        }
        if (archiveSpendingSum != null) {
            spendingView.setText(amountFormatter.format(archiveSpendingSum));
        }
    }

    @Override
    public void renderYearsList(List<String> yearsList) {
        this.yearsList.clear();
        this.yearsList.addAll(yearsList);
    }

    @Override
    public void renderArchiveSalaryChartListsFromYear(List<BarEntry> archiveSalaryList) {
        this.salaryEntries.clear();
        this.salaryEntries.addAll(archiveSalaryList);
    }

    @Override
    public void renderArchiveSpendingChartListsFromYear(List<BarEntry> archiveSpendingList) {
        this.spendingEntries.clear();
        this.spendingEntries.addAll(archiveSpendingList);
    }

    @Override
    public void renderArchiveRemainingFundsChartListsFromYear(List<BarEntry> archiveRemainingFundsList) {
        this.remainingFundsEntries.clear();
        this.remainingFundsEntries.addAll(archiveRemainingFundsList);
    }

    @Override
    public void renderLabelsChartListsFromYear(List<String> labelsList) {
        this.labelsList.clear();
        this.labelsList.addAll(labelsList);
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }
}
