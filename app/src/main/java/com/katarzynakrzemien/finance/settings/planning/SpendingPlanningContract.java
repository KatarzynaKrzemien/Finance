package com.katarzynakrzemien.finance.settings.planning;

import com.katarzynakrzemien.finance.BaseRecyclerContract;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.Category;
import com.katarzynakrzemien.finance.database.models.CategoryAndIncome;

public class SpendingPlanningContract {
    public interface View extends BaseRecyclerContract.View<CategoryAndIncome> {

        void setAvailableFunds(Double availableFunds);

    }

    public interface Presenter extends BaseRecyclerContract.Presenter<View> {

        void fetchAvailableFunds(DatabaseStore databaseStore, PreferenceStore preferenceStore);

        void updateListItem(DatabaseStore databaseStore, CategoryAndIncome model);

    }
}
