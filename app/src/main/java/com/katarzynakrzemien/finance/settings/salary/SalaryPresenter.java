package com.katarzynakrzemien.finance.settings.salary;

import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.models.Salary;
import com.katarzynakrzemien.finance.database.models.TotalSalary;

import java.util.ArrayList;
import java.util.List;

public class SalaryPresenter implements SalaryContract.Presenter {

    private SalaryContract.View view;


    @Override
    public void fetchTotalSalary(DatabaseStore databaseStore) {
        view.renderTotalSalary(databaseStore.getSalarySum());
    }

    @Override
    public void addListItem(DatabaseStore databaseStore, Salary model) {
        databaseStore.insertSalary(model);

    }

    @Override
    public void updateListItem(DatabaseStore databaseStore, Salary model) {
        databaseStore.updateSalary(model);
    }

    @Override
    public void deleteListItem(DatabaseStore databaseStore, int id) {
        databaseStore.deleteSalary(id);
    }

    @Override
    public void fetchList(DatabaseStore databaseStore) {
        List<Salary> salaryList = new ArrayList<>();
        TotalSalary model = databaseStore.getSalaries();
        Salary fromSavings = null;
        if (model.getFromSavings().getQuota() > 0.0) {
            fromSavings = model.getFromSavings();
        }
        for (int i = 0; i < model.getSalaryListSize(); i++) {
            salaryList.add(model.get(i));
        }

        view.renderList(salaryList, model.getLastMonthRemainingFunds(), fromSavings);
    }

    @Override
    public void attachView(SalaryContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }
}
