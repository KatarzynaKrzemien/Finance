package com.katarzynakrzemien.finance.settings.savings;

import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.Salary;

public class SavingsPresenter implements SavingsContract.Presenter {

    private SavingsContract.View view;

    @Override
    public void attachView(SavingsContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void fetchSavings(PreferenceStore preferenceStore) {
        view.setSavings(preferenceStore.getSavings());
    }

    @Override
    public void fetchMonthlySavings(PreferenceStore preferenceStore) {
        view.setMonthlySavings(preferenceStore.getMonthlySavings());
    }

    @Override
    public void fetchSavedSalary(DatabaseStore databaseStore) {
        view.setSavedSalary(databaseStore.getSavedSalarySum());
    }

    @Override
    public void fetchTotalMonthlySavings(DatabaseStore databaseStore, PreferenceStore preferenceStore) {
        view.setTotalMonthlySavings(databaseStore.getSavedSalarySum() + preferenceStore.getMonthlySavings());
    }

    @Override
    public void updateSavings(PreferenceStore preferenceStore, DatabaseStore databaseStore, Double savings) {
        Salary salaryFromSavings = databaseStore.getSalaryFromSavings();
        databaseStore.updateSalary(new Salary(salaryFromSavings.getId(), salaryFromSavings.getName(),
                salaryFromSavings.getQuota() + savings, salaryFromSavings.getSave()));
        view.setSavings(-savings);
    }

    @Override
    public void updateMonthlySavings(PreferenceStore preferenceStore, Double monthlySavings) {
        preferenceStore.setMonthlySavings(monthlySavings);
    }
}
