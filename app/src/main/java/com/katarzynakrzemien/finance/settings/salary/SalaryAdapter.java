package com.katarzynakrzemien.finance.settings.salary;

import android.content.Context;
import android.view.View;

import com.katarzynakrzemien.finance.database.models.Salary;
import com.katarzynakrzemien.finance.utils.recycler.ItemClickListener;
import com.katarzynakrzemien.finance.utils.recycler.swipe.ItemSwipeListener;

import java.util.List;

public class SalaryAdapter extends BaseSalaryAdapter<Salary> {


    private ItemClickListener itemClickListener;
    private ItemSwipeListener<Salary> itemSwipeListener;
    private Salary recentlyChangedItem;
    private int recentlyChangedItemPosition;


    SalaryAdapter(List<Salary> salaryList, Context context, ItemClickListener itemClickListener, ItemSwipeListener<Salary> itemSwipeListener) {
        super(salaryList, context);
        this.itemClickListener = itemClickListener;
        this.itemSwipeListener = itemSwipeListener;
    }

    @Override
    protected View.OnClickListener getOnItemClick(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClick(v, position);
            }
        };
    }

    @Override
    public int getItemCount() {
        return salaryList.size();
    }

    void saveSalary(int position) {
        recentlyChangedItem = salaryList.get(position);
        recentlyChangedItemPosition = position;
        salaryList.get(position).setSave(!salaryList.get(position).getSave());
        itemSwipeListener.changeItem(salaryList.get(position));
        itemSwipeListener.showSnackbarChangeItem();
    }

    void deleteSalary(int position) {
        recentlyChangedItem = salaryList.get(position);
        recentlyChangedItemPosition = position;
        itemSwipeListener.deleteItem(recentlyChangedItem);
        itemSwipeListener.showSnackbarDeletedItem();
    }

    void undoDelete() {
        itemSwipeListener.undoDeleteItem(recentlyChangedItem);
    }

    void undoSave() {
        salaryList.get(recentlyChangedItemPosition).setSave(!salaryList.get(recentlyChangedItemPosition).getSave());
        itemSwipeListener.undoChangeItem(salaryList.get(recentlyChangedItemPosition));

    }

    boolean getItemSaveStatus(int i) {
        if (i < 0) {
            return recentlyChangedItem.getSave();
        }
        return salaryList.get(i).getSave();
    }
}
