package com.katarzynakrzemien.finance.settings.salary;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.katarzynakrzemien.finance.App;
import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.database.DatabaseQuery;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.models.Salary;
import com.katarzynakrzemien.finance.dialog.DialogQuotaCreator;
import com.katarzynakrzemien.finance.dialog.DialogReaction;
import com.katarzynakrzemien.finance.dialog.DialogTextCreator;
import com.katarzynakrzemien.finance.managing.ManagingActivity;
import com.katarzynakrzemien.finance.managing.action.ActionAppListener;
import com.katarzynakrzemien.finance.utils.Keyboard;
import com.katarzynakrzemien.finance.utils.formatter.amount.AmountFormatter;
import com.katarzynakrzemien.finance.utils.recycler.ItemClickListener;
import com.katarzynakrzemien.finance.utils.recycler.swipe.ItemSwipeListener;
import com.katarzynakrzemien.finance.utils.recycler.swipe.UndoSnackbar;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SalaryFragment extends Fragment implements SalaryContract.View, ItemClickListener, ItemSwipeListener<Salary> {

    public static final String TAG = "INCOME";

    private View view = null;
    private SalaryContract.Presenter presenter = new SalaryPresenter();
    private DatabaseStore databaseStore;
    private List<Salary> salaryList = new ArrayList<>();
    private SalaryAdapter salaryAdapter;
    private AmountFormatter amountFormatter;
    private Salary lastMonthRemainingFunds;
    private Salary fromSavings;
    private ActionAppListener actionAppListener;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        amountFormatter = ((App) context.getApplicationContext()).amountFormatter;
        databaseStore = new DatabaseQuery(context);
        presenter.attachView(this);
        actionAppListener = ((ManagingActivity) context);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_income, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        view = getView();
        if (view != null) {
            RecyclerView recyclerView = view.findViewById(R.id.income_recycler_view);
            salaryAdapter = new SalaryAdapter(salaryList, getContext(), this, this);
            recyclerView.setAdapter(salaryAdapter);
            presenter.fetchList(databaseStore);
            presenter.fetchTotalSalary(databaseStore);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new SwipeSalaryItemCallback(salaryAdapter, getContext(), fromSavings != null));
            itemTouchHelper.attachToRecyclerView(recyclerView);
            setupAddSalaryItemListFab();
        }
    }

    private void setupAddSalaryItemListFab() {
        view.findViewById(R.id.fab_income_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final DialogTextCreator dialogAddItem = new DialogTextCreator(getContext(), getString(R.string.addSalary));
                dialogAddItem.setPositiveButton(new DialogReaction() {
                    @Override
                    public void onClick() {
                        presenter.addListItem(databaseStore, new Salary(null,
                                dialogAddItem.getText(), 0.0, false));
                        presenter.fetchList(databaseStore);
                        presenter.fetchTotalSalary(databaseStore);

                        Keyboard.hide(Objects.requireNonNull(getContext()));
                    }
                });
                dialogAddItem.showDialog();
                Keyboard.show(Objects.requireNonNull(getContext()));
            }
        });
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }

    @Override
    public void renderList(List<Salary> list, Salary lastMonthRemainingFunds, Salary fromSavings) {
        salaryList.clear();
        salaryList.add(lastMonthRemainingFunds);
        this.lastMonthRemainingFunds = lastMonthRemainingFunds;
        if (fromSavings != null) {
            salaryList.add(fromSavings);
            this.fromSavings = fromSavings;

        }
        salaryList.addAll(list);
        salaryAdapter.notifyDataSetChanged();
    }

    @Override
    public void renderTotalSalary(Double totalSalary) {
        if (view != null) {
            ((TextView) view.findViewById(R.id.summary_income)).setText(amountFormatter.format(totalSalary));
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        if (!(salaryList.get(position).equals(lastMonthRemainingFunds) || salaryList.get(position).equals(fromSavings))) {
            final Salary salary = salaryList.get(position);
            final String salaryName = salary.getName();
            final DialogQuotaCreator dialogQuotaCreator = new DialogQuotaCreator(getContext(), salaryName);
            dialogQuotaCreator.setPositiveButton(new DialogReaction() {
                @Override
                public void onClick() {
                    String incomeText = dialogQuotaCreator.getDialogEditText().getText().toString();
                    if (incomeText.length() > 0) {
                        double income = Double.parseDouble(incomeText);
                        if (income < 0.0) {
                            income = 0.0;
                        }
                        presenter.updateListItem(databaseStore, new Salary(
                                salary.getId(),
                                salary.getName(),
                                income,
                                salary.getSave()));
                        presenter.fetchList(databaseStore);
                        presenter.fetchTotalSalary(databaseStore);
                        fundsChanged();
                    }
                    Keyboard.hide(Objects.requireNonNull(getContext()));
                }
            });
            dialogQuotaCreator.showDialog();
            Keyboard.show(Objects.requireNonNull(getContext()));
        }
    }

    @Override
    public void onItemLongClick(View view, int position) {
    }

    @Override
    public void deleteItem(Salary model) {
        presenter.deleteListItem(databaseStore, model.getId());
        presenter.fetchList(databaseStore);
        presenter.fetchTotalSalary(databaseStore);
        fundsChanged();
    }

    @Override
    public void changeItem(Salary model) {
        presenter.updateListItem(databaseStore, model);
        presenter.fetchList(databaseStore);
        presenter.fetchTotalSalary(databaseStore);
        fundsChanged();
    }

    @Override
    public void showSnackbarDeletedItem() {
        UndoSnackbar.show((ManagingActivity) Objects.requireNonNull(getActivity()), R.string.deletedSalary, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salaryAdapter.undoDelete();
            }
        });
    }

    @Override
    public void showSnackbarChangeItem() {
        UndoSnackbar.show((ManagingActivity) Objects.requireNonNull(getActivity()), R.string.salaryAddedToSavings, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salaryAdapter.undoSave();
            }
        });
    }

    @Override
    public void undoDeleteItem(Salary model) {
        presenter.addListItem(databaseStore, model);
        presenter.fetchList(databaseStore);
        presenter.fetchTotalSalary(databaseStore);
        fundsChanged();
    }

    @Override
    public void undoChangeItem(Salary model) {
        changeItem(model);
    }

    private void fundsChanged() {
        actionAppListener.fundsChange();
    }

}
