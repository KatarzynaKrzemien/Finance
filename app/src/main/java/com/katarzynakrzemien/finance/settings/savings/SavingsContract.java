package com.katarzynakrzemien.finance.settings.savings;

import com.katarzynakrzemien.finance.BaseContract;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceStore;


public class SavingsContract {

    public interface View extends BaseContract.View {


        void setSavings(double savings);

        void setMonthlySavings(double monthlySavings);

        void setSavedSalary(double savedSalary);

        void setTotalMonthlySavings(double totalMonthlySavings);


    }

    public interface Presenter extends BaseContract.Presenter<SavingsContract.View> {

        void fetchSavings(PreferenceStore preferenceStore);

        void fetchMonthlySavings(PreferenceStore preferenceStore);

        void fetchSavedSalary(DatabaseStore databaseStore);

        void fetchTotalMonthlySavings(DatabaseStore databaseStore, PreferenceStore preferenceStore);

        void updateSavings(PreferenceStore preferenceStore, DatabaseStore databaseStore, Double savings);

        void updateMonthlySavings(PreferenceStore preferenceStore, Double monthlySavings);
    }
}
