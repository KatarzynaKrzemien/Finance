package com.katarzynakrzemien.finance.settings.salary;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.utils.recycler.swipe.SwipeItemCallback;

public class SwipeSalaryItemCallback extends SwipeItemCallback {

    private SalaryAdapter adapter;
    private Drawable yesSavingsIcon, noSavingsIcon;
    private boolean fromSavingsItemEnable;

    SwipeSalaryItemCallback(SalaryAdapter adapter, Context context, boolean fromSavingsItemEnable) {
        super(context);
        this.adapter = adapter;
        yesSavingsIcon = ContextCompat.getDrawable(context, R.drawable.ic_savings);
        noSavingsIcon = ContextCompat.getDrawable(context, R.drawable.ic_no_savings);
        backgroundRight = new ColorDrawable(ContextCompat.getColor(context, R.color.savings));
        this.fromSavingsItemEnable = fromSavingsItemEnable;
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

        if (direction == ItemTouchHelper.LEFT) {
            adapter.deleteSalary(viewHolder.getAdapterPosition());
        } else {
            adapter.saveSalary(viewHolder.getAdapterPosition());
        }
    }

    @Override
    public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        if (fromSavingsItemEnable) {
            return viewHolder.getAdapterPosition() <2 ? ItemTouchHelper.RIGHT : super.getSwipeDirs(recyclerView, viewHolder);

        } else {
            return viewHolder.getAdapterPosition() == 0 ? ItemTouchHelper.RIGHT : super.getSwipeDirs(recyclerView, viewHolder);
        }
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        setSavingsIcon(adapter.getItemSaveStatus(viewHolder.getAdapterPosition()));
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

    }

    private void setSavingsIcon(boolean savedStatus) {
        if (savedStatus) {
            rightIcon = noSavingsIcon;
        } else {
            rightIcon = yesSavingsIcon;
        }
    }
}
