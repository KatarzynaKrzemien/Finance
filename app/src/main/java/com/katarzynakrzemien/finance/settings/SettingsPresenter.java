package com.katarzynakrzemien.finance.settings;

import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.FixedOutgoing;
import com.katarzynakrzemien.finance.database.models.variants.Variants;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class SettingsPresenter implements SettingsContract.Presenter {

    private SettingsContract.View view;

    @Override
    public void attachView(SettingsContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void fetchVariant(PreferenceStore preferenceStore) {
        view.setVariant(preferenceStore.getVariant());
    }

    @Override
    public void fetchSalary(DatabaseStore databaseStore) {
        view.setSalary(databaseStore.getSalarySum());
    }

    @Override
    public void fetchDayOfNewMonth(PreferenceStore preferenceStore) {
        view.setDayOfNewMonth(preferenceStore.getDayOfNewMonth());
    }

    @Override
    public void updateDayOfNewMonth(PreferenceStore preferenceStore, int dayOfNewMonth) {
        preferenceStore.setDayOfNewMonth(dayOfNewMonth);
    }

    @Override
    public void fetchMonthlySavings(PreferenceStore preferenceStore, DatabaseStore databaseStore) {
        view.setMonthlySavings(preferenceStore.getMonthlySavings() + databaseStore.getSavedSalarySum());
    }

    @Override
    public void fetchRemindFixedOutgoings(PreferenceStore preferenceStore) {
        view.setRemindFixedOutgoings(preferenceStore.getRemindingFixedOutgoings());
    }

    @Override
    public void updateRemindFixedOutgoings(PreferenceStore preferenceStore, boolean remindFixedOutgoings) {
        preferenceStore.setRemindingFixedOutgoings(remindFixedOutgoings);
    }

    @Override
    public void fetchTimeRemindFixedOutgoings(PreferenceStore preferenceStore) {
        view.setTimeRemindFixedOutgoings(preferenceStore.getRemindingTimeFixedOutgoings());
    }

    @Override
    public void updateTimeRemindFixedOutgoings(PreferenceStore preferenceStore, Calendar timeRemindFixedOutgoings) {
        preferenceStore.setRemindingTimeFixedOutgoings(timeRemindFixedOutgoings);
    }

    @Override
    public void updateVariant(PreferenceStore preferenceStore, Variants variant) {
        preferenceStore.setVariant(variant);
    }

    @Override
    public void fetchFixedOutgoingsActiveDatedNoPaid(PreferenceStore preferenceStore, DatabaseStore databaseStore) {
        List<FixedOutgoing> fixedOutgoingList = databaseStore.getFixedOutgoings();
        List<FixedOutgoing> newFixedOutgoingList = new ArrayList<>();
        for (int i = 0; i < fixedOutgoingList.size(); i++) {
            if (!fixedOutgoingList.get(i).getIsPaid() && (fixedOutgoingList.get(i).getDueDate() != null)) {
                newFixedOutgoingList.add(fixedOutgoingList.get(i));
            }
        }
        view.renderFixedOutgoingsActiveDatedNoPaid(newFixedOutgoingList, preferenceStore.getRemindingTimeFixedOutgoings());
    }
}
