package com.katarzynakrzemien.finance.settings.salary;

import com.katarzynakrzemien.finance.BaseContract;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.models.Salary;

import java.util.List;

public class SalaryContract {

    public interface View extends BaseContract.View {

        void renderTotalSalary(Double totalSalary);

        void renderList(List<Salary> list, Salary lastMonthRemainingFunds, Salary fromSavings);
    }

    public interface Presenter extends BaseContract.Presenter<View> {

        void fetchList(DatabaseStore databaseStore);

        void fetchTotalSalary(DatabaseStore databaseStore);

        void addListItem(DatabaseStore databaseStore, Salary model);

        void updateListItem(DatabaseStore databaseStore, Salary model);

        void deleteListItem(DatabaseStore databaseStore, int id);

    }
}
