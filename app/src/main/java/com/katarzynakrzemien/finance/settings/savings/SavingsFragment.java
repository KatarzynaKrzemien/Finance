package com.katarzynakrzemien.finance.settings.savings;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.katarzynakrzemien.finance.App;
import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.charts.archive.savings.SavingsChartsFragment;
import com.katarzynakrzemien.finance.database.DatabaseQuery;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceFromDatabase;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.dialog.DialogQuotaCreator;
import com.katarzynakrzemien.finance.dialog.DialogReaction;
import com.katarzynakrzemien.finance.managing.action.ActionAppListener;
import com.katarzynakrzemien.finance.managing.ManagingActivity;
import com.katarzynakrzemien.finance.utils.formatter.amount.AmountFormatter;
import com.katarzynakrzemien.finance.utils.Keyboard;

import java.util.Objects;

public class SavingsFragment extends Fragment implements SavingsContract.View {

    public static final String TAG = "SAVINGS";
    private SavingsContract.Presenter presenter = new SavingsPresenter();
    private DatabaseStore databaseStore;
    private PreferenceStore preferenceStore;
    private AmountFormatter amountFormatter;
    private TextView savingsTextView;
    private TextView savedSalaryTextView;
    private TextView monthlySavingTextView;
    private TextView totalMonthlySavingsTextView;
    private ActionAppListener actionAppListener;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter.attachView(this);
        databaseStore = new DatabaseQuery(context);
        preferenceStore = new PreferenceFromDatabase(context);
        amountFormatter = ((App) context.getApplicationContext()).amountFormatter;
        actionAppListener = ((ManagingActivity) context);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_savings, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        View view = getView();
        if (view != null) {
            setSavingsChartFragment();

            savingsTextView = view.findViewById(R.id.savings_sum);
            savedSalaryTextView = view.findViewById(R.id.savings_incomes);
            monthlySavingTextView = view.findViewById(R.id.savings_month);
            totalMonthlySavingsTextView = view.findViewById(R.id.savings_monthly_sum);

            presenter.fetchSavedSalary(databaseStore);

            if (isSavedSalary()) {
                presenter.fetchMonthlySavings(preferenceStore);
            } else {
                savedSalaryTextView.setVisibility(View.GONE);
                monthlySavingTextView.setVisibility(View.GONE);
                view.findViewById(R.id.savings_incomes_title).setVisibility(View.GONE);
            }
            presenter.fetchSavings(preferenceStore);
            presenter.fetchTotalMonthlySavings(databaseStore, preferenceStore);

            view.findViewById(R.id.savings_sum_layout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final DialogQuotaCreator dialogQuotaCreator = new DialogQuotaCreator(getContext(), getString(R.string.transferToIncomes));
                    dialogQuotaCreator.setPositiveButton(new DialogReaction() {
                        @Override
                        public void onClick() {
                            String transferredSavingsText = Objects.requireNonNull(dialogQuotaCreator.getDialogEditText().getText()).toString();
                            if (transferredSavingsText.length() > 0) {
                                double transferredSavings = Double.parseDouble(transferredSavingsText);
                                if (transferredSavings < 0.0) {
                                    transferredSavings = 0.0;
                                }
                                presenter.updateSavings(preferenceStore, databaseStore, transferredSavings);
                                fundsChanged();
                            }
                            Keyboard.hide(getContext());
                        }
                    });
                    dialogQuotaCreator.showDialog();
                    Keyboard.show(getContext());

                }
            });

            view.findViewById(R.id.savings_month_layout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final DialogQuotaCreator dialogQuotaCreator = new DialogQuotaCreator(getContext(), getString(R.string.monthlySavings));
                    dialogQuotaCreator.setPositiveButton(new DialogReaction() {
                        @Override
                        public void onClick() {
                            String savingQuota = Objects.requireNonNull(dialogQuotaCreator.getDialogEditText().getText()).toString();
                            if (savingQuota.length() > 0) {
                                Double savingsDouble = Double.parseDouble(savingQuota);
                                if (savingsDouble < 0.0) {
                                    savingsDouble = 0.0;
                                }
                                presenter.updateMonthlySavings(preferenceStore, savingsDouble);
                                monthlySavingTextView.setText(amountFormatter.format(savingsDouble));
                                presenter.fetchTotalMonthlySavings(databaseStore, preferenceStore);
                                fundsChanged();
                            }
                            Keyboard.hide(getContext());
                        }
                    });
                    dialogQuotaCreator.showDialog();
                    Keyboard.show(getContext());
                }
            });
        }
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }

    private void setSavingsChartFragment() {
        if ((getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) ==
                Configuration.SCREENLAYOUT_SIZE_NORMAL
                && getResources().getConfiguration().orientation ==
                Configuration.ORIENTATION_LANDSCAPE) {
            Objects.requireNonNull(getView()).findViewById(R.id.savings_fragment).setVisibility(View.GONE);
        } else {
            Objects.requireNonNull(getView()).findViewById(R.id.savings_fragment).setVisibility(View.VISIBLE);
            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.savings_fragment, new SavingsChartsFragment());
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void setSavings(double savings) {
        savingsTextView.setText(amountFormatter.format(savings));
    }

    @Override
    public void setMonthlySavings(double monthlySavings) {
        monthlySavingTextView.setText(amountFormatter.format(monthlySavings));
    }

    @Override
    public void setTotalMonthlySavings(double totalMonthlySavings) {
        totalMonthlySavingsTextView.setText(amountFormatter.format(totalMonthlySavings));
    }

    private void fundsChanged() {
        actionAppListener.fundsChange();
    }

    private boolean isSavedSalary() {
        return Double.parseDouble(savedSalaryTextView.getText().toString()) > 0.0;
    }

    @Override
    public void setSavedSalary(double savedSalary) {
        savedSalaryTextView.setText(amountFormatter.format(savedSalary));
    }
}
