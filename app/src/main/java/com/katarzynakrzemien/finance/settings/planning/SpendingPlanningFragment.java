package com.katarzynakrzemien.finance.settings.planning;

import android.content.Context;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.katarzynakrzemien.finance.App;
import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.database.DatabaseQuery;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceFromDatabase;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.CategoryAndIncome;
import com.katarzynakrzemien.finance.dialog.DialogQuotaCreator;
import com.katarzynakrzemien.finance.dialog.DialogReaction;
import com.katarzynakrzemien.finance.utils.formatter.amount.AmountFormatter;
import com.katarzynakrzemien.finance.utils.Keyboard;
import com.katarzynakrzemien.finance.utils.recycler.ItemClickListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SpendingPlanningFragment extends Fragment implements SpendingPlanningContract.View, ItemClickListener {

    public static final String TAG = "SET_PLANNING";

    private List<CategoryAndIncome> setPlanningList = new ArrayList<>();
    private SpendingPlanningAdapter adapter;
    private SpendingPlanningContract.Presenter presenter = new SpendingPlanningPresenter();
    private DatabaseStore databaseStore;
    private AmountFormatter amountFormatter;
    private PreferenceStore preferenceStore;
    private View view = null;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        amountFormatter = ((App) context.getApplicationContext()).amountFormatter;
        databaseStore = new DatabaseQuery(context);
        preferenceStore = new PreferenceFromDatabase(context);
        presenter.attachView(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_spending_planning, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        view = getView();
        if (view != null) {

            RecyclerView recyclerView = view.findViewById(R.id
                    .set_planning_recycler_view);

            adapter = new SpendingPlanningAdapter(setPlanningList, Objects.requireNonNull(getContext()));
            adapter.setItemClickListener(this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
            presenter.fetchList(databaseStore);
            presenter.fetchAvailableFunds(databaseStore, preferenceStore);

        }
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }


    @Override
    public void renderList(List<CategoryAndIncome> list) {
        setPlanningList.clear();
        setPlanningList.addAll(list);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void setAvailableFunds(Double availableFunds) {

        TextView availableFundsTextView = view.findViewById(R.id.available_funds);
        availableFundsTextView.setText(amountFormatter.format(availableFunds));

        if (availableFunds < 0.0) {
            availableFundsTextView.setTextColor(Objects.requireNonNull(getActivity()).getResources().getColor(R.color.minus));
        } else {
            TypedValue typedValue = new TypedValue();
            if (Objects.requireNonNull(getContext()).getTheme().resolveAttribute(android.R.attr.textColorPrimary, typedValue, true)) {
                availableFundsTextView.setTextColor(typedValue.data);
            }
        }

    }

    @Override
    public void onItemClick(View view, int position) {

        final CategoryAndIncome model = setPlanningList.get(position);
        final DialogQuotaCreator dialogQuotaCreator = new DialogQuotaCreator(getContext(), model.getName());
        dialogQuotaCreator.setPositiveButton(new DialogReaction() {
            @Override
            public void onClick() {
                String plannedSpendingText = Objects.requireNonNull(dialogQuotaCreator.getDialogEditText().getText()).toString();
                if (plannedSpendingText.length() > 0) {
                    double plannedSpending = Double.parseDouble(plannedSpendingText);
                    if (plannedSpending < 0.0) {
                        plannedSpending = 0.0;
                    }
                    model.setIncome(plannedSpending);
                    presenter.updateListItem(databaseStore, model);
                    presenter.fetchList(databaseStore);
                    presenter.fetchAvailableFunds(databaseStore, preferenceStore);

                }
                Keyboard.hide(Objects.requireNonNull(getContext()));
            }
        });
        dialogQuotaCreator.showDialog();

        Keyboard.show(Objects.requireNonNull(getContext()));

    }

    @Override
    public void onItemLongClick(View view, int position) {

    }
}