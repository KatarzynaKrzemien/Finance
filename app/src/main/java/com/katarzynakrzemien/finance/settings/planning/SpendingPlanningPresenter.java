package com.katarzynakrzemien.finance.settings.planning;

import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.CategoryAndIncome;
import com.katarzynakrzemien.finance.database.models.FixedOutgoing;

import java.util.ArrayList;
import java.util.List;

public class SpendingPlanningPresenter implements SpendingPlanningContract.Presenter {

    private SpendingPlanningContract.View view = null;

    @Override
    public void fetchAvailableFunds(DatabaseStore databaseStore, PreferenceStore preferenceStore) {
        List<CategoryAndIncome> categoryList = databaseStore.getSpendingsCategories();
        double plannedSpending = 0.0;
        for (int i = 0; i < categoryList.size(); i++) {
            plannedSpending += categoryList.get(i).getIncome();
        }
        List<FixedOutgoing> fixedOutgoingList = databaseStore.getFixedOutgoings();
        double fixedOutgoings = 0.0;
        for (int i = 0; i < fixedOutgoingList.size(); i++) {
            fixedOutgoings += fixedOutgoingList.get(i).getOutcome();
        }
        view.setAvailableFunds(databaseStore.getSalarySum() - plannedSpending - fixedOutgoings - preferenceStore.getMonthlySavings());

    }

    @Override
    public void updateListItem(DatabaseStore databaseStore, CategoryAndIncome model) {
        databaseStore.updateSpendingCategory(new CategoryAndIncome(model.getId(),
                model.getName(), true, model.getIncome()));
    }

    @Override
    public void fetchList(DatabaseStore databaseStore) {
        List<CategoryAndIncome> categoryDatabaseList = databaseStore.getSpendingsCategories();
        List<CategoryAndIncome> categoryList = new ArrayList<>();
        for (int i = 0; i < categoryDatabaseList.size(); i++) {
            if (categoryDatabaseList.get(i).getActive()) {
                categoryList.add(categoryDatabaseList.get(i));
            }
        }
        view.renderList(categoryList);
    }

    @Override
    public void attachView(SpendingPlanningContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }
}
