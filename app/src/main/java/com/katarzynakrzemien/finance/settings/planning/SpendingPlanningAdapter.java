package com.katarzynakrzemien.finance.settings.planning;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.katarzynakrzemien.finance.App;
import com.katarzynakrzemien.finance.database.models.CategoryAndIncome;
import com.katarzynakrzemien.finance.utils.formatter.amount.AmountFormatter;
import com.katarzynakrzemien.finance.utils.recycler.ItemClickListener;
import com.katarzynakrzemien.finance.R;

import java.util.List;

public class SpendingPlanningAdapter extends RecyclerView.Adapter<SpendingPlanningAdapter.SpendingPlanningViewHolder> {

    private List<CategoryAndIncome> spendingPlanningList;
    private AmountFormatter amountFormatter;
    private ItemClickListener itemClickListener;

    class SpendingPlanningViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name, income;
        private LinearLayout rowLayout;

        SpendingPlanningViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name_row);
            income = view.findViewById(R.id.quota_row);
            rowLayout = view.findViewById(R.id.row_layout);
            rowLayout.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (itemClickListener != null) {
                itemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    SpendingPlanningAdapter(List<CategoryAndIncome> fixedOutgoingsList, Context context) {
        this.spendingPlanningList = fixedOutgoingsList;
        this.amountFormatter = ((App) context.getApplicationContext()).amountFormatter;

    }

    @Override
    public SpendingPlanningViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_quota, parent, false);
        return new SpendingPlanningViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SpendingPlanningViewHolder holder, int position) {
        CategoryAndIncome setPlanning = spendingPlanningList.get(position);
        holder.name.setText(setPlanning.getName());
        holder.income.setText(amountFormatter.format(setPlanning.getIncome()));
    }

    @Override
    public int getItemCount() {
        return spendingPlanningList.size();
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
}
