package com.katarzynakrzemien.finance.settings.salary;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.katarzynakrzemien.finance.App;
import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.database.models.Salary;
import com.katarzynakrzemien.finance.utils.formatter.amount.AmountFormatter;

import java.util.List;

public abstract class BaseSalaryAdapter<T extends Salary> extends RecyclerView.Adapter<BaseSalaryAdapter<T>.SalaryViewHolder> {
    protected List<T> salaryList;
    private AmountFormatter amountFormatter;
    private Resources.Theme theme;


    public BaseSalaryAdapter(List<T> salaryList, Context context) {
        this.salaryList = salaryList;
        this.amountFormatter = ((App) context.getApplicationContext()).amountFormatter;
        this.theme = context.getTheme();
    }

    @Override
    public SalaryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_quota, parent, false);
        return new SalaryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SalaryViewHolder holder, final int position) {
        final Salary salary = salaryList.get(position);
        holder.name.setText(salary.getName());
        holder.income.setText(amountFormatter.format(salary.getQuota()));
        int textColor = getTextColor(salary.getSave());
        holder.name.setTextColor(textColor);
        holder.income.setTextColor(textColor);
    }

    private int getTextColor(boolean save) {
        if (!save) {
            TypedValue typedValue = new TypedValue();
            if (theme.resolveAttribute(android.R.attr.textColorPrimary, typedValue, true)) {
                return typedValue.data;
            }
        }
        return Color.GRAY;
    }


    protected abstract View.OnClickListener getOnItemClick(int position);

    @Override
    public int getItemCount() {
        return salaryList.size();
    }

    public class SalaryViewHolder extends RecyclerView.ViewHolder {
        public TextView name, income;

        SalaryViewHolder(View view) {
            super(view);
            income = view.findViewById(R.id.quota_row);
            name = view.findViewById(R.id.name_row);
            view.setOnClickListener(getOnItemClick(getAdapterPosition()));
        }

    }
}