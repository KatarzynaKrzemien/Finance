package com.katarzynakrzemien.finance.settings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.widget.Toast;

import androidx.preference.CheckBoxPreference;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

import com.google.android.material.snackbar.Snackbar;
import com.katarzynakrzemien.finance.App;
import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.database.DatabaseQuery;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceFromDatabase;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.FixedOutgoing;
import com.katarzynakrzemien.finance.database.models.variants.Variants;
import com.katarzynakrzemien.finance.dialog.DialogNumberPicker;
import com.katarzynakrzemien.finance.dialog.DialogQuestionCreator;
import com.katarzynakrzemien.finance.dialog.DialogReaction;
import com.katarzynakrzemien.finance.dialog.DialogThemePicker;
import com.katarzynakrzemien.finance.dialog.DialogTimePicker;
import com.katarzynakrzemien.finance.fixedoutgoings.reminder.FixedOutgoingsWorkerManager;
import com.katarzynakrzemien.finance.managing.ManagingActivity;
import com.katarzynakrzemien.finance.managing.action.ActionAppListener;
import com.katarzynakrzemien.finance.managing.action.Clicked;
import com.katarzynakrzemien.finance.themes.Themes;
import com.katarzynakrzemien.finance.themes.ThemesManager;
import com.katarzynakrzemien.finance.utils.File;
import com.katarzynakrzemien.finance.utils.formatter.amount.AmountFormatter;
import com.katarzynakrzemien.finance.utils.formatter.date.DateFormatPattern;
import com.katarzynakrzemien.finance.utils.formatter.date.DateFormatter;
import com.katarzynakrzemien.finance.utils.formatter.date.SimpleDateFormatter;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;

public class SettingFragment extends PreferenceFragmentCompat implements SettingsContract.View, SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String TAG = "SETTINGS";

    private static final int FILE_SELECT_CODE = 0;
    private java.io.File database;
    private Preference buttonSetPlanning, buttonIncome, timeListPref, newMonthPref, styleChange, savingsPref;
    private CheckBoxPreference remindingFixedOutgoingsPref;
    private ListPreference variantsList;
    private PreferenceStore preferenceStore;
    private DatabaseStore databaseStore;
    private SettingsContract.Presenter presenter = new SettingsPresenter();
    private Variants variant;
    private AmountFormatter amountFormatter;
    private ThemesManager themesManager = new ThemesManager();
    private FixedOutgoingsWorkerManager fixedOutgoingsWorkerManager = FixedOutgoingsWorkerManager.getInstance();
    private ActionAppListener actionAppListener;
    private DateFormatter timeFormatter = new SimpleDateFormatter(DateFormatPattern.TIME);


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        preferenceStore = new PreferenceFromDatabase(context);
        databaseStore = new DatabaseQuery(context);
        database = context.getApplicationContext().getDatabasePath("Finance.db");
        amountFormatter = ((App) context.getApplicationContext()).amountFormatter;
        presenter.attachView(this);
        presenter.fetchVariant(preferenceStore);
        actionAppListener = ((ManagingActivity) context);
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.settings_preference, rootKey);
        initialize();
    }

    private void initialize() {
        initializeVariant();
        initializePlanningSettings();
        initializeIncome();
        initializeNewMonth();
        initializeSavings();
        initializeRemindingFixedOutgoings();
        initializeStyleChange();
        initializeExport();
        initializeImport();
    }

    private void initializeStyleChange() {
        styleChange = findPreference("style");
        styleChange.setIcon(R.drawable.ic_style);
        int idPref = PreferenceManager.getDefaultSharedPreferences(Objects.requireNonNull(getContext())).
                getInt("style", Themes.DARK.getIdPref());
        styleChange.setSummary(themesManager.getThemes(idPref).getIdString());

        styleChange.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference arg0) {
                final DialogThemePicker dialogThemePicker = new DialogThemePicker(getContext(), getString(R.string.style));
                dialogThemePicker.setPositiveButton(new DialogReaction() {
                    @Override
                    public void onClick() {
                        PreferenceManager.getDefaultSharedPreferences(getContext()).edit().
                                putInt("style", dialogThemePicker.getThemes().getIdPref()).apply();
                        Objects.requireNonNull(getActivity()).recreate();
                    }
                });
                dialogThemePicker.showDialog();
                return false;
            }
        });
    }

    private void initializeVariant() {
        variantsList = (ListPreference) findPreference("list_variant");
        variantsList.setDialogTitle(R.string.mode);
        variantsList.setDialogIcon(R.drawable.ic_monitoring_planning);
        variantsList.setIcon(R.drawable.ic_monitoring_planning);

        variantsList.setTitle(variant.getIntValue());
        if (variant.equals(Variants.MONITORING)) {
            variantsList.setValueIndex(0);
        } else if (variant.equals(Variants.PLANNING)) {
            variantsList.setValueIndex(1);
        } else if (variant.equals(Variants.FULLPLANNING)) {
            variantsList.setValueIndex(2);
        }
    }

    private void initializePlanningSettings() {
        buttonSetPlanning = findPreference("set_planning");

        buttonSetPlanning.setIcon(R.drawable.ic_planning);
        buttonSetPlanning.setTitle(R.string.planSpending);
        if (variant.equals(Variants.FULLPLANNING)) {
            buttonSetPlanning.setVisible(true);
        } else {
            buttonSetPlanning.setVisible(false);
        }

        buttonSetPlanning.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference arg0) {
                actionAppListener.itemClicked(Clicked.SPENDING_PLANNING);
                return false;
            }
        });
    }

    private void initializeIncome() {
        buttonIncome = findPreference("salary");

        buttonIncome.setIcon(R.drawable.ic_income);
        buttonIncome.setTitle(R.string.salary);
        presenter.fetchSalary(databaseStore);
        if (variant.equals(Variants.MONITORING)) {
            buttonIncome.setVisible(false);
        } else {
            buttonIncome.setVisible(true);
        }

        buttonIncome.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference arg0) {
                actionAppListener.itemClicked(Clicked.SALARY);
                return false;
            }
        });
    }

    private void initializeNewMonth() {
        newMonthPref = findPreference("new_month_preference");
        presenter.fetchDayOfNewMonth(preferenceStore);
        newMonthPref.setIcon(R.drawable.ic_new_month);

        newMonthPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference arg0) {
                final DialogNumberPicker dialogNumberPicker = new DialogNumberPicker(getContext(), getString(R.string.newMonth), getString(R.string.chooseDayOfMonth), R.drawable.ic_new_month, 1, 20);
                dialogNumberPicker.setPositiveButton(new DialogReaction() {
                    @Override
                    public void onClick() {
                        int number = dialogNumberPicker.getNumber();
                        newMonthPref.setSummary(getString(R.string.preferenceNewMonth) + " " + number);
                        presenter.updateDayOfNewMonth(preferenceStore, number);
                        actionAppListener.dayOfNewMonthChange();
                        dialogNumberPicker.setNumber(number);
                    }
                });
                dialogNumberPicker.showDialog();
                return false;
            }
        });
    }

    private void initializeSavings() {
        savingsPref = findPreference("savings_preference");
        presenter.fetchMonthlySavings(preferenceStore, databaseStore);
        savingsPref.setIcon(R.drawable.ic_savings);
        savingsPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                actionAppListener.itemClicked(Clicked.SAVINGS);
                return false;
            }
        });
    }

    private void initializeRemindingFixedOutgoings() {
        //initializeRemindingReplayFixedOutgoings();
        initializeRemindingTimeFixedOutgoings();
        remindingFixedOutgoingsPref = (CheckBoxPreference) findPreference("checkbox_reminding_fixed_outgoings");
        presenter.fetchRemindFixedOutgoings(preferenceStore);
        remindingFixedOutgoingsPref.setSummary(getString(R.string.remindingFixedOutgoings));
    }

  /*  private void initializeRemindingReplayFixedOutgoings() {
        listreplayFixedOutgoingsPref = (ListPreference) findPreference("list_replay_reminding_fixed_outgoings");

        listreplayFixedOutgoingsPref.setValueIndex(databaseQueryHelper.getReplayRemindFixedOutgoings());
        listreplayFixedOutgoingsPref.setIcon(R.drawable.ic_replay_reminding);
        if (listreplayFixedOutgoingsPref.findIndexOfValue(listreplayFixedOutgoingsPref.getValue()) == 0) {
            listreplayFixedOutgoingsPref.setSummary(listreplayFixedOutgoingsPref.getEntry().toString());
        } else {
            listreplayFixedOutgoingsPref.setSummary(getString(R.string.preferenceReplay) + " " + listreplayFixedOutgoingsPref.getEntry().toString());
        }

    }*/

    private void initializeRemindingTimeFixedOutgoings() {
        timeListPref = findPreference("time_reminding_fixed_outgoings");
        timeListPref.setIcon(R.drawable.ic_alarm);
        presenter.fetchTimeRemindFixedOutgoings(preferenceStore);

        timeListPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference arg0) {
                final DialogTimePicker dialogTimePicker = new DialogTimePicker(getContext(), getString(R.string.remindingTimeFixedOutgoings));
                dialogTimePicker.setPositiveButton(new DialogReaction() {
                    @Override
                    public void onClick() {
                        timeListPref.setSummary(dialogTimePicker.getTextTime());
                        presenter.updateTimeRemindFixedOutgoings(preferenceStore, dialogTimePicker.getCalendarTime());
                        disableNotificationsFixedOutgoings();
                        enableNotificationsFixedOutgoings();
                        dialogTimePicker.setTime(dialogTimePicker.getCalendarTime());

                    }
                });
                dialogTimePicker.showDialog();
                return false;
            }
        });
    }

    private void initializeImport() {

        Preference buttonImport = findPreference("import_button");

        buttonImport.setIcon(R.drawable.ic_import);
        buttonImport.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference arg0) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");
                try {
                    startActivityForResult(
                            Intent.createChooser(intent, "Select a File to Upload"),
                            FILE_SELECT_CODE);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), "Please install a File Manager.",
                            Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });

    }

    private void initializeExport() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Preference buttonExport = findPreference("export_button");

        buttonExport.setIcon(R.drawable.ic_export);
        buttonExport.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference arg0) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                String fileName = new SimpleDateFormat("DD.MM.YYYY").format(Calendar.getInstance().getTime()) + getString(R.string.app_name) + "DatabaseBackup";
                String fileExtension = ".db";
                try {
                    java.io.File temporaryFile = java.io.File.createTempFile
                            (fileName, fileExtension, Objects.requireNonNull(getActivity()).getExternalCacheDir());
                    FileInputStream base = new FileInputStream(database);
                    FileOutputStream tempFile = new FileOutputStream(temporaryFile);
                    File.copy(base, tempFile);
                    Uri uri = Uri.fromFile(temporaryFile);
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                    intent.setType("file*//*");
                    startActivity(Intent.createChooser(intent, getString(R.string.exportBackup)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        if (requestCode == FILE_SELECT_CODE) {
            if (resultCode == RESULT_OK) {
                final Uri uri = data.getData();
                String mimeType = Objects.requireNonNull(getContext()).getContentResolver().getType(Objects.requireNonNull(uri));
                if (mimeType != null) {
                    if (mimeType.equals("application/octet-stream")) {
                        DialogQuestionCreator dialogQuestionCreator = new DialogQuestionCreator(getContext(), getString(R.string.importBackup), getString(R.string.doYouWantImport) + " " + File.getFileName(getActivity(), uri) + " ?");
                        dialogQuestionCreator.setPositiveButton(new DialogReaction() {
                            @Override
                            public void onClick() {
                                try {
                                    FileOutputStream base = new FileOutputStream(database);
                                    FileInputStream inputStream = (FileInputStream) Objects.requireNonNull(getActivity()).
                                            getContentResolver().openInputStream(uri);
                                    File.copy(Objects.requireNonNull(inputStream), base);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                getActivity().recreate();
                            }
                        });
                        dialogQuestionCreator.showDialog();
                    } else {
                        Snackbar.make(getListView(), getString(R.string.badFileExtensions), Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        if (key.equals("list_variant")) {
            String variant = variantsList.getEntry().toString();
            variantsList.setTitle(variant);
            variantsList.getValue();
            if (variant.equals(getString(Variants.MONITORING.getIntValue()))) {
                buttonSetPlanning.setVisible(false);
                buttonIncome.setVisible(false);
                presenter.updateVariant(preferenceStore, Variants.MONITORING);
                this.variant = Variants.MONITORING;
            } else if (variant.equals(getString(Variants.PLANNING.getIntValue()))) {
                buttonIncome.setVisible(true);
                buttonSetPlanning.setVisible(false);
                presenter.updateVariant(preferenceStore, Variants.PLANNING);
                this.variant = Variants.PLANNING;

            } else if (variant.equals(getString(Variants.FULLPLANNING.getIntValue()))) {
                buttonSetPlanning.setVisible(true);
                buttonIncome.setVisible(true);
                presenter.updateVariant(preferenceStore, Variants.FULLPLANNING);
                this.variant = Variants.FULLPLANNING;
            }
            actionAppListener.variantChange(this.variant);
        }

        if (key.equals("checkbox_reminding_fixed_outgoings")) {

            if (sharedPreferences.getBoolean("checkbox_reminding_fixed_outgoings", true)) {
                presenter.updateRemindFixedOutgoings(preferenceStore, true);
                remindingFixedOutgoingsPref.setIcon(R.drawable.ic_notification_on);
                enableNotificationsFixedOutgoings();
                //listreplayFixedOutgoingsPref.setVisible(true);
                timeListPref.setVisible(true);


            } else {
                presenter.updateRemindFixedOutgoings(preferenceStore, false);
                remindingFixedOutgoingsPref.setIcon(R.drawable.ic_notification_off);
                disableNotificationsFixedOutgoings();
                // listreplayFixedOutgoingsPref.setVisible(false);
                timeListPref.setVisible(false);
            }
        }

    }

    private void enableNotificationsFixedOutgoings() {
        presenter.fetchFixedOutgoingsActiveDatedNoPaid(preferenceStore, databaseStore);
    }


    private void disableNotificationsFixedOutgoings() {
        fixedOutgoingsWorkerManager.cancelAll();
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }

    @Override
    public void onResume() {
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        initialize();
        super.onResume();

    }

    @Override
    public void onPause() {
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    @Override
    public void setVariant(Variants variant) {
        this.variant = variant;
    }

    @Override
    public void setSalary(double salary) {
        buttonIncome.setSummary(amountFormatter.format(salary));
    }

    @Override
    public void setDayOfNewMonth(int dayOfNewMonth) {
        newMonthPref.setSummary(getString(R.string.preferenceNewMonth) + " " + dayOfNewMonth);

    }

    @Override
    public void setMonthlySavings(double monthlySavings) {
        if (monthlySavings == 0.0) {
            savingsPref.setSummary(getString(R.string.noSavings));
        } else {
            savingsPref.setSummary(getString(R.string.preferenceSavings) + " " + amountFormatter.format(monthlySavings));
        }
    }

    @Override
    public void setRemindFixedOutgoings(boolean remindFixedOutgoings) {
        if (remindFixedOutgoings) {
            remindingFixedOutgoingsPref.setChecked(true);
            remindingFixedOutgoingsPref.setIcon(R.drawable.ic_notification_on);
            //listreplayFixedOutgoingsPref.setVisible(true);
            timeListPref.setVisible(true);

        } else {
            remindingFixedOutgoingsPref.setChecked(false);
            remindingFixedOutgoingsPref.setIcon(R.drawable.ic_notification_off);
            //listreplayFixedOutgoingsPref.setVisible(false);
            timeListPref.setVisible(false);
        }
    }

    @Override
    public void setTimeRemindFixedOutgoings(Calendar timeRemindFixedOutgoings) {
        timeListPref.setSummary(timeFormatter.formatToString(timeRemindFixedOutgoings));

    }

    @Override
    public void renderFixedOutgoingsActiveDatedNoPaid(List<FixedOutgoing> fixedOutgoingList, Calendar timeRemindFixedOutgoings) {

        for (int i = 0; i < fixedOutgoingList.size(); i++) {
            fixedOutgoingsWorkerManager.create(
                    fixedOutgoingList.get(i).getId(),
                    fixedOutgoingList.get(i).getDueDate(), timeRemindFixedOutgoings,
                    fixedOutgoingList.get(i).getName(),
                    amountFormatter.format(fixedOutgoingList.get(i).getOutcome()));

        }
    }
}
