package com.katarzynakrzemien.finance.settings;

import com.katarzynakrzemien.finance.BaseContract;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.FixedOutgoing;
import com.katarzynakrzemien.finance.database.models.variants.Variants;

import java.util.Calendar;
import java.util.List;

public class SettingsContract {

    public interface View extends BaseContract.View {

        void setVariant(Variants variant);

        void setSalary(double salary);

        void setDayOfNewMonth(int dayOfNewMonth);

        void setMonthlySavings(double monthlySavings);

        void setRemindFixedOutgoings(boolean remindFixedOutgoings);

        void setTimeRemindFixedOutgoings(Calendar timeRemindFixedOutgoings);

        void renderFixedOutgoingsActiveDatedNoPaid(List<FixedOutgoing> fixedOutgoingList, Calendar timeRemindFixedOutgoings);

    }

    public interface Presenter extends BaseContract.Presenter<View> {

        void fetchVariant(PreferenceStore preferenceStore);

        void fetchSalary(DatabaseStore databaseStore);

        void fetchDayOfNewMonth(PreferenceStore preferenceStore);

        void updateDayOfNewMonth(PreferenceStore preferenceStore, int dayOfNewMonth);

        void fetchMonthlySavings(PreferenceStore preferenceStore, DatabaseStore databaseStore);

        void fetchRemindFixedOutgoings(PreferenceStore preferenceStore);

        void updateRemindFixedOutgoings(PreferenceStore preferenceStore, boolean remindFixedOutgoings);

        void fetchTimeRemindFixedOutgoings(PreferenceStore preferenceStore);

        void updateTimeRemindFixedOutgoings(PreferenceStore preferenceStore, Calendar timeRemindFixedOutgoings);

        void updateVariant(PreferenceStore preferenceStore, Variants variant);

        void fetchFixedOutgoingsActiveDatedNoPaid(PreferenceStore preferenceStore, DatabaseStore databaseStore);

    }
}