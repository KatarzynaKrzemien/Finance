package com.katarzynakrzemien.finance.themes;

public class ThemesManager {

    public Themes getThemes(int idPref) {

        if ( Themes.LIGHT.getIdPref()== idPref) {
            return Themes.LIGHT;
        } else if (Themes.DARK_BLUE.getIdPref() == idPref) {
            return Themes.DARK_BLUE;
        } else {
            return Themes.DARK;
        }

    }
}
