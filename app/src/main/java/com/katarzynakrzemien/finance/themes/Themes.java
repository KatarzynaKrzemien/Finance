package com.katarzynakrzemien.finance.themes;

import com.katarzynakrzemien.finance.R;

public enum Themes {
    DARK(0,R.style.DarkAppTheme,R.string.darkStyle),
    LIGHT(1,R.style.LightAppTheme, R.string.lightStyle),
    DARK_BLUE(2,R.style.DarkBlueAppTheme, R.string.darkBlueStyle);

    private int idString;
    private int idTheme;
    private int idPref;

    Themes(int idPref, int idTheme,int idString) {
        this.idPref= idPref;
        this.idTheme = idTheme;
        this.idString = idString;
    }

    public int getIdString(){return idString;}


    public int getIdTheme(){return idTheme;}

    public int getIdPref(){return idPref;}
}
