package com.katarzynakrzemien.finance.help;


import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.utils.OnSearchListener;

import java.util.ArrayList;
import java.util.List;

public class HelpFragment extends Fragment implements OnSearchListener {

    public static final String TAG = "HELP";
    private HelpImagesAdapter adapter;
    private List<Help> helpList = new ArrayList<>();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        prepareData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_help, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        View view = getView();
        if (view != null) {
            prepareRecyclerView(view);
        }
    }

    private void prepareData() {
        helpList.add(new Help(getString(R.string.drawerHelpTitle), getString(R.string.drawerHelpDescription), R.drawable.drawer));
        helpList.add(new Help(getString(R.string.changeVariantTitle), getString(R.string.changeVariantDescription), R.drawable.change_variant));
        helpList.add(new Help(getString(R.string.monitoringTitle), getString(R.string.monitoringDescription), R.drawable.monitoring));
        helpList.add(new Help(getString(R.string.planningTitle), getString(R.string.planningDescription), R.drawable.planning));
        helpList.add(new Help(getString(R.string.enterSalaryTitle), getString(R.string.enterSalaryDescription), R.drawable.enter_salary));
        helpList.add(new Help(getString(R.string.enterPlanningTitle), getString(R.string.enterPlanningDescription), R.drawable.enter_planning_income));
        helpList.add(new Help(getString(R.string.remainingColorTitle), getString(R.string.remainingColorDescription), R.drawable.remaining));
        helpList.add(new Help(getString(R.string.onlySalaryTitle), getString(R.string.onlySalaryDescription), R.drawable.on_off_onlysalary));
        helpList.add(new Help(getString(R.string.activeTitle), getResources().getString(R.string.activeDescription), R.drawable.active));
        helpList.add(new Help(getString(R.string.addCategoryTitle), getString(R.string.addCategoryDescription), R.drawable.add_category));
        helpList.add(new Help(getString(R.string.deleteCategoryTitle), getString(R.string.deleteCategoryDescription), R.drawable.delete_category));
        helpList.add(new Help(getString(R.string.enterOutcomeTitle), getString(R.string.enterOutcomeDescription), R.drawable.enter_outcome));
        helpList.add(new Help(getString(R.string.enterFixedOutgoingsTitle), getString(R.string.enterFixedOutgoingsDescription), R.drawable.enter_fixed_outgoings));
        helpList.add(new Help(getString(R.string.newMonthTitle), getString(R.string.newMonthDescription), R.drawable.new_month));
        helpList.add(new Help(getString(R.string.saveHelpTitle), getString(R.string.saveHelpDescription), R.drawable.save));
        helpList.add(new Help(getString(R.string.tagChartsTitle), getString(R.string.tagChartsDescription), R.drawable.tag_chart));
        helpList.add(new Help(getString(R.string.currentChartTitle), getString(R.string.currentChartDescription), R.drawable.current_chart));
        helpList.add(new Help(getString(R.string.archiveChartTitle), getString(R.string.archiveChartDescription), R.drawable.gesture_chart));
        helpList.add(new Help(getString(R.string.activeTitle), getString(R.string.activeDescription), R.drawable.pick_archive));
        helpList.add(new Help(getString(R.string.archiveAttributesTitle), getString(R.string.archiveAttributesDescription), R.drawable.archive_attributes));
        helpList.add(new Help(getString(R.string.importExportHelpTitle), getString(R.string.importExportHelpDescription), R.drawable.export));
    }

    void prepareRecyclerView(View view) {
        RecyclerView helpRecycler = view.findViewById(R.id.recycler_view);
        adapter = new HelpImagesAdapter(helpList);
        helpRecycler.setAdapter(adapter);

        if ((getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) ==
                Configuration.SCREENLAYOUT_SIZE_XLARGE
                && getResources().getConfiguration().orientation ==
                Configuration.ORIENTATION_LANDSCAPE
        ) {
            GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
            helpRecycler.setLayoutManager(layoutManager);
        } else if (((getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) ==
                Configuration.SCREENLAYOUT_SIZE_LARGE
                && getResources().getConfiguration().orientation ==
                Configuration.ORIENTATION_LANDSCAPE)
                ||
                ((getResources().getConfiguration().screenLayout &
                        Configuration.SCREENLAYOUT_SIZE_MASK) ==
                        Configuration.SCREENLAYOUT_SIZE_XLARGE
                        && getResources().getConfiguration().orientation ==
                        Configuration.ORIENTATION_PORTRAIT)

        ) {
            GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
            helpRecycler.setLayoutManager(layoutManager);
        } else {
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
            helpRecycler.setLayoutManager(layoutManager);
        }

    }

    @Override
    public void setFilterText(String filterText) {
        adapter.getFilter().filter(filterText);
    }
}


