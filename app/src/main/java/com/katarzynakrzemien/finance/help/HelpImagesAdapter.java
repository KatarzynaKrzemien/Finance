package com.katarzynakrzemien.finance.help;

import android.graphics.drawable.Drawable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.katarzynakrzemien.finance.R;

import java.util.ArrayList;
import java.util.List;

public class HelpImagesAdapter extends RecyclerView.Adapter<HelpImagesAdapter.ViewHolder> implements Filterable {

    private List<Help> helpList;
    private List<Help> helpListFiltered;

    HelpImagesAdapter(List<Help> helpList) {
        this.helpList = helpList;
        this.helpListFiltered = helpList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private CardView cardView;

        public ViewHolder(CardView v) {
            super(v);
            cardView = v;
        }
    }

    @Override
    public HelpImagesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView cv = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_help, parent, false);
        return new ViewHolder(cv);
    }

    @Override
    public void onBindViewHolder(HelpImagesAdapter.ViewHolder holder, int position) {
        CardView cardView = holder.cardView;
        Help help = helpListFiltered.get(position);
        TextView titleHelp = cardView.findViewById(R.id.help_title);
        titleHelp.setText(help.getTitle());
        ImageView imageView = cardView.findViewById(R.id.help_image);
        Drawable drawable = cardView.getResources().getDrawable(help.getImageId());
        imageView.setImageDrawable(drawable);
        imageView.setContentDescription(help.getDescription());
        TextView textView = cardView.findViewById(R.id.help_description);
        textView.setText(help.getDescription());
    }

    @Override
    public int getItemCount() {
        return helpListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    helpListFiltered = helpList;
                } else {
                    List<Help> filteredList = new ArrayList<>();
                    for (Help row : helpList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTitle().toLowerCase().contains(charString.toLowerCase()) || row.getDescription().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    helpListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = helpListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                helpListFiltered = (ArrayList<Help>) filterResults.values;

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }
}
