package com.katarzynakrzemien.finance.database.models.variants;

import com.katarzynakrzemien.finance.R;

public enum Variants {
    MONITORING("monitoring", R.string.monitoring),
    PLANNING("planning", R.string.planning),
    FULLPLANNING("full planning", R.string.fullPlanning);

    private String stringValue;
    private int intValue;

    Variants(String toString, int value) {
        stringValue = toString;
        intValue = value;
    }

    @Override
    public String toString() {
        return stringValue;
    }

    public int getIntValue(){return intValue;}
}
