package com.katarzynakrzemien.finance.database.models;

public class Salary extends BaseModel {
    private Boolean save;

    public Salary(Integer id, String name, double income, Boolean save) {
        super(id, name, income);
        this.save = save;
    }

    public Boolean getSave() {
        return save;
    }

    public void setSave(Boolean save) {
        this.save = save;
    }
}
