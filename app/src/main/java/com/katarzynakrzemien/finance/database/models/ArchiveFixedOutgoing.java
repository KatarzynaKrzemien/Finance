package com.katarzynakrzemien.finance.database.models;

import java.util.List;

public class ArchiveFixedOutgoing extends CategorizedFixedOutgoing {

    private int idPeriod;

    public ArchiveFixedOutgoing(String name, int idPeriod, List<FixedOutgoing> fixedOutgoingList) {
        super(name, fixedOutgoingList);
        this.idPeriod = idPeriod;
    }

    public int getIdPeriod() {
        return idPeriod;
    }
}
