package com.katarzynakrzemien.finance.database.models;

public class CategoryAndIncome extends Category {

    private Double income;

    public CategoryAndIncome(Integer id, String name, Boolean active, Double income) {
        super(id, name, active);
        this.income = income;
    }

    public Double getIncome() {
        return income;
    }

    public void setIncome(Double income) {
        this.income = income;
    }
}
