package com.katarzynakrzemien.finance.database.models;

import java.util.List;

public class TotalSalary {

    private List<Salary> salaryList;
    private Salary fromSavings;
    private Salary lastMonthRemainingFunds;

    public TotalSalary(List<Salary> salaryList, Salary fromSavings, Salary lastMonthRemainingFunds) {
        this.salaryList = salaryList;
        this.fromSavings = fromSavings;
        this.lastMonthRemainingFunds = lastMonthRemainingFunds;
    }

    public Salary getFromSavings() {
        return fromSavings;
    }

    public Salary getLastMonthRemainingFunds() {
        return lastMonthRemainingFunds;
    }

    public int getSalaryListSize() {
        return salaryList.size();
    }

    public Salary get(int position) {
        return salaryList.get(position);
    }

    public void set(int position, Salary salary) {
        salaryList.set(position, salary);
    }

}
