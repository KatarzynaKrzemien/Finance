package com.katarzynakrzemien.finance.database.models;

import java.util.Calendar;
import java.util.Comparator;

public class FixedOutgoing {

    private Integer id, idCategoryOfFO;
    private String name;
    private Calendar dueDate, paymentDate;
    private double outcome;
    private boolean isPaid;

    public FixedOutgoing(Integer id, Integer idCategoryOfFO, String name, double outcome, Calendar dueDate, Calendar paymentDate, boolean isPaid) {
        this.id = id;
        this.idCategoryOfFO = idCategoryOfFO;
        this.name = name;
        this.outcome = outcome;
        this.dueDate = dueDate;
        this.paymentDate = paymentDate;
        this.isPaid = isPaid;
    }

    public Integer getId() {
        return id;
    }

    public Integer getIdCategoryOfFO() {
        return idCategoryOfFO;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getOutcome() {
        return outcome;
    }

    public void setOutcome(double outcome) {
        this.outcome = outcome;
    }

    public Calendar getDueDate() {
        return dueDate;
    }

    public void setDueDate(Calendar dueDate) {
        this.dueDate = dueDate;
    }

    public Calendar getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Calendar paymentDate) {
        this.paymentDate = paymentDate;
    }

    public boolean getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(boolean isPaid) {
        this.isPaid = isPaid;
    }


    public static class DueDateCompare implements Comparator<FixedOutgoing> {

        @Override
        public int compare(FixedOutgoing o1, FixedOutgoing o2) {
            if (o1.getDueDate() != null && o2.getDueDate() != null) {
                return o1.getDueDate().compareTo(o2.getDueDate());
            } else if (o1.getDueDate() == null && o2.getDueDate() != null) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    public static class PaidStatusCompare implements Comparator<FixedOutgoing> {

        @Override
        public int compare(FixedOutgoing o1, FixedOutgoing o2) {
            if (o1.getIsPaid() && o2.getIsPaid()) {
                return 0;
            } else if (!o1.getIsPaid() && !o2.getIsPaid()) {
                return 0;
            } else if (o1.getIsPaid() || !o2.getIsPaid()) {
                return 1;
            } else {
                return -1;
            }
        }
    }


}
