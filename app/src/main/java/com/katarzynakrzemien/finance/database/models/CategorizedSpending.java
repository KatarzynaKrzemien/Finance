package com.katarzynakrzemien.finance.database.models;

import java.util.List;

public class CategorizedSpending {

    private List<Spending> spendingList;
    private String categoryName;
    private Double income;

    public CategorizedSpending(String categoryName, double income, List<Spending> spendingList) {
        this.categoryName = categoryName;
        this.income = income;
        this.spendingList = spendingList;
    }

    public double getSum() {
        double sum = 0.0;
        for (int i = 0; i < spendingList.size(); i++) {
            sum += spendingList.get(i).getQuota();
        }
        return sum;
    }

    public double getIncome() {
        return income;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public Integer getId() {
        return get(0).getId();
    }

    public int getSpendingListSize() {
        return spendingList.size();
    }

    public Spending get(int position) {
        return spendingList.get(position);
    }

    public void set(int position, Spending spending) {
        spendingList.set(position, spending);
    }
}
