package com.katarzynakrzemien.finance.database.models;

public class BaseModel {
    private Integer id;
    private String name;
    private double quota;

    public BaseModel(Integer id, String name, double quota) {
        this.id = id;
        this.name = name;
        this.quota = quota;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getQuota() {
        return quota;
    }

    public void setQuota(Double quota) {
        this.quota = quota;
    }

    public Integer getId() {
        return id;
    }
}
