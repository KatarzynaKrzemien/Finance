package com.katarzynakrzemien.finance.database;

import com.katarzynakrzemien.finance.database.models.variants.Variants;

import java.util.Calendar;

public interface PreferenceStore {

    Variants getVariant();

    void setVariant(Variants variant);

    Double getSavings();

    void setSavings(double savings);

    Double getMonthlySavings();

    void setMonthlySavings(double monthlySavings);

    int getDayOfNewMonth();

    void setDayOfNewMonth(int day);

    Boolean getRemindingFixedOutgoings();

    void setRemindingFixedOutgoings(Boolean reminding);

    Calendar getRemindingTimeFixedOutgoings();

    void setRemindingTimeFixedOutgoings(Calendar remindingTimeFixedOutgoings);

    Boolean getReplayRemindingFixedOutgoings();

    void getReplayRemindingFixedOutgoings(Boolean replayReminding);


}
