package com.katarzynakrzemien.finance.database.models;

public class Summary {

    private String name;
    private Double quota;

    public Summary(String name, Double quota) {
        this.name = name;
        this.quota = quota;
    }

    public String getName() {
        return name;
    }

    public Double getQuota() {
        return quota;
    }
}
