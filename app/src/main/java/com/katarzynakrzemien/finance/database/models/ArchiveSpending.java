package com.katarzynakrzemien.finance.database.models;

public class ArchiveSpending {

    private Integer id;
    private int idPeriod;
    private String name;
    private double outcome, income;

    public ArchiveSpending(Integer id, int idPeriod, String name, double outcome, double income) {
        this.id = id;
        this.idPeriod = idPeriod;
        this.name = name;
        this.outcome = outcome;
        this.income = income;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getIdPeriod() {
        return idPeriod;
    }

    public double getOutcome() {
        return outcome;
    }

    public double getIncome() {
        return income;
    }
}
