package com.katarzynakrzemien.finance.database.models;

import android.annotation.TargetApi;
import android.os.Build;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;

public class CategorizedFixedOutgoing {

    private List<FixedOutgoing> fixedOutgoingList;
    private String name;

    public CategorizedFixedOutgoing(String name, List<FixedOutgoing> fixedOutgoingList) {
        this.name = name;
        this.fixedOutgoingList = fixedOutgoingList;
    }

    public double getSum() {
        double sum = 0.0;
        for (FixedOutgoing fixedOutgoing : fixedOutgoingList) {
            sum += fixedOutgoing.getOutcome();
        }
        return sum;
    }

    public double getPaidSum() {
        double sum = 0.0;
        for (FixedOutgoing fixedOutgoing : fixedOutgoingList) {
            if (fixedOutgoing.getIsPaid()) {
                sum += fixedOutgoing.getOutcome();
            }
        }
        return sum;
    }

    public double getUnpaidSum() {
        double sum = 0.0;
        for (FixedOutgoing fixedOutgoing : fixedOutgoingList) {
            if (!fixedOutgoing.getIsPaid()) {
                sum += fixedOutgoing.getOutcome();
            }
        }
        return sum;
    }

    public int getCountOfPaidFixedOutgoings() {
        int paid = 0;
        for (int i = 0; i < fixedOutgoingList.size(); i++) {
            if (fixedOutgoingList.get(i).getIsPaid()) {
                paid++;
            }
        }
        return paid;
    }

    public boolean getIsAllPaid() {
        return getCountOfPaidFixedOutgoings() == getFixedOutgoingsListSize();
    }

    public String getName() {
        return name;
    }

    public int getFixedOutgoingsListSize() {
        return fixedOutgoingList.size();
    }

    public FixedOutgoing get(int position) {
        return fixedOutgoingList.get(position);
    }

    public List<FixedOutgoing> getList() {
        return fixedOutgoingList;
    }


    public void set(int position, FixedOutgoing spending) {
        fixedOutgoingList.set(position, spending);
    }

    @TargetApi(Build.VERSION_CODES.N)
    private Calendar getClosestDueDateUnpaidFO() {
        List<FixedOutgoing> list = new ArrayList<>(fixedOutgoingList);
        list.sort(new FixedOutgoing.DueDateCompare());
        return list.get(0).getDueDate();
    }

    public static class DueDateCompare implements Comparator<CategorizedFixedOutgoing> {

        @Override
        public int compare(CategorizedFixedOutgoing o1, CategorizedFixedOutgoing o2) {
            if (o1.getClosestDueDateUnpaidFO() != null && o2.getClosestDueDateUnpaidFO() != null) {
                return o1.getClosestDueDateUnpaidFO().compareTo(o2.getClosestDueDateUnpaidFO());
            } else if (o1.getClosestDueDateUnpaidFO() == null && o2.getClosestDueDateUnpaidFO() != null) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    public static class PaidStatusCompare implements Comparator<CategorizedFixedOutgoing> {

        @Override
        public int compare(CategorizedFixedOutgoing o1, CategorizedFixedOutgoing o2) {
            if (o1.getIsAllPaid() && o2.getIsAllPaid()) {
                return 0;
            } else if (!o1.getIsAllPaid() && !o2.getIsAllPaid()) {
                return 0;
            } else if (o1.getIsAllPaid() || !o2.getIsAllPaid()) {
                return 1;
            } else {
                return -1;
            }
        }
    }
}
