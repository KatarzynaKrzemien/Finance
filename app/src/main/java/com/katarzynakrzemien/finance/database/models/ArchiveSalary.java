package com.katarzynakrzemien.finance.database.models;

public class ArchiveSalary extends Salary {

    private int idPeriod;

    public ArchiveSalary(Integer id, int idPeriod, String name, double income, Boolean active) {
        super(id, name, income, active);
        this.idPeriod = idPeriod;
    }


    public int getIdPeriod() {
        return idPeriod;
    }
}

