package com.katarzynakrzemien.finance.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.database.models.variants.Variants;

public class FinanceDatabaseHelper extends SQLiteOpenHelper {

    static final String _id = "_id";
    static final String VARS_TABLE = "VARS";
    static final String VARS_NAME = "VAR_NAME";
    static final String VARS_VALUE = "VALUE";
    static final String VARS_VALUE_DAY_NEW_MONTH = "NEW";
    static final String VARS_VALUE_SAVINGS = "SAVE";
    static final String VARS_VALUE_MONTHLY_SAVINGS = "SAVE_MONTH";
    static final String VARS_VALUE_VARIANT = "VARIANT";
    static final String VARS_VALUE_REMINDING_FIXED_OUTGOINGS = "REMINDING_FIXED_OUTGOINGS";
    static final String VARS_VALUE_TIME_REMINDING_FIXED_OUTGOINGS = "TIME_REMINDING_FIXED_OUTGOINGS";
    static final String VARS_VALUE_REPLAY_REMINDING_FIXED_OUTGOINGS = "REPLAY_REMINDING_FIXED_OUTGOINGS";
    static final String SALARY_TABLE = "SALARY";
    static final String SALARY_NAME = "NAME";
    static final String SALARY_INCOME = "INCOME";
    static final String SALARY_SAVE = "SAVE";
    static final String CATEGORIES_TABLE = "CATEGORIES";
    static final String CATEGORIES_NAME = "CATEGORY";
    static final String CATEGORIES_INCOME = "INCOME";
    static final String CATEGORIES_ACTIVE = "ACTIVE";
    static final String FIXED_OUTGOINGS_TABLE = "FIXED_OUTGOINGS";
    static final String FIXED_OUTGOINGS_NAME = "FIXED_OUTGOING";
    static final String FIXED_OUTGOINGS_ACTIVE = "ACTIVE";
    static final String CURRENT_FIXED_OUTGOINGS_TABLE = "CURRENT_FIXED_OUTGOINGS";
    static final String CURRENT_FIXED_OUTGOINGS_ID_FO = "ID_FO";
    static final String CURRENT_FIXED_OUTGOINGS_OUTCOME = "OUTCOME";
    static final String CURRENT_FIXED_OUTGOINGS_DUE_DATE = "DUE_DATE";
    static final String CURRENT_FIXED_OUTGOINGS_PAYMENT_DATE = "PAYMENT_DATE";
    static final String CURRENT_FIXED_OUTGOINGS_PAY = "PAY";
    static final String PERIOD_TABLE = "PERIOD";
    static final String PERIOD_START_DATE = "START_DATE";
    static final String PERIOD_END_DATE = "END_DATE";
    static final String PERIOD_VARIANT = "VARIANT";
    static final String PERIOD_SAVINGS = "SAVINGS";
    static final String ARCHIVE_SPENDINGS_TABLE = "ARCHIVE_SPENDINGS";
    static final String ARCHIVE_SPENDINGS_ID_PERIOD = "ID_PERIOD";
    static final String ARCHIVE_SPENDINGS_NAME_CATEGORY = "NAME_CATEGORY";
    static final String ARCHIVE_SPENDINGS_OUTCOME = "OUTCOME";
    static final String ARCHIVE_SPENDINGS_INCOME = "INCOME";
    static final String ARCHIVE_FIXED_OUTGOINGS_TABLE = "ARCHIVE_FIXED_OUTGOINGS";
    static final String ARCHIVE_FIXED_OUTGOINGS_ID_PERIOD = "ID_PERIOD";
    static final String ARCHIVE_FIXED_OUTGOINGS_NAME = "NAME_FIXED_OUTGOING";
    static final String ARCHIVE_FIXED_OUTGOINGS_OUTCOME = "OUTCOME";
    static final String ARCHIVE_FIXED_OUTGOINGS_DUE_DATE = "DUE_DATE";
    static final String ARCHIVE_FIXED_OUTGOINGS_PAYMENT_DATE = "PAYMENT_DATE";
    static final String ARCHIVE_FIXED_OUTGOINGS_PAY = "PAY";
    static final String ARCHIVE_SALARY_TABLE = "ARCHIVE_SALARY";
    static final String ARCHIVE_SALARY_ID_PERIOD = "ID_PERIOD";
    static final String ARCHIVE_SALARY_NAME = "NAME_SALARY";
    static final String ARCHIVE_SALARY_INCOME = "INCOME";
    static final String ARCHIVE_SALARY_SAVE = "SAVE";
    static final String CURRENT_SPENDINGS_TABLE = "CURRENT_SPENDINGS";
    static final String CURRENT_SPENDINGS_ID_CAT = "ID_CAT";
    static final String CURRENT_SPENDINGS_OUTCOME = "OUTCOME";
    static final String CURRENT_SPENDINGS_DATE = "DATE";
    static final String idLastMonth = "1";
    static final String idFromSavings = "2";
    static final String NODATE = "null";
    private static final String DBNAME = "Finance.db";
    private static final int VERSION = 2;
    private Context context;

    FinanceDatabaseHelper(Context c) {
        super(c, DBNAME, null, VERSION);
        context = c;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
        initialiseDefaultData(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private void createTables(SQLiteDatabase db) {
        createVarsTable(db);
        createSalaryTable(db);
        createCategoriesTable(db);
        createCurrentFixedOutgoingsTable(db);
        createFixedOutgoingsTable(db);
        createPeriodsTable(db);
        createArchiveSpendingsTable(db);
        createArchiveFixedOutgoingsTable(db);
        createArchiveSalaryTable(db);
        createCurrentSpendingsTable(db);
    }

    private void createVarsTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + VARS_TABLE + "(_id INTEGER PRIMARY KEY, "
                + VARS_NAME + " TEXT, "
                + VARS_VALUE + " TEXT);");
    }

    private void createSalaryTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + SALARY_TABLE + " (_id INTEGER PRIMARY KEY, "
                + SALARY_NAME + " TEXT, "
                + SALARY_INCOME + " REAL, "
                + SALARY_SAVE + " INT);");
    }

    private void createCategoriesTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + CATEGORIES_TABLE + "(_id INTEGER PRIMARY KEY, "
                + CATEGORIES_NAME + " TEXT, "
                + CATEGORIES_INCOME + " REAL, "
                + CATEGORIES_ACTIVE + " INTEGER);");
    }

    private void createFixedOutgoingsTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + FIXED_OUTGOINGS_TABLE + "(_id INTEGER PRIMARY KEY, "
                + FIXED_OUTGOINGS_NAME + " TEXT, "
                + FIXED_OUTGOINGS_ACTIVE + " INTEGER);");
    }

    private void createCurrentFixedOutgoingsTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + CURRENT_FIXED_OUTGOINGS_TABLE + " (_id INTEGER PRIMARY KEY, "
                + CURRENT_FIXED_OUTGOINGS_ID_FO + " INT, "
                + CURRENT_FIXED_OUTGOINGS_OUTCOME + " REAL, "
                + CURRENT_FIXED_OUTGOINGS_DUE_DATE + " TEXT,"
                + CURRENT_FIXED_OUTGOINGS_PAYMENT_DATE + " TEXT,"
                + CURRENT_FIXED_OUTGOINGS_PAY + " INTEGER);");
    }

    private void createPeriodsTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + PERIOD_TABLE + "(_id INTEGER PRIMARY KEY, "
                + PERIOD_START_DATE + " TEXT, "
                + PERIOD_END_DATE + " TEXT, "
                + PERIOD_VARIANT + " TEXT,"
                + PERIOD_SAVINGS + " REAL);");
    }

    private void createArchiveSpendingsTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + ARCHIVE_SPENDINGS_TABLE + "(_id INTEGER PRIMARY KEY, "
                + ARCHIVE_SPENDINGS_ID_PERIOD + " INTEGER REFERENCES PERIOD(_id), "
                + ARCHIVE_SPENDINGS_NAME_CATEGORY + " TEXT, "
                + ARCHIVE_SPENDINGS_OUTCOME + " REAL, "
                + ARCHIVE_SPENDINGS_INCOME + " REAL);");
    }

    private void createArchiveFixedOutgoingsTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + ARCHIVE_FIXED_OUTGOINGS_TABLE + "(_id INTEGER PRIMARY KEY, "
                + ARCHIVE_FIXED_OUTGOINGS_ID_PERIOD + " INTEGER REFERENCES PERIOD(_id), "
                + ARCHIVE_FIXED_OUTGOINGS_NAME + " TEXT, "
                + ARCHIVE_FIXED_OUTGOINGS_OUTCOME + " REAL, "
                + ARCHIVE_FIXED_OUTGOINGS_DUE_DATE + " TEXT, "
                + ARCHIVE_FIXED_OUTGOINGS_PAYMENT_DATE + " TEXT, "
                + ARCHIVE_FIXED_OUTGOINGS_PAY + " INTEGER);");
    }

    private void createArchiveSalaryTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + ARCHIVE_SALARY_TABLE + "(_id INTEGER PRIMARY KEY, "
                + ARCHIVE_SALARY_ID_PERIOD + " INTEGER REFERENCES PERIOD(_id), "
                + ARCHIVE_SALARY_NAME + " TEXT, "
                + ARCHIVE_SALARY_INCOME + " REAL, "
                + ARCHIVE_SALARY_SAVE + " INT);");
    }

    private void createCurrentSpendingsTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + CURRENT_SPENDINGS_TABLE + "(_id INTEGER PRIMARY KEY, "
                + CURRENT_SPENDINGS_ID_CAT + " INTEGER REFERENCES CATEGORIES(_id),"
                + CURRENT_SPENDINGS_DATE + " TEXT, "
                + CURRENT_SPENDINGS_OUTCOME + " REAL);");
    }

    private void initialiseDefaultData(SQLiteDatabase db) {
        initialiseVarsTable(db);
        initialiseSalaryTable(db);
        initialiseCategoriesTable(db);
        initialiseFixedOutgoingsTable(db);
    }

    private void initialiseVarsTable(SQLiteDatabase db) {
        insertVars(db, VARS_VALUE_DAY_NEW_MONTH, "1");
        insertVars(db, VARS_VALUE_SAVINGS, "0.0");
        insertVars(db, VARS_VALUE_MONTHLY_SAVINGS, "0.0");
        insertVars(db, VARS_VALUE_VARIANT, Variants.MONITORING.toString());
        insertVars(db, VARS_VALUE_REMINDING_FIXED_OUTGOINGS, "0");
        insertVars(db, VARS_VALUE_TIME_REMINDING_FIXED_OUTGOINGS, "12:00");
        insertVars(db, VARS_VALUE_REPLAY_REMINDING_FIXED_OUTGOINGS, "0");
    }

    private void initialiseFixedOutgoingsTable(SQLiteDatabase db) {
        insertFixedOutgoing(db, context.getResources().getString(R.string.rent), false);
        insertFixedOutgoing(db, context.getResources().getString(R.string.water), false);
        insertFixedOutgoing(db, context.getResources().getString(R.string.gas), false);
        insertFixedOutgoing(db, context.getResources().getString(R.string.power), false);
    }

    private void initialiseCategoriesTable(SQLiteDatabase db) {
        insertCategory(db, context.getResources().getString(R.string.food), false, 0.0);
        insertCategory(db, context.getResources().getString(R.string.fuel), false, 0.0);
        insertCategory(db, context.getResources().getString(R.string.cleaning_supplies), false, 0.0);
        insertCategory(db, context.getResources().getString(R.string.entertainment), false, 0.0);
        insertCategory(db, context.getResources().getString(R.string.home), false, 0.0);
        insertCategory(db, context.getResources().getString(R.string.cosmetics), false, 0.0);
        insertCategory(db, context.getResources().getString(R.string.clothes), false, 0.0);
    }

    private void initialiseSalaryTable(SQLiteDatabase db) {
        insertSalary(db, context.getResources().getString(R.string.lastMonth), 0, false);
        insertSalary(db, context.getResources().getString(R.string.fromSavings), 0, false);
    }

    private void insertVars(SQLiteDatabase db, String name, String value) {
        ContentValues values = new ContentValues();
        values.put(VARS_NAME, name);
        values.put(VARS_VALUE, value);
        db.insert(VARS_TABLE, null, values);
    }

    void insertSalary(SQLiteDatabase db, String name, double income, boolean isSaved) {
        ContentValues values = new ContentValues();
        values.put(SALARY_NAME, name);
        values.put(SALARY_INCOME, income);
        if (isSaved) {
            values.put(SALARY_SAVE, 1);
        } else {
            values.put(SALARY_SAVE, 0);
        }
        db.insert(SALARY_TABLE, null, values);
    }

    void insertCategory(SQLiteDatabase db, String category, boolean isActive, double income) {
        ContentValues values = new ContentValues();
        values.put(CATEGORIES_NAME, category);
        values.put(CATEGORIES_INCOME, income);
        if (isActive) {
            values.put(CATEGORIES_ACTIVE, 1);
        } else {
            values.put(CATEGORIES_ACTIVE, 0);
        }
        db.insert(CATEGORIES_TABLE, null, values);
    }

    void insertFixedOutgoing(SQLiteDatabase db, String fixedOutgoing, boolean isActive) {
        ContentValues values = new ContentValues();
        values.put(FIXED_OUTGOINGS_NAME, fixedOutgoing);
        if (isActive) {
            values.put(FIXED_OUTGOINGS_ACTIVE, 1);
        } else {
            values.put(FIXED_OUTGOINGS_ACTIVE, 0);
        }
        db.insert(FIXED_OUTGOINGS_TABLE, null, values);
    }

    void insertCurrentFixedOutgoing(SQLiteDatabase db, int idFO, double outcome, String dueDate, String paymentDate, boolean isPaid) {
        ContentValues values = new ContentValues();
        values.put(CURRENT_FIXED_OUTGOINGS_ID_FO, idFO);
        values.put(CURRENT_FIXED_OUTGOINGS_OUTCOME, outcome);
        values.put(CURRENT_FIXED_OUTGOINGS_DUE_DATE, dueDate);
        values.put(CURRENT_FIXED_OUTGOINGS_PAYMENT_DATE, paymentDate);
        if (isPaid) {
            values.put(CURRENT_FIXED_OUTGOINGS_PAY, 1);
        } else {
            values.put(CURRENT_FIXED_OUTGOINGS_PAY, 0);
        }
        db.insert(CURRENT_FIXED_OUTGOINGS_TABLE, null, values);
    }

    void insertCurrentSpending(SQLiteDatabase db, int idCat, double outcome, String date) {
        ContentValues values = new ContentValues();
        values.put(CURRENT_SPENDINGS_ID_CAT, idCat);
        values.put(CURRENT_SPENDINGS_OUTCOME, outcome);
        values.put(CURRENT_SPENDINGS_DATE, date);
        db.insert(CURRENT_SPENDINGS_TABLE, null, values);
    }

    void insertPeriod(SQLiteDatabase db, String startDate, String endDate, String variant, double savings) {
        ContentValues values = new ContentValues();
        values.put(PERIOD_START_DATE, startDate);
        values.put(PERIOD_END_DATE, endDate);
        values.put(PERIOD_VARIANT, variant);
        values.put(PERIOD_SAVINGS, savings);
        db.insert(PERIOD_TABLE, null, values);
    }

    void insertArchiveSpending(SQLiteDatabase db, int idPeriod, String categoryName, double outcome, double income) {
        ContentValues values = new ContentValues();
        values.put(ARCHIVE_SPENDINGS_ID_PERIOD, idPeriod);
        values.put(ARCHIVE_SPENDINGS_NAME_CATEGORY, categoryName);
        values.put(ARCHIVE_SPENDINGS_OUTCOME, outcome);
        values.put(ARCHIVE_SPENDINGS_INCOME, income);
        db.insert(ARCHIVE_SPENDINGS_TABLE, null, values);
    }

    void insertArchiveFixedOutgoing(SQLiteDatabase db, int idPeriod, String name, double outcome, String dueDate, String paymentDate, boolean isPaid) {
        ContentValues values = new ContentValues();
        values.put(ARCHIVE_FIXED_OUTGOINGS_ID_PERIOD, idPeriod);
        values.put(ARCHIVE_FIXED_OUTGOINGS_NAME, name);
        values.put(ARCHIVE_FIXED_OUTGOINGS_OUTCOME, outcome);
        values.put(ARCHIVE_FIXED_OUTGOINGS_DUE_DATE, dueDate);
        values.put(ARCHIVE_FIXED_OUTGOINGS_PAYMENT_DATE, paymentDate);
        if (isPaid) {
            values.put(ARCHIVE_FIXED_OUTGOINGS_PAY, 1);
        } else {
            values.put(ARCHIVE_FIXED_OUTGOINGS_PAY, 0);
        }
        db.insert(ARCHIVE_FIXED_OUTGOINGS_TABLE, null, values);
    }

    void insertArchiveSalary(SQLiteDatabase db, int idPeriod, String name, double income, boolean isSaved) {
        ContentValues values = new ContentValues();
        values.put(ARCHIVE_SALARY_ID_PERIOD, idPeriod);
        values.put(ARCHIVE_SALARY_NAME, name);
        values.put(ARCHIVE_SALARY_INCOME, income);
        if (isSaved) {
            values.put(ARCHIVE_SALARY_SAVE, 1);
        } else {
            values.put(ARCHIVE_SALARY_SAVE, 0);
        }
        db.insert(ARCHIVE_SALARY_TABLE, null, values);
    }
}
