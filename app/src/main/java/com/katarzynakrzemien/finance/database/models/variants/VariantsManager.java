package com.katarzynakrzemien.finance.database.models.variants;

public class VariantsManager {

    public static Variants stringToVariant(String variant) {

        if (Variants.MONITORING.toString().equals(variant)) {
            return Variants.MONITORING;
        } else if (Variants.PLANNING.toString().equals(variant)) {
            return Variants.PLANNING;
        } else if (Variants.FULLPLANNING.toString().equals(variant)) {
            return Variants.FULLPLANNING;
        }
        throw new IllegalArgumentException();
    }
}
