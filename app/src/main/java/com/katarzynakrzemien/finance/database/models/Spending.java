package com.katarzynakrzemien.finance.database.models;


import java.util.Calendar;

public class Spending {
    private Integer id;
    private Calendar date;
    private double quota;

    public Spending(Integer id, Calendar date, double quota) {
        this.id = id;
        this.date = date;
        this.quota = quota;
    }

    public Integer getId() {
        return id;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public double getQuota() {
        return quota;
    }

    public void setQuota(double quota) {
        this.quota = quota;
    }
}
