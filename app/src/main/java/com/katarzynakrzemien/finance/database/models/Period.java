package com.katarzynakrzemien.finance.database.models;

import com.katarzynakrzemien.finance.database.models.variants.Variants;
import com.katarzynakrzemien.finance.database.models.variants.VariantsManager;

import java.util.Calendar;

public class Period {

    private Integer id;
    private Calendar startDate, endDate;
    private Variants variant;
    private double savings;

    public Period(Integer id, Calendar startDate, Calendar endDate,
                  Variants variant, double savings) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.variant = variant;
        this.savings = savings;
    }

    public Period(Integer id, Calendar startDate, Calendar endDate,
                  String variant, double savings) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.variant = stringToVariant(variant);
        this.savings = savings;
    }

    public Integer getId() {
        return id;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public void setStartDate(Calendar startDate) {
        this.startDate = startDate;
    }

    public Calendar getEndDate() {
        return endDate;
    }

    public void setEndDate(Calendar endDate) {
        this.endDate = endDate;
    }

    public int getStartYear() {
        return startDate.get(Calendar.YEAR);
    }

    public int getStartMonth() {
        return startDate.get(Calendar.MONTH);
    }

    public int getStartDay() {
        return startDate.get(Calendar.DAY_OF_MONTH);
    }

    public int getEndYear() {
        return endDate.get(Calendar.YEAR);
    }

    public int getEndMonth() {
        return endDate.get(Calendar.MONTH);
    }

    public int getEndDay() {
        return endDate.get(Calendar.DAY_OF_MONTH);
    }

    public double getSavings() {
        return savings;
    }

    public void setSavings(double savings) {
        this.savings = savings;
    }

    public Variants getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = stringToVariant(variant);
    }

    public void setVariant(Variants variant) {
        this.variant = variant;
    }

    private Variants stringToVariant(String variant) {
        return VariantsManager.stringToVariant(variant);
    }
}
