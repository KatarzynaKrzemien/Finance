package com.katarzynakrzemien.finance.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.database.models.variants.Variants;
import com.katarzynakrzemien.finance.utils.formatter.date.DateFormatPattern;
import com.katarzynakrzemien.finance.utils.formatter.date.DateFormatter;
import com.katarzynakrzemien.finance.utils.formatter.date.SimpleDateFormatter;

import java.util.Calendar;

public class PreferenceFromDatabase implements PreferenceStore {

    private SQLiteDatabase db;
    private DateFormatter timeFormatter = new SimpleDateFormatter(DateFormatPattern.TIME);

    public PreferenceFromDatabase(final Context context) {
        try {
            SQLiteOpenHelper sqLiteOpenHelper = new FinanceDatabaseHelper(context);
            db = sqLiteOpenHelper.getReadableDatabase();

        } catch (SQLiteException e) {
            Toast.makeText(context, context.getResources().getString(R.string
                    .noDatabase), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public Variants getVariant() {

        Cursor cursor = db.query(FinanceDatabaseHelper.VARS_TABLE,
                new String[]{FinanceDatabaseHelper._id, FinanceDatabaseHelper.VARS_VALUE},
                FinanceDatabaseHelper.VARS_NAME + "=?", new String[]{FinanceDatabaseHelper.VARS_VALUE_VARIANT},
                null, null, null);
        cursor.moveToFirst();
        String variant = cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.VARS_VALUE));
        cursor.close();

        if (variant.equals(Variants.PLANNING.toString())) {
            return Variants.PLANNING;
        } else if (variant.equals(Variants.FULLPLANNING.toString())) {
            return Variants.FULLPLANNING;
        }
        return Variants.MONITORING;
    }

    @Override
    public void setVariant(Variants variant) {
        ContentValues valueUpdate = new ContentValues();
        valueUpdate.put(FinanceDatabaseHelper.VARS_VALUE, variant.toString());
        db.update(FinanceDatabaseHelper.VARS_TABLE, valueUpdate,
                FinanceDatabaseHelper.VARS_NAME + " = ?",
                new String[]{FinanceDatabaseHelper.VARS_VALUE_VARIANT});

    }

    @Override
    public Double getSavings() {
        Cursor cursor = db.query(FinanceDatabaseHelper.VARS_TABLE,
                new String[]{FinanceDatabaseHelper._id, FinanceDatabaseHelper.VARS_VALUE},
                FinanceDatabaseHelper.VARS_NAME + "=?", new String[]{FinanceDatabaseHelper.VARS_VALUE_SAVINGS},
                null, null, null);
        cursor.moveToFirst();
        Double savings = cursor.getDouble(cursor.getColumnIndex(FinanceDatabaseHelper.VARS_VALUE));
        cursor.close();
        return savings;
    }

    @Override
    public void setSavings(double savings) {
        ContentValues valueUpdate = new ContentValues();
        valueUpdate.put(FinanceDatabaseHelper.VARS_VALUE, getSavings() + savings);
        db.update(FinanceDatabaseHelper.VARS_TABLE, valueUpdate,
                FinanceDatabaseHelper.VARS_NAME + " = ?",
                new String[]{FinanceDatabaseHelper.VARS_VALUE_SAVINGS});

    }

    @Override
    public Double getMonthlySavings() {
        Cursor cursor = db.query(FinanceDatabaseHelper.VARS_TABLE,
                new String[]{FinanceDatabaseHelper._id, FinanceDatabaseHelper.VARS_VALUE},
                FinanceDatabaseHelper.VARS_NAME + "=?", new String[]{FinanceDatabaseHelper.VARS_VALUE_MONTHLY_SAVINGS},
                null, null, null);
        cursor.moveToFirst();
        Double savings = cursor.getDouble(cursor.getColumnIndex(FinanceDatabaseHelper.VARS_VALUE));
        cursor.close();
        return savings;
    }

    @Override
    public void setMonthlySavings(double monthlySavings) {
        ContentValues valueUpdate = new ContentValues();
        valueUpdate.put(FinanceDatabaseHelper.VARS_VALUE, monthlySavings);
        db.update(FinanceDatabaseHelper.VARS_TABLE, valueUpdate,
                FinanceDatabaseHelper.VARS_NAME + " = ?",
                new String[]{FinanceDatabaseHelper.VARS_VALUE_MONTHLY_SAVINGS});

    }

    @Override
    public int getDayOfNewMonth() {
        Cursor cursor = db.query(FinanceDatabaseHelper.VARS_TABLE,
                new String[]{FinanceDatabaseHelper._id, FinanceDatabaseHelper.VARS_VALUE},
                FinanceDatabaseHelper.VARS_NAME + "=?", new String[]{FinanceDatabaseHelper.VARS_VALUE_DAY_NEW_MONTH},
                null, null, null);
        cursor.moveToFirst();
        int day = cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper.VARS_VALUE));
        cursor.close();
        return day;
    }

    @Override
    public void setDayOfNewMonth(int day) {
        ContentValues valueUpdate = new ContentValues();
        valueUpdate.put(FinanceDatabaseHelper.VARS_VALUE, day);
        db.update(FinanceDatabaseHelper.VARS_TABLE, valueUpdate,
                FinanceDatabaseHelper.VARS_NAME + " = ?",
                new String[]{FinanceDatabaseHelper.VARS_VALUE_DAY_NEW_MONTH});

    }

    @Override
    public Boolean getRemindingFixedOutgoings() {
        Cursor cursor = db.query(FinanceDatabaseHelper.VARS_TABLE,
                new String[]{FinanceDatabaseHelper._id, FinanceDatabaseHelper.VARS_VALUE},
                FinanceDatabaseHelper.VARS_NAME + "=?", new String[]{FinanceDatabaseHelper.VARS_VALUE_REMINDING_FIXED_OUTGOINGS},
                null, null, null);
        cursor.moveToFirst();
        Boolean reminding = cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper.VARS_VALUE)) == 1;
        cursor.close();
        return reminding;
    }

    @Override
    public void setRemindingFixedOutgoings(Boolean reminding) {
        ContentValues valueUpdate = new ContentValues();
        if (reminding) {
            valueUpdate.put(FinanceDatabaseHelper.VARS_VALUE, 1);
        } else {
            valueUpdate.put(FinanceDatabaseHelper.VARS_VALUE, 0);
        }
        db.update(FinanceDatabaseHelper.VARS_TABLE, valueUpdate,
                FinanceDatabaseHelper.VARS_NAME + " = ?", new String[]{FinanceDatabaseHelper.VARS_VALUE_REMINDING_FIXED_OUTGOINGS});

    }

    @Override
    public Calendar getRemindingTimeFixedOutgoings() {
        Cursor cursor = db.query(FinanceDatabaseHelper.VARS_TABLE,
                new String[]{FinanceDatabaseHelper._id, FinanceDatabaseHelper.VARS_VALUE},
                FinanceDatabaseHelper.VARS_NAME + "=?", new String[]{FinanceDatabaseHelper.VARS_VALUE_TIME_REMINDING_FIXED_OUTGOINGS},
                null, null, null);
        cursor.moveToFirst();
        Calendar time = timeFormatter.parseToCalendar(cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.VARS_VALUE)));
        cursor.close();
        return time;
    }

    @Override
    public void setRemindingTimeFixedOutgoings(Calendar remindingTimeFixedOutgoings) {
        ContentValues valueUpdate = new ContentValues();
        valueUpdate.put(FinanceDatabaseHelper.VARS_VALUE, timeFormatter.formatToString(remindingTimeFixedOutgoings));
        db.update(FinanceDatabaseHelper.VARS_TABLE, valueUpdate,
                FinanceDatabaseHelper.VARS_NAME + " = ?",
                new String[]{FinanceDatabaseHelper.VARS_VALUE_TIME_REMINDING_FIXED_OUTGOINGS});

    }

    @Override
    public Boolean getReplayRemindingFixedOutgoings() {
        Cursor cursor = db.query(FinanceDatabaseHelper.VARS_TABLE,
                new String[]{FinanceDatabaseHelper._id, FinanceDatabaseHelper.VARS_VALUE},
                FinanceDatabaseHelper.VARS_NAME + "=?", new String[]{FinanceDatabaseHelper.VARS_VALUE_REPLAY_REMINDING_FIXED_OUTGOINGS},
                null, null, null);
        cursor.moveToFirst();
        Boolean replay = cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper.VARS_VALUE)) == 1;
        cursor.close();
        return replay;
    }

    @Override
    public void getReplayRemindingFixedOutgoings(Boolean replayReminding) {
        ContentValues valueUpdate = new ContentValues();
        if (replayReminding) {
            valueUpdate.put(FinanceDatabaseHelper.VARS_VALUE, 1);
        } else {
            valueUpdate.put(FinanceDatabaseHelper.VARS_VALUE, 0);
        }
        db.update(FinanceDatabaseHelper.VARS_TABLE, valueUpdate,
                FinanceDatabaseHelper.VARS_NAME + " = ?",
                new String[]{FinanceDatabaseHelper.VARS_VALUE_REPLAY_REMINDING_FIXED_OUTGOINGS});
    }
}
