package com.katarzynakrzemien.finance.database.models;

import android.annotation.TargetApi;
import android.os.Build;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CategorizedFixedOutgoingManager {

    public List<CategorizedFixedOutgoing> categorizeAndSort(List<FixedOutgoing> dbFixedOutgoingList) {
        return categorize(groupFixedOutgoingsByName(dbFixedOutgoingList));
    }

    public Calendar isGetClosestDueDate(CategorizedFixedOutgoing categorizedFixedOutgoing) {
        //TODO
        Calendar closestDueDate = null;
        for (FixedOutgoing fixedOutgoing : categorizedFixedOutgoing.getList()) {
            if (fixedOutgoing.getDueDate().after(closestDueDate)) {
                closestDueDate = fixedOutgoing.getDueDate();
            }
        }
        return closestDueDate;
    }

    @TargetApi(Build.VERSION_CODES.N)
    private Map<String, List<FixedOutgoing>> groupFixedOutgoingsByName(List<FixedOutgoing> dbFixedOutgoingList) {
        Map<String, List<FixedOutgoing>> fixedOutgoingsListMap = new HashMap<>();
        for (FixedOutgoing item : dbFixedOutgoingList) {
            String key = item.getName();
            if (fixedOutgoingsListMap.containsKey(key)) {
                List<FixedOutgoing> list = fixedOutgoingsListMap.get(key);
                assert list != null;
                list.add(item);
                fixedOutgoingsListMap.replace(key, list);
            } else {
                List<FixedOutgoing> list = new ArrayList<>();
                list.add(item);
                fixedOutgoingsListMap.put(key, list);
            }
        }
        return fixedOutgoingsListMap;
    }

    @TargetApi(Build.VERSION_CODES.N)
    private List<CategorizedFixedOutgoing> categorize(Map<String, List<FixedOutgoing>> fixedOutgoingsListMap) {
        List<CategorizedFixedOutgoing> categorizedFixedOutgoingList = new ArrayList<>();
        for (Map.Entry<String, List<FixedOutgoing>> entry : fixedOutgoingsListMap.entrySet()) {
            List<FixedOutgoing> list = entry.getValue();
            list.sort(new FixedOutgoing.DueDateCompare());
            list.sort(new FixedOutgoing.PaidStatusCompare());
            categorizedFixedOutgoingList.add(new CategorizedFixedOutgoing(entry.getKey(), entry.getValue()));
        }
        categorizedFixedOutgoingList.sort(new CategorizedFixedOutgoing.DueDateCompare());
        categorizedFixedOutgoingList.sort(new CategorizedFixedOutgoing.PaidStatusCompare());
        return categorizedFixedOutgoingList;
    }
}
