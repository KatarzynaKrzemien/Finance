package com.katarzynakrzemien.finance.database;

import com.katarzynakrzemien.finance.database.models.ArchiveFixedOutgoing;
import com.katarzynakrzemien.finance.database.models.ArchiveSalary;
import com.katarzynakrzemien.finance.database.models.ArchiveSpending;
import com.katarzynakrzemien.finance.database.models.BaseModel;
import com.katarzynakrzemien.finance.database.models.CategorizedSpending;
import com.katarzynakrzemien.finance.database.models.Category;
import com.katarzynakrzemien.finance.database.models.CategoryAndIncome;
import com.katarzynakrzemien.finance.database.models.FixedOutgoing;
import com.katarzynakrzemien.finance.database.models.Period;
import com.katarzynakrzemien.finance.database.models.Salary;
import com.katarzynakrzemien.finance.database.models.Spending;
import com.katarzynakrzemien.finance.database.models.TotalSalary;

import java.util.List;

public interface DatabaseStore {

    List<CategorizedSpending> getCurrentSpendings();

    List<CategoryAndIncome> getSpendingsCategories();

    List<Category> getFixedOutgoingsCategories();

    List<FixedOutgoing> getFixedOutgoings();

    FixedOutgoing getFixedOutgoing(int id);

    TotalSalary getSalaries();

    Salary getSalary(int id);

    Salary getLastMonthRemainingFunds();

    Salary getSalaryFromSavings();

    List<ArchiveSpending> getArchiveSpending(int idPeriod);

    List<ArchiveFixedOutgoing> getArchiveFixedOutgoings(int idPeriod);

    List<ArchiveSalary> getArchiveSalary(int idPeriod);

    List<Period> getArchivePeriods();

    Period getCurrentPeriod();

    Period getPeriod(int id);

    List<BaseModel> getAnnualArchiveSpendingSums(int year);

    List<BaseModel> getAnnualArchiveFixedOutgoingSums(int year);

    List<BaseModel> getAnnualArchiveSalarySums(int year);

    double getSpendingSum();

    double getPaidFixedOutgoingsSum();

    double getUnpaidFixedOutgoingsSum();

    double getSalarySum();

    double getSavedSalarySum();

    double getArchiveSpendingSum(int idPeriod);

    double getArchiveFixedOutgoingsSum(int idPeriod);

    double getArchiveSalarySum(int idPeriod);

    double getArchiveSavings(int idPeriod);

    void insertSalary(Salary salary);

    void insertSpendingCategory(CategoryAndIncome category);

    void insertFixedOutgoingCategory(Category category);

    void insertFixedOutgoing(FixedOutgoing fixedOutgoing);

    void insertSpending(String categoryName, Spending spending);

    void insertPeriod(Period period);

    void insertArchiveSpending(ArchiveSpending archiveSpending);

    void insertArchiveFixedOutgoing(int idPeriod, FixedOutgoing archiveFixedOutgoing);

    void insertArchiveSalary(ArchiveSalary archiveSalary);

    void deleteSalary(int id);

    void deleteSpendingCategory(int id);

    void deleteFixedOutgoing(int id);

    void deleteFixedOutgoingCategory(int id);

    void deleteSpending(int id);

    void deletePeriod(int id);

    void deleteArchiveSpending(int id);

    void deleteArchiveFixedOutgoing(int id);

    void deleteArchiveSalary(int id);

    void updateSalary(Salary model);

    void updateSpendingCategory(CategoryAndIncome model);

    void updateFixedOutgoingCategory(Category model);

    void updateFixedOutgoing(FixedOutgoing model);

    void updateSpending(Spending model);

    void updatePeriod(Period model);
}
