package com.katarzynakrzemien.finance.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.database.models.ArchiveFixedOutgoing;
import com.katarzynakrzemien.finance.database.models.ArchiveSalary;
import com.katarzynakrzemien.finance.database.models.ArchiveSpending;
import com.katarzynakrzemien.finance.database.models.BaseModel;
import com.katarzynakrzemien.finance.database.models.CategorizedSpending;
import com.katarzynakrzemien.finance.database.models.Category;
import com.katarzynakrzemien.finance.database.models.CategoryAndIncome;
import com.katarzynakrzemien.finance.database.models.FixedOutgoing;
import com.katarzynakrzemien.finance.database.models.Period;
import com.katarzynakrzemien.finance.database.models.Salary;
import com.katarzynakrzemien.finance.database.models.Spending;
import com.katarzynakrzemien.finance.database.models.TotalSalary;
import com.katarzynakrzemien.finance.utils.formatter.date.DateFormatPattern;
import com.katarzynakrzemien.finance.utils.formatter.date.DateFormatter;
import com.katarzynakrzemien.finance.utils.formatter.date.SimpleDateFormatter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DatabaseQuery implements DatabaseStore {
    private SQLiteDatabase db;
    private FinanceDatabaseHelper financeDatabaseHelper;
    private DateFormatter sqlDateFormatter = new SimpleDateFormatter(DateFormatPattern.SQLITE);

    public DatabaseQuery(final Context context) {
        try {
            SQLiteOpenHelper sqLiteOpenHelper = new FinanceDatabaseHelper(context);
            financeDatabaseHelper = (FinanceDatabaseHelper) sqLiteOpenHelper;
            db = sqLiteOpenHelper.getReadableDatabase();

        } catch (SQLiteException e) {
            Toast.makeText(context, context.getResources().getString(R.string
                    .noDatabase), Toast.LENGTH_SHORT).show();
        }
    }

    private Calendar formatFixedOutgoingModelDate(String date) {
        if (date.equals(FinanceDatabaseHelper.NODATE)) {
            return null;
        }
        return sqlDateFormatter.parseToCalendar(date);
    }

    private String formatFixedOutgoingSQLDate(Calendar date) {
        if (date == null) {
            return FinanceDatabaseHelper.NODATE;
        }
        return sqlDateFormatter.formatToString(date);
    }

    @Override
    public FixedOutgoing getFixedOutgoing(int id) {
        Cursor cursor = db.rawQuery("SELECT cf." + FinanceDatabaseHelper._id +
                ", cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_ID_FO +
                ", f." + FinanceDatabaseHelper.FIXED_OUTGOINGS_NAME +
                ", cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_OUTCOME +
                ", cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_DUE_DATE +
                ", cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_PAYMENT_DATE +
                ", cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_PAY +
                ", f." + FinanceDatabaseHelper.FIXED_OUTGOINGS_ACTIVE +
                " FROM " + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_TABLE + " AS cf INNER JOIN " +
                FinanceDatabaseHelper.FIXED_OUTGOINGS_TABLE + " AS f ON " +
                "cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_ID_FO + " = " +
                "f." + FinanceDatabaseHelper._id +
                " WHERE cf." + FinanceDatabaseHelper._id + " = " + id, null);
        FixedOutgoing fixedOutgoings = null;
        if (cursor.moveToFirst()) {
            fixedOutgoings = new FixedOutgoing(
                    cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper._id)),
                    cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_ID_FO)),
                    cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.FIXED_OUTGOINGS_NAME)),
                    cursor.getDouble(cursor.getColumnIndex(FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_OUTCOME)),
                    formatFixedOutgoingModelDate(cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_DUE_DATE))),
                    formatFixedOutgoingModelDate(cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_PAYMENT_DATE))),
                    cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_PAY)) == 1);
        }
        cursor.close();
        return fixedOutgoings;
    }

    @Override
    public List<CategorizedSpending> getCurrentSpendings() {
        Cursor cursor = db.rawQuery("SELECT " + FinanceDatabaseHelper._id +
                ", " + FinanceDatabaseHelper.CATEGORIES_NAME +
                ", " + FinanceDatabaseHelper.CATEGORIES_INCOME +
                ", " + FinanceDatabaseHelper.CATEGORIES_ACTIVE +
                " FROM " + FinanceDatabaseHelper.CATEGORIES_TABLE +
                " WHERE " + FinanceDatabaseHelper.CATEGORIES_ACTIVE + " = 1" +
                " ORDER BY " + FinanceDatabaseHelper.CATEGORIES_NAME + " ASC", null);
        List<CategorizedSpending> categorizedSpendingList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                categorizedSpendingList.add(new CategorizedSpending(
                        cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.CATEGORIES_NAME)),
                        cursor.getDouble(cursor.getColumnIndex(FinanceDatabaseHelper.CATEGORIES_INCOME)),
                        getCategorySpendingList(cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper._id)))
                ));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return categorizedSpendingList;
    }

    private List<Spending> getCategorySpendingList(String categoryId) {
        Cursor cursor = db.rawQuery("SELECT " + FinanceDatabaseHelper._id + ", " +
                        FinanceDatabaseHelper.CURRENT_SPENDINGS_ID_CAT + ", " +
                        FinanceDatabaseHelper.CURRENT_SPENDINGS_DATE + ", " +
                        FinanceDatabaseHelper.CURRENT_SPENDINGS_OUTCOME +
                        " FROM " + FinanceDatabaseHelper.CURRENT_SPENDINGS_TABLE +
                        " WHERE " + FinanceDatabaseHelper.CURRENT_SPENDINGS_ID_CAT +
                        " = " + categoryId + " ORDER BY " + FinanceDatabaseHelper.CURRENT_SPENDINGS_DATE + " ASC",
                null);
        List<Spending> spendingList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                spendingList.add(new Spending(
                        cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper._id)),
                        sqlDateFormatter.parseToCalendar(cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.CURRENT_SPENDINGS_DATE))),
                        cursor.getDouble(cursor.getColumnIndex(FinanceDatabaseHelper.CURRENT_SPENDINGS_OUTCOME))
                ));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return spendingList;

    }

    @Override
    public List<CategoryAndIncome> getSpendingsCategories() {
        Cursor cursor = db.query(FinanceDatabaseHelper.CATEGORIES_TABLE,
                new String[]{FinanceDatabaseHelper._id,
                        FinanceDatabaseHelper.CATEGORIES_NAME,
                        FinanceDatabaseHelper.CATEGORIES_INCOME,
                        FinanceDatabaseHelper.CATEGORIES_ACTIVE},
                null, null, null, null,
                FinanceDatabaseHelper.CATEGORIES_NAME + " ASC");
        List<CategoryAndIncome> spendingCategoriesList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                spendingCategoriesList.add(new CategoryAndIncome(
                        cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper._id)),
                        cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.CATEGORIES_NAME)),
                        cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper.CATEGORIES_ACTIVE)) == 1,
                        cursor.getDouble(cursor.getColumnIndex(FinanceDatabaseHelper.CATEGORIES_INCOME))));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return spendingCategoriesList;
    }

    @Override
    public List<Category> getFixedOutgoingsCategories() {
        Cursor cursor = db.query(FinanceDatabaseHelper.FIXED_OUTGOINGS_TABLE,
                new String[]{FinanceDatabaseHelper._id,
                        FinanceDatabaseHelper.FIXED_OUTGOINGS_NAME,
                        FinanceDatabaseHelper.FIXED_OUTGOINGS_ACTIVE},
                null, null, null, null,
                FinanceDatabaseHelper.FIXED_OUTGOINGS_NAME + " ASC");
        List<Category> fixedOutgoingCategoriesList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                fixedOutgoingCategoriesList.add(new Category(
                        cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper._id)),
                        cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.FIXED_OUTGOINGS_NAME)),
                        cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper.FIXED_OUTGOINGS_ACTIVE)) == 1));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return fixedOutgoingCategoriesList;
    }

    @Override
    public List<FixedOutgoing> getFixedOutgoings() {
        Cursor cursor = db.rawQuery("SELECT cf." + FinanceDatabaseHelper._id +
                ", cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_ID_FO +
                ", f." + FinanceDatabaseHelper.FIXED_OUTGOINGS_NAME +
                ", cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_OUTCOME +
                ", cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_DUE_DATE +
                ", cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_PAYMENT_DATE +
                ", cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_PAY +
                ", f." + FinanceDatabaseHelper.FIXED_OUTGOINGS_ACTIVE +
                " FROM " + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_TABLE + " AS cf INNER JOIN " +
                FinanceDatabaseHelper.FIXED_OUTGOINGS_TABLE + " AS f ON " +
                "cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_ID_FO + " = " +
                "f." + FinanceDatabaseHelper._id +
                " WHERE f." + FinanceDatabaseHelper.FIXED_OUTGOINGS_ACTIVE + " = 1" +
                " ORDER BY " + " cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_DUE_DATE + " ASC", null);
        List<FixedOutgoing> fixedOutgoingList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                fixedOutgoingList.add(new FixedOutgoing(
                        cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper._id)),
                        cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_ID_FO)),
                        cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.FIXED_OUTGOINGS_NAME)),
                        cursor.getDouble(cursor.getColumnIndex(FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_OUTCOME)),
                        formatFixedOutgoingModelDate(cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_DUE_DATE))),
                        formatFixedOutgoingModelDate(cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_PAYMENT_DATE))),
                        cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_PAY)) == 1));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return fixedOutgoingList;
    }

    @Override
    public List<ArchiveSpending> getArchiveSpending(int idPeriod) {
        Cursor cursor = db.query(FinanceDatabaseHelper.ARCHIVE_SPENDINGS_TABLE,
                new String[]{FinanceDatabaseHelper._id,
                        FinanceDatabaseHelper.ARCHIVE_SPENDINGS_ID_PERIOD,
                        FinanceDatabaseHelper.ARCHIVE_SPENDINGS_NAME_CATEGORY,
                        FinanceDatabaseHelper.ARCHIVE_SPENDINGS_OUTCOME,
                        FinanceDatabaseHelper.ARCHIVE_SPENDINGS_INCOME},
                FinanceDatabaseHelper.ARCHIVE_SPENDINGS_ID_PERIOD + " =? ",
                new String[]{String.valueOf(idPeriod)}, null, null,
                FinanceDatabaseHelper.ARCHIVE_SPENDINGS_NAME_CATEGORY + " ASC");
        List<ArchiveSpending> archiveList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                archiveList.add(new ArchiveSpending(
                        cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper._id)),
                        cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper.ARCHIVE_SPENDINGS_ID_PERIOD)),
                        cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.ARCHIVE_SPENDINGS_NAME_CATEGORY)),
                        cursor.getDouble(cursor.getColumnIndex(FinanceDatabaseHelper.ARCHIVE_SPENDINGS_OUTCOME)),
                        cursor.getDouble(cursor.getColumnIndex(FinanceDatabaseHelper.ARCHIVE_SPENDINGS_INCOME))));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return archiveList;
    }

    @Override
    public TotalSalary getSalaries() {
        Cursor cursor = db.query(FinanceDatabaseHelper.SALARY_TABLE,
                new String[]{FinanceDatabaseHelper._id, FinanceDatabaseHelper.SALARY_NAME,
                        FinanceDatabaseHelper.SALARY_INCOME, FinanceDatabaseHelper.SALARY_SAVE},
                FinanceDatabaseHelper._id + " >? ", new String[]{FinanceDatabaseHelper.idFromSavings},
                null, null, FinanceDatabaseHelper.SALARY_NAME + " ASC");
        List<Salary> salaryList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                salaryList.add(new Salary(
                        cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper._id)),
                        cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.SALARY_NAME)),
                        cursor.getDouble(cursor.getColumnIndex(FinanceDatabaseHelper.SALARY_INCOME)),
                        cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper.SALARY_SAVE)) == 1));
            } while (cursor.moveToNext());
        }
        TotalSalary totalSalary = new TotalSalary(salaryList, getSalaryFromSavings(), getLastMonthRemainingFunds());
        cursor.close();
        return totalSalary;
    }

    @Override
    public Salary getSalary(int id) {
        Cursor cursor = db.query(FinanceDatabaseHelper.SALARY_TABLE,
                new String[]{FinanceDatabaseHelper._id, FinanceDatabaseHelper.SALARY_NAME,
                        FinanceDatabaseHelper.SALARY_INCOME, FinanceDatabaseHelper.SALARY_SAVE},
                FinanceDatabaseHelper._id + " =? ",
                new String[]{String.valueOf(id)}, null, null, null);
        cursor.moveToFirst();
        Salary salary = new Salary(
                cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper._id)),
                cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.SALARY_NAME)),
                cursor.getDouble(cursor.getColumnIndex(FinanceDatabaseHelper.SALARY_INCOME)),
                cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper.SALARY_SAVE)) == 1);
        cursor.close();
        return salary;
    }

    @Override
    public Salary getSalaryFromSavings() {
        return getSalary(Integer.parseInt(FinanceDatabaseHelper.idFromSavings));

    }

    @Override
    public Salary getLastMonthRemainingFunds() {
        return getSalary(Integer.parseInt(FinanceDatabaseHelper.idLastMonth));
    }

    @Override
    public List<ArchiveFixedOutgoing> getArchiveFixedOutgoings(int idPeriod) {
        Cursor cursor = db.query(FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_TABLE,
                new String[]{FinanceDatabaseHelper._id,
                        FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_ID_PERIOD,
                        FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_NAME,
                        FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_DUE_DATE},
                FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_ID_PERIOD + " =? ",
                new String[]{String.valueOf(idPeriod)}, null, null,
                FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_DUE_DATE + " ASC");
        List<ArchiveFixedOutgoing> archiveFixedOutgoingList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                archiveFixedOutgoingList.add(new ArchiveFixedOutgoing(
                        cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_NAME)),
                        cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_ID_PERIOD)),
                        getArchiveFixedOutgoingsList(cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_NAME)), cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_ID_PERIOD)))));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return archiveFixedOutgoingList;
    }

    private List<FixedOutgoing> getArchiveFixedOutgoingsList(String fixedOutgoingName, String idPeriod) {
        Cursor cursor = db.rawQuery("SELECT " + FinanceDatabaseHelper._id + ", " +
                FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_NAME + ", " +
                FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_OUTCOME + ", " +
                FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_DUE_DATE + ", " +
                FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_PAYMENT_DATE + ", " +
                FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_PAY +
                " FROM " + FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_TABLE +
                " WHERE " + FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_NAME + " = '" + fixedOutgoingName + "'" +
                " AND " + FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_ID_PERIOD + " = " + idPeriod +
                " ORDER BY " + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_DUE_DATE + " ASC", null);
        List<FixedOutgoing> archiveFixedOutgoingsList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                archiveFixedOutgoingsList.add(new FixedOutgoing(
                        cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper._id)), 0,
                        cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_NAME)),
                        cursor.getDouble(cursor.getColumnIndex(FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_OUTCOME)),
                        formatFixedOutgoingModelDate(cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_DUE_DATE))),
                        formatFixedOutgoingModelDate(cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_PAYMENT_DATE))),
                        cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_PAY)) == 1));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return archiveFixedOutgoingsList;
    }

    @Override
    public List<ArchiveSalary> getArchiveSalary(int idPeriod) {
        Cursor cursor = db.query(FinanceDatabaseHelper.ARCHIVE_SALARY_TABLE,
                new String[]{FinanceDatabaseHelper._id,
                        FinanceDatabaseHelper.ARCHIVE_SALARY_ID_PERIOD,
                        FinanceDatabaseHelper.ARCHIVE_SALARY_NAME,
                        FinanceDatabaseHelper.ARCHIVE_SALARY_INCOME,
                        FinanceDatabaseHelper.ARCHIVE_SALARY_SAVE},
                FinanceDatabaseHelper.ARCHIVE_SALARY_ID_PERIOD + " =? ",
                new String[]{String.valueOf(idPeriod)}, null, null,
                FinanceDatabaseHelper.ARCHIVE_SALARY_NAME + " ASC");
        List<ArchiveSalary> archiveSalaryList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                archiveSalaryList.add(new ArchiveSalary(
                        cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper._id)),
                        cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper.ARCHIVE_SALARY_ID_PERIOD)),
                        cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.ARCHIVE_SALARY_NAME)),
                        cursor.getDouble(cursor.getColumnIndex(FinanceDatabaseHelper.ARCHIVE_SALARY_INCOME)),
                        cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper.ARCHIVE_SALARY_SAVE)) == 1));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return archiveSalaryList;
    }

    private int getCurrentPeriodId() {
        Cursor cursor = db.rawQuery("SELECT MAX(" + FinanceDatabaseHelper._id + ")" +
                " FROM " + FinanceDatabaseHelper.PERIOD_TABLE, null);
        int currentPeriodId = 0;
        if (cursor.moveToFirst()) {
            currentPeriodId = cursor.getInt(0);
        }
        cursor.close();
        return currentPeriodId;
    }

    @Override
    public List<Period> getArchivePeriods() {
        Cursor cursor = db.query(FinanceDatabaseHelper.PERIOD_TABLE,
                new String[]{FinanceDatabaseHelper._id,
                        FinanceDatabaseHelper.PERIOD_START_DATE,
                        FinanceDatabaseHelper.PERIOD_END_DATE,
                        FinanceDatabaseHelper.PERIOD_VARIANT,
                        FinanceDatabaseHelper.PERIOD_SAVINGS},
                FinanceDatabaseHelper._id + " <?",
                new String[]{String.valueOf(getCurrentPeriodId())}, null, null,
                FinanceDatabaseHelper.PERIOD_START_DATE + " ASC");
        List<Period> periodList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                periodList.add(new Period(
                        cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper._id)),
                        sqlDateFormatter.parseToCalendar(cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.PERIOD_START_DATE))),
                        sqlDateFormatter.parseToCalendar(cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.PERIOD_END_DATE))),
                        cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.PERIOD_VARIANT)),
                        cursor.getDouble(cursor.getColumnIndex(FinanceDatabaseHelper.PERIOD_SAVINGS))));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return periodList;
    }

    @Override
    public Period getCurrentPeriod() {
        return getPeriod(getCurrentPeriodId());
    }

    @Override
    public Period getPeriod(int id) {
        Cursor cursor = db.query(FinanceDatabaseHelper.PERIOD_TABLE,
                new String[]{FinanceDatabaseHelper._id,
                        FinanceDatabaseHelper.PERIOD_START_DATE,
                        FinanceDatabaseHelper.PERIOD_END_DATE,
                        FinanceDatabaseHelper.PERIOD_VARIANT,
                        FinanceDatabaseHelper.PERIOD_SAVINGS},
                FinanceDatabaseHelper._id + " =?",
                new String[]{String.valueOf(getCurrentPeriodId())},
                null, null, null);
        Period period = null;
        if (cursor.moveToFirst()) {
            period = new Period(cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper._id)),
                    sqlDateFormatter.parseToCalendar(cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.PERIOD_START_DATE))),
                    sqlDateFormatter.parseToCalendar(cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.PERIOD_END_DATE))),
                    cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.PERIOD_VARIANT)),
                    cursor.getDouble(cursor.getColumnIndex(FinanceDatabaseHelper.PERIOD_SAVINGS)));
        }
        cursor.close();
        return period;
    }

    @Override
    public List<BaseModel> getAnnualArchiveSpendingSums(int year) {
        Cursor cursor = db.rawQuery("SELECT SUM(a." + FinanceDatabaseHelper.ARCHIVE_SPENDINGS_OUTCOME +
                "), a." + FinanceDatabaseHelper.ARCHIVE_SPENDINGS_NAME_CATEGORY +
                ", a." + FinanceDatabaseHelper.ARCHIVE_SPENDINGS_ID_PERIOD +
                ", a." + FinanceDatabaseHelper._id +
                ", p." + FinanceDatabaseHelper._id +
                ", strftime('%Y',p." + FinanceDatabaseHelper.PERIOD_START_DATE +
                ") AS YEAR FROM " + FinanceDatabaseHelper.PERIOD_TABLE +
                " AS p INNER JOIN " + FinanceDatabaseHelper.ARCHIVE_SPENDINGS_TABLE +
                " AS a ON p." + FinanceDatabaseHelper._id +
                " = a." + FinanceDatabaseHelper.ARCHIVE_SPENDINGS_ID_PERIOD +
                " WHERE YEAR = '" + year + "' GROUP BY " + FinanceDatabaseHelper.ARCHIVE_SPENDINGS_NAME_CATEGORY, null);
        List<BaseModel> archiveSpendingList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                archiveSpendingList.add(new BaseModel(
                        cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper._id)),
                        cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.ARCHIVE_SPENDINGS_NAME_CATEGORY)),
                        cursor.getDouble(0)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return archiveSpendingList;
    }

    @Override
    public List<BaseModel> getAnnualArchiveFixedOutgoingSums(int year) {
        Cursor cursor = db.rawQuery("SELECT SUM(a." + FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_OUTCOME +
                "), a." + FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_NAME +
                ", a." + FinanceDatabaseHelper.ARCHIVE_SPENDINGS_ID_PERIOD +
                ", a." + FinanceDatabaseHelper._id +
                ", p." + FinanceDatabaseHelper._id +
                ", strftime('%Y',p." + FinanceDatabaseHelper.PERIOD_START_DATE +
                ") AS YEAR FROM " + FinanceDatabaseHelper.PERIOD_TABLE +
                " AS p INNER JOIN " + FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_TABLE +
                " AS a ON p." + FinanceDatabaseHelper._id + " = a." + FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_ID_PERIOD +
                " WHERE YEAR = '" + year + "' GROUP BY " + FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_NAME, null);
        List<BaseModel> archiveFixedOutgoingList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                archiveFixedOutgoingList.add(new BaseModel(
                        cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper._id)),
                        cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_NAME)),
                        cursor.getDouble(0)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return archiveFixedOutgoingList;
    }

    @Override
    public List<BaseModel> getAnnualArchiveSalarySums(int year) {
        Cursor cursor = db.rawQuery("SELECT SUM(a." + FinanceDatabaseHelper.ARCHIVE_SALARY_INCOME +
                "), a." + FinanceDatabaseHelper.ARCHIVE_SALARY_NAME +
                ", a." + FinanceDatabaseHelper._id +
                ", p." + FinanceDatabaseHelper._id +
                ", strftime('%Y',p." + FinanceDatabaseHelper.PERIOD_START_DATE +
                ") AS YEAR FROM " + FinanceDatabaseHelper.PERIOD_TABLE +
                " AS p INNER JOIN " + FinanceDatabaseHelper.ARCHIVE_SALARY_TABLE +
                " AS a ON p." + FinanceDatabaseHelper._id + " = a." + FinanceDatabaseHelper.ARCHIVE_SALARY_ID_PERIOD +
                " WHERE YEAR = '" + year + "' GROUP BY " + FinanceDatabaseHelper.ARCHIVE_SALARY_NAME, null);
        List<BaseModel> archiveSalaryList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                archiveSalaryList.add(new BaseModel(
                        cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper._id)),
                        cursor.getString(cursor.getColumnIndex(FinanceDatabaseHelper.ARCHIVE_SALARY_NAME)),
                        cursor.getDouble(0)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return archiveSalaryList;
    }

    @Override
    public double getSpendingSum() {
        Cursor cursor = db.rawQuery("SELECT cm._id, SUM(cm." + FinanceDatabaseHelper.CURRENT_SPENDINGS_OUTCOME +
                "),c." + FinanceDatabaseHelper._id + " FROM " +
                FinanceDatabaseHelper.CURRENT_SPENDINGS_TABLE + " AS cm INNER JOIN " +
                FinanceDatabaseHelper.CATEGORIES_TABLE + " AS c ON cm." +
                FinanceDatabaseHelper.CURRENT_SPENDINGS_ID_CAT + " = c." +
                FinanceDatabaseHelper._id + " WHERE c." +
                FinanceDatabaseHelper.CATEGORIES_ACTIVE + " = 1", null);
        double spendingSum = 0.0;
        if (cursor.moveToFirst()) {
            spendingSum = cursor.getDouble(1);
        }
        cursor.close();
        return spendingSum;
    }

    @Override
    public double getPaidFixedOutgoingsSum() {
        Cursor cursor = db.rawQuery("SELECT cf." + FinanceDatabaseHelper._id +
                ", f." + FinanceDatabaseHelper._id +
                ", SUM( cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_OUTCOME + ")" +
                ", cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_ID_FO +
                ", cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_PAY +
                ", f." + FinanceDatabaseHelper.FIXED_OUTGOINGS_ACTIVE +
                " FROM " + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_TABLE + " AS cf INNER JOIN " +
                FinanceDatabaseHelper.FIXED_OUTGOINGS_TABLE + " AS f ON " +
                "cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_ID_FO + " = " +
                "f." + FinanceDatabaseHelper._id +
                " WHERE f." + FinanceDatabaseHelper.FIXED_OUTGOINGS_ACTIVE + " = 1" +
                " AND cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_PAY + " = 1", null);

        double fixedOutgoingSum = 0.0;
        if (cursor.moveToFirst()) {
            fixedOutgoingSum = cursor.getDouble(2);
        }
        cursor.close();
        return fixedOutgoingSum;
    }

    @Override
    public double getUnpaidFixedOutgoingsSum() {
        Cursor cursor = db.rawQuery("SELECT cf." + FinanceDatabaseHelper._id +
                ", f." + FinanceDatabaseHelper._id +
                ", SUM( cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_OUTCOME + ")" +
                ", cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_ID_FO +
                ", cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_PAY +
                ", f." + FinanceDatabaseHelper.FIXED_OUTGOINGS_ACTIVE +
                " FROM " + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_TABLE + " AS cf INNER JOIN " +
                FinanceDatabaseHelper.FIXED_OUTGOINGS_TABLE + " AS f ON " +
                "cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_ID_FO + " = " +
                "f." + FinanceDatabaseHelper._id +
                " WHERE f." + FinanceDatabaseHelper.FIXED_OUTGOINGS_ACTIVE + " = 1" +
                " AND cf." + FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_PAY + " = 0", null);

        double fixedOutgoingSum = 0.0;
        if (cursor.moveToFirst()) {
            fixedOutgoingSum = cursor.getDouble(2);
        }
        cursor.close();
        return fixedOutgoingSum;
    }

    @Override
    public double getSalarySum() {
        Cursor cursor = db.rawQuery("SELECT " + FinanceDatabaseHelper._id +
                ", SUM(" + FinanceDatabaseHelper.SALARY_INCOME +
                ") FROM " + FinanceDatabaseHelper.SALARY_TABLE +
                " WHERE " + FinanceDatabaseHelper.SALARY_SAVE + " = 0", null);
        double salarySum = 0.0;
        if (cursor.moveToFirst()) {
            salarySum = cursor.getDouble(1);
        }
        cursor.close();
        return salarySum;
    }

    @Override
    public double getSavedSalarySum() {
        Cursor cursor = db.rawQuery("SELECT " + FinanceDatabaseHelper._id +
                ", SUM(" + FinanceDatabaseHelper.SALARY_INCOME +
                ") FROM " + FinanceDatabaseHelper.SALARY_TABLE +
                " WHERE " + FinanceDatabaseHelper.SALARY_SAVE + " = 1", null);
        double salarySum = 0.0;
        if (cursor.moveToFirst()) {
            salarySum = cursor.getDouble(1);
        }
        cursor.close();
        return salarySum;
    }

    @Override
    public double getArchiveSpendingSum(int idPeriod) {
        Cursor cursor = db.rawQuery("SELECT " + FinanceDatabaseHelper._id +
                ", SUM( " + FinanceDatabaseHelper.ARCHIVE_SPENDINGS_OUTCOME +
                ")" + " FROM " + FinanceDatabaseHelper.ARCHIVE_SPENDINGS_TABLE +
                " WHERE " + FinanceDatabaseHelper.ARCHIVE_SPENDINGS_ID_PERIOD + " = " + idPeriod, null);
        double archiveSpendingSum = 0.0;
        if (cursor.moveToFirst()) {
            archiveSpendingSum = cursor.getDouble(1);
        }
        cursor.close();
        return archiveSpendingSum;
    }

    @Override
    public double getArchiveFixedOutgoingsSum(int idPeriod) {
        Cursor cursor = db.rawQuery("SELECT " + FinanceDatabaseHelper._id +
                ", SUM( " + FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_OUTCOME +
                ")" + " FROM " + FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_TABLE +
                " WHERE " + FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_ID_PERIOD +
                " = " + idPeriod + " AND " + FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_PAY + " = 1", null);
        double archiveFixedOutgoingsSum = 0.0;
        if (cursor.moveToFirst()) {
            archiveFixedOutgoingsSum = cursor.getDouble(1);
        }
        cursor.close();
        return archiveFixedOutgoingsSum;
    }

    @Override
    public double getArchiveSalarySum(int idPeriod) {
        Cursor cursor = db.rawQuery("SELECT  SUM( " + FinanceDatabaseHelper.ARCHIVE_SALARY_INCOME +
                "), " + FinanceDatabaseHelper._id + " FROM " + FinanceDatabaseHelper.ARCHIVE_SALARY_TABLE +
                " WHERE " + FinanceDatabaseHelper.ARCHIVE_SALARY_ID_PERIOD + " = " + idPeriod, null);
        double archiveSalarySum = 0.0;
        if (cursor.moveToFirst()) {
            archiveSalarySum = cursor.getDouble(0);
        }
        cursor.close();
        return archiveSalarySum;
    }

    @Override
    public double getArchiveSavings(int idPeriod) {
        Cursor cursor = db.query(FinanceDatabaseHelper.PERIOD_TABLE,
                new String[]{FinanceDatabaseHelper._id,
                        FinanceDatabaseHelper.PERIOD_SAVINGS},
                FinanceDatabaseHelper._id + " =? ",
                new String[]{String.valueOf(idPeriod)}, null, null, null);
        double savings = 0.0;
        if (cursor.moveToFirst()) {
            savings = cursor.getDouble(cursor.getColumnIndex(FinanceDatabaseHelper.PERIOD_SAVINGS));
        }
        cursor.close();
        return savings;
    }


    @Override
    public void insertSalary(Salary salary) {
        financeDatabaseHelper.insertSalary(db, salary.getName(),
                salary.getQuota(), salary.getSave());
    }

    @Override
    public void insertSpendingCategory(CategoryAndIncome category) {
        financeDatabaseHelper.insertCategory(db, category.getName(), category.getActive(), category.getIncome());
    }

    @Override
    public void insertFixedOutgoingCategory(Category category) {
        financeDatabaseHelper.insertFixedOutgoing(db, category.getName(), category.getActive());
    }

    @Override
    public void insertFixedOutgoing(FixedOutgoing fixedOutgoing) {
        financeDatabaseHelper.insertCurrentFixedOutgoing(db, fixedOutgoing.getIdCategoryOfFO(),
                fixedOutgoing.getOutcome(), formatFixedOutgoingSQLDate(fixedOutgoing.getDueDate()),
                formatFixedOutgoingSQLDate(fixedOutgoing.getPaymentDate()), fixedOutgoing.getIsPaid());
    }

    @Override
    public void insertSpending(String categoryName, Spending spending) {
        Cursor cursor = db.query(FinanceDatabaseHelper.CATEGORIES_TABLE,
                new String[]{FinanceDatabaseHelper._id},
                FinanceDatabaseHelper.CATEGORIES_NAME + " =? ", new String[]{categoryName},
                null, null, null);
        if (cursor.moveToFirst()) {
            financeDatabaseHelper.insertCurrentSpending(db, cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper._id)),
                    spending.getQuota(), sqlDateFormatter.formatToString(spending.getDate()));
        }
        cursor.close();
    }

    @Override
    public void insertPeriod(Period period) {
        financeDatabaseHelper.insertPeriod(db,
                sqlDateFormatter.formatToString(period.getStartDate()),
                sqlDateFormatter.formatToString(period.getEndDate()),
                period.getVariant().toString(), period.getSavings());
    }

    @Override
    public void insertArchiveSpending(ArchiveSpending archiveSpending) {
        financeDatabaseHelper.insertArchiveSpending(db, archiveSpending.getIdPeriod(),
                archiveSpending.getName(), archiveSpending.getOutcome(),
                archiveSpending.getIncome());
    }

    @Override
    public void insertArchiveFixedOutgoing(int idPeriod, FixedOutgoing archiveFixedOutgoing) {
        financeDatabaseHelper.insertArchiveFixedOutgoing(db, idPeriod,
                archiveFixedOutgoing.getName(),
                archiveFixedOutgoing.getOutcome(),
                formatFixedOutgoingSQLDate(archiveFixedOutgoing.getDueDate()),
                formatFixedOutgoingSQLDate(archiveFixedOutgoing.getPaymentDate()),
                archiveFixedOutgoing.getIsPaid());
    }

    @Override
    public void insertArchiveSalary(ArchiveSalary archiveSalary) {
        financeDatabaseHelper.insertArchiveSalary(db,
                archiveSalary.getIdPeriod(), archiveSalary.getName(),
                archiveSalary.getQuota(), archiveSalary.getSave());
    }

    @Override
    public void deleteSalary(int id) {
        db.delete(FinanceDatabaseHelper.SALARY_TABLE, FinanceDatabaseHelper._id + "=?", new String[]{String.valueOf(id)});
    }

    @Override
    public void deleteSpendingCategory(int id) {
        db.delete(FinanceDatabaseHelper.CATEGORIES_TABLE, FinanceDatabaseHelper._id + "=?", new String[]{String.valueOf(id)});
    }

    @Override
    public void deleteFixedOutgoing(int id) {
        db.delete(FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_TABLE, FinanceDatabaseHelper._id + "=?", new String[]{String.valueOf(id)});
    }

    @Override
    public void deleteFixedOutgoingCategory(int id) {
        db.delete(FinanceDatabaseHelper.FIXED_OUTGOINGS_TABLE, FinanceDatabaseHelper._id + "=?", new String[]{String.valueOf(id)});

    }

    @Override
    public void deleteSpending(int id) {
        db.delete(FinanceDatabaseHelper.CURRENT_SPENDINGS_TABLE, FinanceDatabaseHelper._id + "=?", new String[]{String.valueOf(id)});
    }

    @Override
    public void deletePeriod(int id) {
        db.delete(FinanceDatabaseHelper.PERIOD_TABLE, FinanceDatabaseHelper._id + "=?", new String[]{String.valueOf(id)});
    }

    @Override
    public void deleteArchiveSpending(int id) {
        db.delete(FinanceDatabaseHelper.ARCHIVE_SPENDINGS_TABLE, FinanceDatabaseHelper._id + "=?", new String[]{String.valueOf(id)});
    }

    @Override
    public void deleteArchiveFixedOutgoing(int id) {
        db.delete(FinanceDatabaseHelper.ARCHIVE_FIXED_OUTGOINGS_TABLE, FinanceDatabaseHelper._id + "=?", new String[]{String.valueOf(id)});
    }

    @Override
    public void deleteArchiveSalary(int id) {
        db.delete(FinanceDatabaseHelper.ARCHIVE_SALARY_TABLE, FinanceDatabaseHelper._id + "=?", new String[]{String.valueOf(id)});
    }

    @Override
    public void updateSalary(Salary model) {
        ContentValues valueUpdate = new ContentValues();
        valueUpdate.put(FinanceDatabaseHelper.SALARY_NAME, model.getName());
        valueUpdate.put(FinanceDatabaseHelper.SALARY_INCOME, model.getQuota());

        if (model.getSave()) {
            valueUpdate.put(FinanceDatabaseHelper.SALARY_SAVE, 1);
        } else {
            valueUpdate.put(FinanceDatabaseHelper.SALARY_SAVE, 0);
        }
        db.update(FinanceDatabaseHelper.SALARY_TABLE, valueUpdate,
                FinanceDatabaseHelper._id + " = ?", new String[]{String.valueOf(model.getId())});
    }

    @Override
    public void updateSpendingCategory(CategoryAndIncome model) {
        ContentValues valueUpdate = new ContentValues();
        valueUpdate.put(FinanceDatabaseHelper.CATEGORIES_NAME, model.getName());
        if (model.getIncome() != null) {
            valueUpdate.put(FinanceDatabaseHelper.CATEGORIES_INCOME, model.getIncome());
        }
        if (model.getActive()) {
            valueUpdate.put(FinanceDatabaseHelper.CATEGORIES_ACTIVE, 1);
        } else {
            valueUpdate.put(FinanceDatabaseHelper.CATEGORIES_ACTIVE, 0);
        }
        db.update(FinanceDatabaseHelper.CATEGORIES_TABLE, valueUpdate,
                FinanceDatabaseHelper._id + " = ?", new String[]{String.valueOf(model.getId())});
    }

    @Override
    public void updateFixedOutgoingCategory(Category model) {
        ContentValues valueUpdate = new ContentValues();
        valueUpdate.put(FinanceDatabaseHelper.FIXED_OUTGOINGS_NAME, model.getName());
        if (model.getActive()) {
            valueUpdate.put(FinanceDatabaseHelper.FIXED_OUTGOINGS_ACTIVE, 1);
        } else {
            valueUpdate.put(FinanceDatabaseHelper.FIXED_OUTGOINGS_ACTIVE, 0);
        }
        db.update(FinanceDatabaseHelper.FIXED_OUTGOINGS_TABLE, valueUpdate,
                FinanceDatabaseHelper._id + " = ?", new String[]{String.valueOf(model.getId())});
    }

    @Override
    public void updateFixedOutgoing(FixedOutgoing model) {
        ContentValues valueUpdate = new ContentValues();
        valueUpdate.put(FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_OUTCOME, model.getOutcome());
        valueUpdate.put(FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_DUE_DATE, formatFixedOutgoingSQLDate(model.getDueDate()));
        valueUpdate.put(FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_PAYMENT_DATE, formatFixedOutgoingSQLDate(model.getPaymentDate()));
        if (model.getIsPaid()) {
            valueUpdate.put(FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_PAY, 1);
        } else {
            valueUpdate.put(FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_PAY, 0);
        }
        db.update(FinanceDatabaseHelper.CURRENT_FIXED_OUTGOINGS_TABLE, valueUpdate,
                FinanceDatabaseHelper._id + " = ?", new String[]{String.valueOf(model.getId())});
    }

    @Override
    public void updateSpending(Spending model) {
        Cursor cursor = db.query(FinanceDatabaseHelper.CATEGORIES_TABLE,
                new String[]{FinanceDatabaseHelper._id, FinanceDatabaseHelper.CURRENT_SPENDINGS_ID_CAT},
                FinanceDatabaseHelper.CURRENT_SPENDINGS_ID_CAT + "=?", new String[]{String.valueOf(model.getId())},
                null, null, null);
        ContentValues valueUpdate = new ContentValues();
        valueUpdate.put(FinanceDatabaseHelper.CURRENT_SPENDINGS_ID_CAT, cursor.getInt(cursor.getColumnIndex(FinanceDatabaseHelper.CURRENT_SPENDINGS_ID_CAT)));
        valueUpdate.put(FinanceDatabaseHelper.CURRENT_SPENDINGS_DATE, sqlDateFormatter.formatToString(model.getDate()));
        valueUpdate.put(FinanceDatabaseHelper.CURRENT_SPENDINGS_OUTCOME, model.getQuota());
        db.update(FinanceDatabaseHelper.CURRENT_SPENDINGS_TABLE, valueUpdate,
                FinanceDatabaseHelper._id + " = ?", new String[]{String.valueOf(model.getId())});
        cursor.close();
    }

    @Override
    public void updatePeriod(Period model) {
        ContentValues valueUpdate = new ContentValues();
        valueUpdate.put(FinanceDatabaseHelper.PERIOD_END_DATE, sqlDateFormatter.formatToString(model.getEndDate()));
        valueUpdate.put(FinanceDatabaseHelper.PERIOD_VARIANT, model.getVariant().toString());
        valueUpdate.put(FinanceDatabaseHelper.PERIOD_SAVINGS, model.getSavings());
        db.update(FinanceDatabaseHelper.PERIOD_TABLE, valueUpdate, FinanceDatabaseHelper._id + " = ?", new String[]{String.valueOf(model.getId())});
    }
}
