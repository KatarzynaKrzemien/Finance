package com.katarzynakrzemien.finance.summary.annual;

import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.ArchiveFixedOutgoing;
import com.katarzynakrzemien.finance.database.models.Period;
import com.katarzynakrzemien.finance.summary.SummaryContract;

import java.util.List;

public class ArchiveAnnualSummaryPresenter implements SummaryContract.Presenter {

    private SummaryContract.View view;

    @Override
    public void fetchTotalSpendingSum(DatabaseStore databaseStore, Integer dataControlValue) {
        double archiveAnnualTotalSpending = 0.0;
        for (Period period : databaseStore.getArchivePeriods()) {
            if (period.getStartYear() == dataControlValue) {
                archiveAnnualTotalSpending += databaseStore.getArchiveSpendingSum(period.getId()) +
                        databaseStore.getArchiveFixedOutgoingsSum(period.getId());
            }
        }
        view.setTotalSpendingSum(archiveAnnualTotalSpending);
    }

    @Override
    public void fetchSpendingSum(DatabaseStore databaseStore, Integer dataControlValue) {
        double archiveAnnualSpending = 0.0;
        for (Period period : databaseStore.getArchivePeriods()) {
            if (period.getStartYear() == dataControlValue) {
                archiveAnnualSpending += databaseStore.getArchiveSpendingSum(period.getId());
            }
        }
        view.setSpendingSum(archiveAnnualSpending);
    }

    @Override
    public void fetchFixedOutgoingsSum(DatabaseStore databaseStore, Integer dataControlValue) {
        double archiveAnnualFixedOutgoingSum = 0.0;
        for (Period period : databaseStore.getArchivePeriods()) {
            if (period.getStartYear() == dataControlValue) {
                archiveAnnualFixedOutgoingSum += databaseStore.getArchiveFixedOutgoingsSum(period.getId());
            }
        }
        view.setFixedOutgoingsSum(archiveAnnualFixedOutgoingSum);
    }

    @Override
    public void fetchPaidFixedOutgoingsSum(DatabaseStore databaseStore, Integer dataControlValue) {
    }

    @Override
    public void fetchUnpaidFixedOutgoingsSum(DatabaseStore databaseStore, Integer dataControlValue) {
    }

    @Override
    public void fetchSavings(DatabaseStore databaseStore, PreferenceStore preferenceStore, Integer dataControlValue) {
        double archiveAnnualSavings = 0.0;
        for (Period period : databaseStore.getArchivePeriods()) {
            if (period.getStartYear() == dataControlValue) {
                archiveAnnualSavings += period.getSavings();
            }
        }
        view.setFixedOutgoingsSum(archiveAnnualSavings);
    }

    @Override
    public void fetchSalary(DatabaseStore databaseStore, Integer dataControlValue) {
        double archiveAnnualSalary = 0.0;
        for (Period period : databaseStore.getArchivePeriods()) {
            if (period.getStartYear() == dataControlValue) {
                archiveAnnualSalary += databaseStore.getArchiveSalarySum(period.getId());
            }
        }
        view.setFixedOutgoingsSum(archiveAnnualSalary);
    }

    @Override
    public void fetchAvailableFunds(DatabaseStore databaseStore, PreferenceStore preferenceStore, Integer dataControlValue) {
        double archiveAnnualRemainingFunds = 0.0;
        for (Period period : databaseStore.getArchivePeriods()) {
            if (period.getStartYear() == dataControlValue) {
                archiveAnnualRemainingFunds += databaseStore.getArchiveSalarySum(period.getId()) -
                        databaseStore.getArchiveSpendingSum(period.getId()) -
                        databaseStore.getArchiveFixedOutgoingsSum(period.getId());
            }
        }
        view.setAvailableFunds(archiveAnnualRemainingFunds);
    }

    @Override
    public void attachView(SummaryContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }
}
