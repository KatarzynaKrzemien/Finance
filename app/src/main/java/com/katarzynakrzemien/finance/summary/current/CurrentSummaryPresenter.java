package com.katarzynakrzemien.finance.summary.current;

import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.TotalSalary;
import com.katarzynakrzemien.finance.summary.SummaryContract;

public class CurrentSummaryPresenter implements SummaryContract.Presenter {

    private SummaryContract.View view;

    @Override
    public void attachView(SummaryContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void fetchTotalSpendingSum(DatabaseStore databaseStore, Integer dataControlValue) {
        view.setTotalSpendingSum(databaseStore.getSpendingSum() + databaseStore.getPaidFixedOutgoingsSum());
    }

    @Override
    public void fetchSpendingSum(DatabaseStore databaseStore, Integer dataControlValue) {
        view.setSpendingSum(databaseStore.getSpendingSum());
    }

    @Override
    public void fetchFixedOutgoingsSum(DatabaseStore databaseStore, Integer dataControlValue) {
        view.setFixedOutgoingsSum(databaseStore.getPaidFixedOutgoingsSum());
    }

    @Override
    public void fetchPaidFixedOutgoingsSum(DatabaseStore databaseStore, Integer dataControlValue) {
        view.setPaidFixedOutgoingsSum(databaseStore.getPaidFixedOutgoingsSum());
    }

    @Override
    public void fetchUnpaidFixedOutgoingsSum(DatabaseStore databaseStore, Integer dataControlValue) {
        view.setUnpaidFixedOutgoingsSum(databaseStore.getUnpaidFixedOutgoingsSum());
    }

    @Override
    public void fetchSavings(DatabaseStore databaseStore, PreferenceStore preferenceStore, Integer dataControlValue) {
        view.setSavings(preferenceStore.getMonthlySavings() + databaseStore.getSavedSalarySum());
    }

    @Override
    public void fetchSalary(DatabaseStore databaseStore, Integer dataControlValue) {
        double salary = 0.0;
        TotalSalary model = databaseStore.getSalaries();
        for (int i = 0; i < model.getSalaryListSize(); i++) {
            salary += model.get(i).getQuota();
        }
        salary += model.getLastMonthRemainingFunds().getQuota();
        salary += model.getFromSavings().getQuota();
        view.setSalary(salary);
    }

    @Override
    public void fetchAvailableFunds(DatabaseStore databaseStore, PreferenceStore preferenceStore, Integer dataControlValue) {
        view.setAvailableFunds(databaseStore.getSavedSalarySum() - databaseStore.getSpendingSum() - databaseStore.getPaidFixedOutgoingsSum() - preferenceStore.getMonthlySavings());
    }
}
