package com.katarzynakrzemien.finance.summary.archive;

import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.ArchiveFixedOutgoing;
import com.katarzynakrzemien.finance.database.models.Period;
import com.katarzynakrzemien.finance.summary.SummaryContract;

public class ArchiveSummaryPresenter implements SummaryContract.Presenter {

    private SummaryContract.View view;

    @Override
    public void fetchTotalSpendingSum(DatabaseStore databaseStore, Integer dataControlValue) {
        Period period = databaseStore.getPeriod(dataControlValue);
        double archiveTotalSpending = databaseStore.getArchiveSpendingSum(period.getId()) + databaseStore.getArchiveFixedOutgoingsSum(period.getId());
        view.setTotalSpendingSum(archiveTotalSpending);
    }

    @Override
    public void fetchSpendingSum(DatabaseStore databaseStore, Integer dataControlValue) {
        Period period = databaseStore.getPeriod(dataControlValue);
        view.setSpendingSum(databaseStore.getArchiveSpendingSum(period.getId()));
    }

    @Override
    public void fetchFixedOutgoingsSum(DatabaseStore databaseStore, Integer dataControlValue) {
        Period period = databaseStore.getPeriod(dataControlValue);
        view.setFixedOutgoingsSum(databaseStore.getArchiveFixedOutgoingsSum(period.getId()));
    }

    @Override
    public void fetchPaidFixedOutgoingsSum(DatabaseStore databaseStore, Integer dataControlValue) {
        Period period = databaseStore.getPeriod(dataControlValue);
        double paidSum = 0.0;
        for (ArchiveFixedOutgoing archiveFixedOutgoing : databaseStore.getArchiveFixedOutgoings(period.getId())) {
            paidSum += archiveFixedOutgoing.getPaidSum();
        }
        view.setFixedOutgoingsSum(paidSum);
    }

    @Override
    public void fetchUnpaidFixedOutgoingsSum(DatabaseStore databaseStore, Integer dataControlValue) {
        Period period = databaseStore.getPeriod(dataControlValue);
        double unpaidSum = 0.0;
        for (ArchiveFixedOutgoing archiveFixedOutgoing : databaseStore.getArchiveFixedOutgoings(period.getId())) {
            unpaidSum += archiveFixedOutgoing.getUnpaidSum();
        }
        view.setFixedOutgoingsSum(unpaidSum);
    }

    @Override
    public void fetchSavings(DatabaseStore databaseStore, PreferenceStore preferenceStore, Integer dataControlValue) {
        Period period = databaseStore.getPeriod(dataControlValue);
        view.setSavings(period.getSavings());
    }

    @Override
    public void fetchSalary(DatabaseStore databaseStore, Integer dataControlValue) {
        Period period = databaseStore.getPeriod(dataControlValue);
        view.setSalary(databaseStore.getArchiveSalarySum(period.getId()));
    }

    @Override
    public void fetchAvailableFunds(DatabaseStore databaseStore, PreferenceStore preferenceStore, Integer dataControlValue) {
        Period period = databaseStore.getPeriod(dataControlValue);
        view.setAvailableFunds(databaseStore.getArchiveSalarySum(period.getId()) -
                databaseStore.getArchiveSpendingSum(period.getId()) -
                databaseStore.getArchiveFixedOutgoingsSum(period.getId()));
    }

    @Override
    public void attachView(SummaryContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }
}
