package com.katarzynakrzemien.finance.summary;

import com.katarzynakrzemien.finance.BaseContract;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceStore;

public class SummaryContract {

    public interface View extends BaseContract.View {

        void setTotalSpendingSum(double totalSpendingSum);

        void setSpendingSum(double spendingSum);

        void setFixedOutgoingsSum(double fixedOutgoingsSum);

        void setPaidFixedOutgoingsSum(double paidFixedOutgoingsSum);

        void setUnpaidFixedOutgoingsSum(double unpaidFixedOutgoingsSum);

        void setSavings(double savings);

        void setSalary(double salary);

        void setAvailableFunds(double availableFunds);
    }

    public interface Presenter extends BaseContract.Presenter<SummaryContract.View> {
        void fetchTotalSpendingSum(DatabaseStore databaseStore, Integer dataControlValue);

        void fetchSpendingSum(DatabaseStore databaseStore, Integer dataControlValue);

        void fetchFixedOutgoingsSum(DatabaseStore databaseStore, Integer dataControlValue);

        void fetchPaidFixedOutgoingsSum(DatabaseStore databaseStore, Integer dataControlValue);

        void fetchUnpaidFixedOutgoingsSum(DatabaseStore databaseStore, Integer dataControlValue);

        void fetchSavings(DatabaseStore databaseStore, PreferenceStore preferenceStore, Integer dataControlValue);

        void fetchSalary(DatabaseStore databaseStore, Integer dataControlValue);

        void fetchAvailableFunds(DatabaseStore databaseStore, PreferenceStore preferenceStore, Integer dataControlValue);
    }
}
