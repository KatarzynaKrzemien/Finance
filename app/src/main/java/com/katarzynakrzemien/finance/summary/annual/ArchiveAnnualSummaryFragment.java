package com.katarzynakrzemien.finance.summary.annual;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.database.models.variants.Variants;
import com.katarzynakrzemien.finance.summary.BaseSummaryFragment;

public class ArchiveAnnualSummaryFragment extends BaseSummaryFragment {

    private static String YEAR_KEY = "year";

    public static ArchiveAnnualSummaryFragment newInstance(int year) {
        ArchiveAnnualSummaryFragment fragment = new ArchiveAnnualSummaryFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(YEAR_KEY, year);
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = new ArchiveAnnualSummaryPresenter();
        variant = Variants.FULLPLANNING.toString();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getArguments() != null) {
            dataControlValue = getArguments().getInt(YEAR_KEY);
        } else {
            throw (new IllegalArgumentException());
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        hidePaidUnpaidFixedOutgoingsSummary();
    }
}
