package com.katarzynakrzemien.finance.summary.archive;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.summary.BaseSummaryFragment;

public class ArchiveSummaryFragment extends BaseSummaryFragment {

    public static final String TAG = "CurrentSummaryFragment";
    private static String VARIANT_KEY = "variant";
    private static String ID_PERIOD_KEY = "id_period";

    public static ArchiveSummaryFragment newInstance(String variant, int idPeriod) {
        ArchiveSummaryFragment fragment = new ArchiveSummaryFragment();
        Bundle arguments = new Bundle();
        arguments.putString(VARIANT_KEY, variant);
        arguments.putInt(ID_PERIOD_KEY, idPeriod);
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        presenter = new ArchiveSummaryPresenter();
        savingsStringId = R.string.monthlySavings;
        super.onAttach(context);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            variant = getArguments().getString(VARIANT_KEY);
            dataControlValue = getArguments().getInt(ID_PERIOD_KEY);
        }
    }
}
