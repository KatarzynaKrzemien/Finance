package com.katarzynakrzemien.finance.summary;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.katarzynakrzemien.finance.App;
import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.database.DatabaseQuery;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceFromDatabase;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.variants.Variants;
import com.katarzynakrzemien.finance.summary.current.CurrentSummaryPresenter;
import com.katarzynakrzemien.finance.utils.formatter.amount.AmountFormatter;

public abstract class BaseSummaryFragment extends Fragment implements SummaryContract.View {

    protected static String VARIANT_KEY = "variant";
    protected SummaryContract.Presenter presenter = new CurrentSummaryPresenter();
    protected DatabaseStore databaseStore;
    protected PreferenceStore preferenceStore;
    protected String variant = null;
    protected int savingsStringId = R.string.savings;
    protected Integer dataControlValue = null;
    protected AmountFormatter amountFormatter;
    private View view;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter.attachView(this);
        databaseStore = new DatabaseQuery(context);
        preferenceStore = new PreferenceFromDatabase(context);
        amountFormatter = ((App) context.getApplicationContext()).amountFormatter;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_summary, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        view = getView();
        if (view != null) {
            presenter.fetchSpendingSum(databaseStore, dataControlValue);
            presenter.fetchFixedOutgoingsSum(databaseStore, dataControlValue);
            presenter.fetchPaidFixedOutgoingsSum(databaseStore, dataControlValue);
            presenter.fetchUnpaidFixedOutgoingsSum(databaseStore, dataControlValue);
            presenter.fetchTotalSpendingSum(databaseStore, dataControlValue);
            presenter.fetchSavings(databaseStore, preferenceStore, dataControlValue);
            if (variant != null) {
                if (!variant.equals(Variants.MONITORING.toString())) {
                    presenter.fetchSalary(databaseStore, dataControlValue);
                    presenter.fetchAvailableFunds(databaseStore, preferenceStore, dataControlValue);
                } else {
                    hidePlanningSummary();
                }
            }
        }
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }

    @Override
    public void setTotalSpendingSum(double totalSpendingSum) {
        ((TextView) view.findViewById(R.id.total_spending)).setText(amountFormatter.format(totalSpendingSum));
    }

    @Override
    public void setSpendingSum(double spendingSum) {
        ((TextView) view.findViewById(R.id.spending)).setText(amountFormatter.format(spendingSum));
    }

    @Override
    public void setFixedOutgoingsSum(double fixedOutgoingsSum) {
        ((TextView) view.findViewById(R.id.fixed_outgoings)).setText(amountFormatter.format(fixedOutgoingsSum));
    }

    @Override
    public void setPaidFixedOutgoingsSum(double paidFixedOutgoingsSum) {
        ((TextView) view.findViewById(R.id.fixed_outgoings_paid)).setText(amountFormatter.format(paidFixedOutgoingsSum));
    }

    @Override
    public void setUnpaidFixedOutgoingsSum(double unpaidFixedOutgoingsSum) {
        ((TextView) view.findViewById(R.id.fixed_outgoings_unpaid)).setText(amountFormatter.format(unpaidFixedOutgoingsSum));
    }

    @Override
    public void setSavings(double savings) {
        ((TextView) view.findViewById(R.id.savings)).setText(amountFormatter.format(savings));
        ((TextView) view.findViewById(R.id.savings_title)).setText(getString(savingsStringId));
    }

    @Override
    public void setSalary(double salary) {
        ((TextView) view.findViewById(R.id.salary)).setText(amountFormatter.format(salary));
    }

    @Override
    public void setAvailableFunds(double availableFunds) {
        ((TextView) view.findViewById(R.id.remaining_funds)).setText(amountFormatter.format(availableFunds));
    }

    private void hidePlanningSummary() {
        view.findViewById(R.id.planning_summary_group).setVisibility(View.GONE);
    }

    protected void hidePaidUnpaidFixedOutgoingsSummary() {
        view.findViewById(R.id.paid_unpaid_fixed_outgoings_group).setVisibility(View.GONE);
    }
}
