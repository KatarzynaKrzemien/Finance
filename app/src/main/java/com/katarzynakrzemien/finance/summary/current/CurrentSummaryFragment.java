package com.katarzynakrzemien.finance.summary.current;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;

import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.summary.BaseSummaryFragment;

public class CurrentSummaryFragment extends BaseSummaryFragment {

    public static final String TAG = "CurrentSummaryFragment";

    public static CurrentSummaryFragment newInstance(String variant) {
        CurrentSummaryFragment fragment = new CurrentSummaryFragment();
        Bundle arguments = new Bundle();
        arguments.putString(VARIANT_KEY, variant);
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        presenter = new CurrentSummaryPresenter();
        savingsStringId = R.string.monthlySavings;
        super.onAttach(context);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            variant = getArguments().getString(VARIANT_KEY);
        }
    }
}
