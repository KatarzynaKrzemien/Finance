package com.katarzynakrzemien.finance;

import com.katarzynakrzemien.finance.database.DatabaseStore;

import java.util.List;


public abstract class BaseRecyclerContract {

    public interface View<T> extends BaseContract.View {

        void renderList(List<T> list);

    }

    public interface Presenter<T extends BaseRecyclerContract.View> extends BaseContract.Presenter<T> {

        void fetchList(DatabaseStore databaseStore);

    }
}
