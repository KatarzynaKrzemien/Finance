package com.katarzynakrzemien.finance.archive;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.archive.fixedoutgoings.ArchiveFixedOutgoingsFragment;
import com.katarzynakrzemien.finance.archive.salary.ArchiveSalaryFragment;
import com.katarzynakrzemien.finance.archive.spending.ArchiveFullPlanningSpendingFragment;
import com.katarzynakrzemien.finance.archive.spending.ArchiveSpendingFragment;
import com.katarzynakrzemien.finance.charts.annual.fixedoutgoing.ArchiveAnnualFixedOutgoingsChartFragment;
import com.katarzynakrzemien.finance.charts.annual.salary.ArchiveAnnualSalaryChartFragment;
import com.katarzynakrzemien.finance.charts.annual.spending.ArchiveAnnualSpendingChartFragment;
import com.katarzynakrzemien.finance.database.DatabaseQuery;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.models.variants.Variants;
import com.katarzynakrzemien.finance.summary.annual.ArchiveAnnualSummaryFragment;
import com.katarzynakrzemien.finance.summary.archive.ArchiveSummaryFragment;
import com.katarzynakrzemien.finance.utils.ViewPagerAdapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

public class ArchiveFragment extends Fragment implements ArchiveContract.View {

    public static final String TAG = "ARCHIVE";
    private Spinner spinnerMonth;
    private Spinner spinnerYears;
    private int year;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private List<String> yearsList = new ArrayList<>();
    private List<String> monthsList = new ArrayList<>();
    private ArchiveContract.Presenter presenter = new ArchivePresenter();
    private DatabaseStore databaseStore;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        databaseStore = new DatabaseQuery(context);
        presenter.attachView(this);
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_archive, container, false);
        viewPager = view.findViewById(R.id.viewpager);
        TabLayout tabLayout = view.findViewById(R.id.tab);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        spinnerYears = view.findViewById(R.id.spinnerYear);
        spinnerMonth = view.findViewById(R.id.spinnerMonth);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter.fetchIsArchivePeriod(databaseStore);
    }

    private void setupViewPager(ViewPager viewPager) {
        if (spinnerMonth != null) {
            viewPagerAdapter.clearAllFragments();

            if (spinnerMonth.getSelectedItem().toString().equals(getString(R.string.annualSummary))) {
                viewPagerAdapter.addFragment(ArchiveAnnualSummaryFragment.
                        newInstance(year), getString(R.string.summary));
                viewPagerAdapter.addFragment(ArchiveAnnualSpendingChartFragment.
                        newInstance(year), getString(R.string.spending));
                viewPagerAdapter.addFragment(ArchiveAnnualFixedOutgoingsChartFragment.
                        newInstance(year), getString(R.string.fixedOutgoings));
                viewPagerAdapter.addFragment(ArchiveAnnualSalaryChartFragment.
                        newInstance(year), getString(R.string.salary));

            } else {
                presenter.fetchPeriodIdAndVariant(databaseStore, spinnerMonth.getSelectedItem().toString(), spinnerYears.getSelectedItem().toString());
            }
            viewPagerAdapter.notifyDataSetChanged();
            viewPager.invalidate();

        } else {
            viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
            viewPager.setAdapter(viewPagerAdapter);
        }
    }

    private void prepareMonthSpinner() {
        presenter.fetchMonthsList(databaseStore, spinnerYears.getSelectedItem().toString());
        ArrayAdapter<String> adapterMonth = new ArrayAdapter<>(Objects.requireNonNull(getActivity()), R.layout.spinner_item, monthsList);
        spinnerMonth.setAdapter(adapterMonth);
        spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setupViewPager(viewPager);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void prepareYearSpinner() {
        presenter.fetchYearsList(databaseStore);
        ArrayAdapter<String> adapterYear = new ArrayAdapter<>(Objects.requireNonNull(getActivity()), R.layout.spinner_item, yearsList);
        spinnerYears.setAdapter(adapterYear);
        spinnerYears.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                year = Integer.parseInt(spinnerYears.getSelectedItem().toString());
                prepareMonthSpinner();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void renderYearsList(List<String> yearsList) {
        this.yearsList.clear();
        this.yearsList.addAll(yearsList);
    }

    @Override
    public void renderMonthsList(List<String> monthsList) {
        this.monthsList.clear();
        if (Integer.parseInt(spinnerYears.getSelectedItem().toString()) < Calendar.getInstance().get(Calendar.YEAR)) {
            this.monthsList.add(getString(R.string.annualSummary));
        }
        this.monthsList.addAll(monthsList);
    }

    @Override
    public void setPeriodIdAndVariant(int idPeriod, Variants variant) {
        viewPagerAdapter.addFragment(ArchiveSummaryFragment.newInstance(variant.toString(), idPeriod), getString(R.string.summary));
        if (variant.equals(Variants.FULLPLANNING)) {
            viewPagerAdapter.addFragment(ArchiveFullPlanningSpendingFragment.newInstance(idPeriod), getString(R.string.spending));
        } else {
            viewPagerAdapter.addFragment(ArchiveSpendingFragment.newInstance(idPeriod), getString(R.string.spending));
        }
        viewPagerAdapter.addFragment(ArchiveFixedOutgoingsFragment.newInstance(idPeriod), getString(R.string.fixedOutgoings));
        if (!variant.equals(Variants.MONITORING)) {
            viewPagerAdapter.addFragment(ArchiveSalaryFragment.newInstance(idPeriod), getString(R.string.salary));
        }
    }

    @Override
    public void isArchivePeriod(boolean isArchivePeriod) {
        if (isArchivePeriod) {
            Objects.requireNonNull(getView()).findViewById(R.id.emptyArchive).setVisibility(View.GONE);
            prepareYearSpinner();
        }
    }
}

