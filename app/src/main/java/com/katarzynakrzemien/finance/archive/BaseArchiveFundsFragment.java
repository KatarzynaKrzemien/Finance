package com.katarzynakrzemien.finance.archive;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.katarzynakrzemien.finance.App;
import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.database.DatabaseQuery;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceFromDatabase;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.utils.formatter.amount.AmountFormatter;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseArchiveFundsFragment<T extends RecyclerView.Adapter, M> extends Fragment implements ArchiveSpendingContract.View<M> {

    protected static String ID_PERIOD_KEY = "id_period";
    protected View view = null;
    protected List<M> list = new ArrayList<>();
    protected T adapter;
    protected ArchiveSpendingContract.Presenter presenter;
    protected DatabaseStore databaseStore;
    protected PreferenceStore preferenceStore;
    protected AmountFormatter amountFormatter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        amountFormatter = ((App) context.getApplicationContext()).amountFormatter;
        databaseStore = new DatabaseQuery(context);
        preferenceStore = new PreferenceFromDatabase(context);
        presenter.attachView(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            presenter.fetchList(databaseStore, getArguments().getInt(ID_PERIOD_KEY));
        } else {
            throw (new IllegalArgumentException());
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        view = getView();
        if (view != null) {

            RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
            recyclerView.setAdapter(adapter);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.animate();
        }
    }

    @Override
    public void renderList(List<M> list) {
        this.list.clear();
        this.list.addAll(list);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }

}
