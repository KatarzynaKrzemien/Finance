package com.katarzynakrzemien.finance.archive.fixedoutgoings;

import android.content.Context;
import android.view.View;

import com.katarzynakrzemien.finance.database.models.ArchiveFixedOutgoing;
import com.katarzynakrzemien.finance.database.models.FixedOutgoing;
import com.katarzynakrzemien.finance.fixedoutgoings.BaseFixedOutgoingsAdapter;

import java.util.List;

public class ArchiveFixedOutgoingsAdapter extends BaseFixedOutgoingsAdapter<ArchiveFixedOutgoing> {


    ArchiveFixedOutgoingsAdapter(List<ArchiveFixedOutgoing> fixedOutgoingsList, Context context) {
        super(fixedOutgoingsList, context);
    }

    @Override
    protected View.OnClickListener getDueDateClick(FixedOutgoing fixedOutgoing) {
        return null;
    }

    @Override
    protected View.OnClickListener getPaymentDateClick(FixedOutgoing fixedOutgoing) {
        return null;
    }

    @Override
    protected View.OnClickListener getPaidClick(FixedOutgoing fixedOutgoing) {
        return null;
    }

    @Override
    protected View.OnClickListener getOutcomeClick(FixedOutgoing fixedOutgoing) {
        return null;
    }
}
