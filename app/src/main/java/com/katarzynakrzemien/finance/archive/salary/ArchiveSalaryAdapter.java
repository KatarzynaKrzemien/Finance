package com.katarzynakrzemien.finance.archive.salary;

import android.content.Context;
import android.view.View;

import com.katarzynakrzemien.finance.database.models.ArchiveSalary;
import com.katarzynakrzemien.finance.settings.salary.BaseSalaryAdapter;

import java.util.List;

public class ArchiveSalaryAdapter extends BaseSalaryAdapter<ArchiveSalary> {


    ArchiveSalaryAdapter(List<ArchiveSalary> archiveList, Context context) {
        super(archiveList, context);
    }

    @Override
    protected View.OnClickListener getOnItemClick(int position) {
        return null;
    }
}
