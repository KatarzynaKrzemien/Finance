package com.katarzynakrzemien.finance.archive;

import com.katarzynakrzemien.finance.BaseContract;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.models.variants.Variants;

import java.util.List;

public class ArchiveContract {

    public interface View extends BaseContract.View {

        void renderYearsList(List<String> yearsList);

        void renderMonthsList(List<String> monthsList);

        void setPeriodIdAndVariant(int idPeriod, Variants variant);

        void isArchivePeriod(boolean isArchivePeriod);
    }


    public interface Presenter extends BaseContract.Presenter<View> {

        void fetchYearsList(DatabaseStore databaseStore);

        void fetchMonthsList(DatabaseStore databaseStore, String year);

        void fetchPeriodIdAndVariant(DatabaseStore databaseStore, String monthListItem, String yearListItem);

        void fetchIsArchivePeriod(DatabaseStore databaseStore);
    }
}
