package com.katarzynakrzemien.finance.archive.spending;

import com.katarzynakrzemien.finance.archive.ArchiveSpendingContract;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.models.ArchiveSpending;

public class ArchiveSpendingPresenter implements ArchiveSpendingContract.Presenter {
    private ArchiveSpendingContract.View<ArchiveSpending> view;

    @Override
    public void fetchList(DatabaseStore databaseStore, int periodId) {
        view.renderList(databaseStore.getArchiveSpending(periodId));
    }

    @Override
    public void attachView(ArchiveSpendingContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

}
