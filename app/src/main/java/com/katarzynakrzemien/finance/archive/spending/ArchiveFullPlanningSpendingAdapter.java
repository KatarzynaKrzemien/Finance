package com.katarzynakrzemien.finance.archive.spending;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import com.katarzynakrzemien.finance.database.models.ArchiveSpending;
import com.katarzynakrzemien.finance.spending.planning.AvailableFundsColorEvaluator;

import java.util.List;

public class ArchiveFullPlanningSpendingAdapter extends ArchiveSpendingAdapter {

    private AvailableFundsColorEvaluator availableFundsColorEvaluator;

    ArchiveFullPlanningSpendingAdapter(Context context, List<ArchiveSpending> archiveList, AvailableFundsColorEvaluator availableFundsColorEvaluator) {
        super(context, archiveList);
        this.availableFundsColorEvaluator = availableFundsColorEvaluator;
    }

    @Override
    public void onBindViewHolder(@NonNull SpendingViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        setupAvailableFundsView(holder, archiveSpendingList.get(position));
    }

    private void setupAvailableFundsView(SpendingViewHolder holder, ArchiveSpending archiveSpending) {
        double availableFunds = archiveSpending.getIncome() - archiveSpending.getOutcome();
        holder.availableFunds.setText(amountFormatter.format(availableFunds));
        holder.itemView.setBackgroundColor(availableFundsColorEvaluator.getBackgroundColor(availableFunds));
        holder.availableFunds.setTextColor(availableFundsColorEvaluator.getTextColor(availableFunds, archiveSpending.getIncome()));
    }

    @Override
    protected int getAvailableFundsVisibility() {
        return View.VISIBLE;
    }

}
