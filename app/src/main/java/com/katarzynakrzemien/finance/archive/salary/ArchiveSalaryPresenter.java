package com.katarzynakrzemien.finance.archive.salary;

import com.katarzynakrzemien.finance.archive.ArchiveSpendingContract;
import com.katarzynakrzemien.finance.database.DatabaseStore;

public class ArchiveSalaryPresenter implements ArchiveSpendingContract.Presenter {
    private ArchiveSpendingContract.View view;

    @Override
    public void fetchList(DatabaseStore databaseStore, int periodId) {
        view.renderList(databaseStore.getArchiveSalary(periodId));
    }

    @Override
    public void attachView(ArchiveSpendingContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

}
