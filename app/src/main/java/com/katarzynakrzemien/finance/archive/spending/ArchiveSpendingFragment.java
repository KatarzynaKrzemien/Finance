package com.katarzynakrzemien.finance.archive.spending;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.archive.BaseArchiveFundsFragment;
import com.katarzynakrzemien.finance.database.models.ArchiveSpending;

public class ArchiveSpendingFragment extends BaseArchiveFundsFragment<ArchiveSpendingAdapter, ArchiveSpending> {

    public static ArchiveSpendingFragment newInstance(int idPeriod) {
        ArchiveSpendingFragment fragment = new ArchiveSpendingFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ID_PERIOD_KEY, idPeriod);
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_monitoring, container, false);
    }

    @Override
    public void onAttach(Context context) {
        presenter = new ArchiveSpendingPresenter();
        adapter = new ArchiveSpendingAdapter(context, list);
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();
        view.findViewById(R.id.spending_sum_layout).setVisibility(View.GONE);
    }
}
