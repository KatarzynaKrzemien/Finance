package com.katarzynakrzemien.finance.archive;

import com.katarzynakrzemien.finance.BaseContract;
import com.katarzynakrzemien.finance.database.DatabaseStore;

import java.util.List;

public class ArchiveSpendingContract {
    public interface View<T> extends BaseContract.View {

        void renderList(List<T> list);
    }

    public interface Presenter extends BaseContract.Presenter<View> {

        void fetchList(DatabaseStore databaseStore, int periodId);
    }

}
