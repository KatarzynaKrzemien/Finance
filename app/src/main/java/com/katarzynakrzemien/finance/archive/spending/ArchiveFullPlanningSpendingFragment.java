package com.katarzynakrzemien.finance.archive.spending;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.archive.ArchiveSpendingContract;
import com.katarzynakrzemien.finance.archive.BaseArchiveFundsFragment;
import com.katarzynakrzemien.finance.database.models.ArchiveSpending;
import com.katarzynakrzemien.finance.spending.planning.AvailableFundsColorEvaluator;

public class ArchiveFullPlanningSpendingFragment extends BaseArchiveFundsFragment<ArchiveFullPlanningSpendingAdapter, ArchiveSpending> {

    public static ArchiveFullPlanningSpendingFragment newInstance(int idPeriod) {
        ArchiveFullPlanningSpendingFragment fragment = new ArchiveFullPlanningSpendingFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ID_PERIOD_KEY, idPeriod);
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_planning, container, false);
    }

    @Override
    public void onAttach(Context context) {
        presenter = new ArchiveSpendingPresenter();
        AvailableFundsColorEvaluator availableFundsColorEvaluator = new AvailableFundsColorEvaluator(context);
        adapter = new ArchiveFullPlanningSpendingAdapter(context, list, availableFundsColorEvaluator);
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();
        view.findViewById(R.id.planning_sum_layout).setVisibility(View.GONE);
    }
}