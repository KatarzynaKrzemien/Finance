package com.katarzynakrzemien.finance.archive;

import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.models.Period;
import com.katarzynakrzemien.finance.utils.formatter.date.DateFormatPattern;
import com.katarzynakrzemien.finance.utils.formatter.date.DateFormatter;
import com.katarzynakrzemien.finance.utils.formatter.date.DateManger;
import com.katarzynakrzemien.finance.utils.formatter.date.SimpleDateFormatter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ArchivePresenter implements ArchiveContract.Presenter {
    private ArchiveContract.View view;
    private DateFormatter dayMonthFormatter = new SimpleDateFormatter(DateFormatPattern.DAY_MONTH);
    private String monthsSplit = " - ";

    @Override
    public void fetchYearsList(DatabaseStore databaseStore) {
        List<String> yearsList = new ArrayList<>();
        for (Period period : databaseStore.getArchivePeriods()) {
            if (!yearsList.contains(String.valueOf(period.getStartYear()))) {
                yearsList.add(String.valueOf(period.getStartYear()));
            }
        }
        view.renderYearsList(yearsList);
    }

    @Override
    public void fetchMonthsList(DatabaseStore databaseStore, String year) {
        List<String> monthsList = new ArrayList<>();
        for (Period period : databaseStore.getArchivePeriods()) {
            if (period.getStartYear() == Integer.parseInt(year)) {
                monthsList.add(dayMonthFormatter.formatToString(period.getStartDate())
                        + monthsSplit + dayMonthFormatter.formatToString(period.getEndDate()));
            }
        }
        view.renderMonthsList(monthsList);
    }

    @Override
    public void fetchPeriodIdAndVariant(DatabaseStore databaseStore, String monthListItem, String yearListItem) {
        Calendar periodDate = getPeriodStartDate(monthListItem, yearListItem);
        for (Period period : databaseStore.getArchivePeriods()) {
            if (DateManger.compareDates(period.getStartDate(), periodDate)) {
                view.setPeriodIdAndVariant(period.getId(), period.getVariant());
            }
        }
    }

    private Calendar getPeriodStartDate(String monthListItem, String yearListItem) {
        String startDayMonth = getStartDayMonth(monthListItem);
        Calendar periodDate = dayMonthFormatter.parseToCalendar(startDayMonth);
        periodDate.set(Calendar.YEAR, Integer.parseInt(yearListItem));
        return periodDate;
    }

    private String getStartDayMonth(String monthListItem) {
        return monthListItem.split(monthsSplit)[0];
    }

    @Override
    public void fetchIsArchivePeriod(DatabaseStore databaseStore) {
        view.isArchivePeriod(databaseStore.getArchivePeriods().size() != 0);
    }

    @Override
    public void attachView(ArchiveContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }
}
