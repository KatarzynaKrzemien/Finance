package com.katarzynakrzemien.finance.archive.fixedoutgoings;

import com.katarzynakrzemien.finance.archive.ArchiveSpendingContract;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.models.ArchiveFixedOutgoing;

public class ArchiveFixedOutgoingsPresenter implements ArchiveSpendingContract.Presenter {
    private ArchiveSpendingContract.View<ArchiveFixedOutgoing> view;

    @Override
    public void fetchList(DatabaseStore databaseStore, int periodId) {
        view.renderList(databaseStore.getArchiveFixedOutgoings(periodId));
    }

    @Override
    public void attachView(ArchiveSpendingContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

}