package com.katarzynakrzemien.finance.archive.spending;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import com.katarzynakrzemien.finance.App;
import com.katarzynakrzemien.finance.database.models.ArchiveSpending;
import com.katarzynakrzemien.finance.spending.BaseSpendingAdapter;
import com.katarzynakrzemien.finance.utils.formatter.amount.AmountFormatter;

import java.util.List;

public class ArchiveSpendingAdapter extends BaseSpendingAdapter {

    protected List<ArchiveSpending> archiveSpendingList;
    protected AmountFormatter amountFormatter;

    ArchiveSpendingAdapter(Context context, List<ArchiveSpending> archiveSpendingList) {
        this.archiveSpendingList = archiveSpendingList;
        this.amountFormatter = ((App) context.getApplicationContext()).amountFormatter;
    }

    @Override
    public void onBindViewHolder(@NonNull SpendingViewHolder holder, int position) {
        ArchiveSpending archiveSpending = archiveSpendingList.get(position);
        holder.name.setText(archiveSpending.getName());
        holder.outcome.setText(amountFormatter.format(archiveSpending.getOutcome()));
    }

    @Override
    protected int getAvailableFundsVisibility() {
        return View.GONE;
    }

    @Override
    protected int getExpandingLayoutVisibility() {
        return View.GONE;
    }

    @Override
    protected View.OnClickListener getOnItemClick(int position) {
        return null;
    }

    @Override
    public int getItemCount() {
        return archiveSpendingList.size();
    }
}
