package com.katarzynakrzemien.finance.managing.action;

import com.katarzynakrzemien.finance.database.models.variants.Variants;

public interface ActionAppListener {

    void fundsChange();

    void variantChange(Variants variant);

    void dayOfNewMonthChange();

    void itemClicked(Clicked itemClicked);
}
