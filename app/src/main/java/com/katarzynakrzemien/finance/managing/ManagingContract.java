package com.katarzynakrzemien.finance.managing;

import com.katarzynakrzemien.finance.BaseContract;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.variants.Variants;

public class ManagingContract {


    public interface View extends BaseContract.View {

        void setVariant(Variants variant);

        void setDayOfNewMonth(int dayOfNewMonth);

        void setAvailableFunds(double availableFunds);

        void setTotalSpending(double totalSpending);

    }

    public interface Presenter extends BaseContract.Presenter<View> {

        void fetchVariant(PreferenceStore preferenceStore);

        void fetchDayOfNewMonth(PreferenceStore preferenceStore);

        void fetchAvailableFunds(DatabaseStore databaseStore, PreferenceStore preferenceStore);

        void fetchTotalSpending(DatabaseStore databaseStore);

        void updateSalaryLastMonthRemainingFundsSavedStatus(DatabaseStore databaseStore, boolean savedStatus);
    }
}

