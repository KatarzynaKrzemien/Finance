package com.katarzynakrzemien.finance.managing;

import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.ArchiveSalary;
import com.katarzynakrzemien.finance.database.models.ArchiveSpending;
import com.katarzynakrzemien.finance.database.models.CategorizedSpending;
import com.katarzynakrzemien.finance.database.models.FixedOutgoing;
import com.katarzynakrzemien.finance.database.models.Period;
import com.katarzynakrzemien.finance.database.models.Salary;
import com.katarzynakrzemien.finance.database.models.TotalSalary;
import com.katarzynakrzemien.finance.database.models.variants.Variants;
import com.katarzynakrzemien.finance.fixedoutgoings.reminder.FixedOutgoingsWorkerManager;
import com.katarzynakrzemien.finance.utils.formatter.amount.AmountFormatter;
import com.katarzynakrzemien.finance.utils.formatter.date.DateManger;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

class PeriodManager {

    private Period lastPeriod;
    private DatabaseStore databaseStore;
    private PreferenceStore preferenceStore;
    private OnStartAppShowingDialogs dialogs;
    private Calendar today = Calendar.getInstance();
    private Calendar nextPeriodStartDate;
    private List<Period> periodsList;
    private AmountFormatter amountFormatter;

    PeriodManager(DatabaseStore databaseStore, PreferenceStore preferenceStore, OnStartAppShowingDialogs dialogs, AmountFormatter amountFormatter) {
        this.preferenceStore = preferenceStore;
        this.databaseStore = databaseStore;
        this.dialogs = dialogs;
        this.amountFormatter = amountFormatter;
        this.lastPeriod = databaseStore.getCurrentPeriod();
        this.periodsList = databaseStore.getArchivePeriods();
        if (lastPeriod != null) {
            this.periodsList.add(lastPeriod);
        }
    }

    void manage() {
        if (isAnyPeriodExist()) {
            getNextPeriodStartDate();
            if (DateManger.compareDates(today, nextPeriodStartDate) || today.getTime().after(nextPeriodStartDate.getTime())) {
                if (!isThatPeriodExist(nextPeriodStartDate)) {
                    createNewPeriod();
                }
            }
        } else {
            insertPeriod(today, today);
        }
    }

    private void getNextPeriodStartDate() {
        nextPeriodStartDate = DateManger.createDateCalendar(preferenceStore.getDayOfNewMonth(), lastPeriod.getStartMonth(), lastPeriod.getStartYear());
        nextPeriodStartDate.add(Calendar.MONTH, 1);
    }

    private void createNewPeriod() {
        prepareToCreatingNewPeriod();
        if (TimeUnit.DAYS.convert((today.getTimeInMillis() - nextPeriodStartDate.getTimeInMillis()), TimeUnit.MILLISECONDS) > nextPeriodStartDate.getActualMaximum(Calendar.DAY_OF_MONTH)) {
            insertPeriod(today, today);
        } else {
            insertPeriod(nextPeriodStartDate, nextPeriodStartDate);
        }
    }

    private void prepareToCreatingNewPeriod() {
        double monthSavings = preferenceStore.getMonthlySavings();
        Variants variant = preferenceStore.getVariant();
        Salary lastMonthRemainingFunds = databaseStore.getLastMonthRemainingFunds();
        if (variant.equals(Variants.MONITORING)) {
            monthSavings += lastMonthRemainingFunds.getQuota();
            lastMonthRemainingFunds.setQuota(0.0);
            dialogs.showNewPeriodCreatedDialog(nextPeriodStartDate.get(Calendar.DAY_OF_MONTH));
        } else {
            monthSavings += databaseStore.getSavedSalarySum();
            double availableFunds = getAvailableFunds();
            copySalaryToArchive(lastPeriod.getId());
            lastMonthRemainingFunds.setQuota(availableFunds);
            dialogs.showSavingAvailableFundsDialog(availableFunds);
        }
        updateLastPeriod(nextPeriodStartDate, monthSavings, variant);
        databaseStore.updateSalary(lastMonthRemainingFunds);
        updateSavings(monthSavings);
        copySpendingToArchive(lastPeriod.getId());
        copyFixedOutgoingsToArchive(lastPeriod.getId());
        createNotificationsFixedOutgoings();
    }

    private void insertPeriod(Calendar periodStartDate, Calendar periodEndDate) {
        databaseStore.insertPeriod(new Period(null, periodStartDate, periodEndDate, Variants.MONITORING, 0.0));
    }

    private void copySpendingToArchive(int idPeriod) {
        List<CategorizedSpending> currentSpendingList = databaseStore.getCurrentSpendings();
        for (int i = 0; i < currentSpendingList.size(); i++) {
            databaseStore.insertArchiveSpending(new ArchiveSpending(null, idPeriod,
                    currentSpendingList.get(i).getCategoryName(),
                    currentSpendingList.get(i).getSum(),
                    currentSpendingList.get(i).getIncome()));
            if (currentSpendingList.get(i).getSpendingListSize() != 0) {
                deletingSpending(currentSpendingList.get(i));
            }
        }
    }

    private void deletingSpending(CategorizedSpending categorizedSpending) {
        for (int i = 0; i < categorizedSpending.getSpendingListSize(); i++) {
            databaseStore.deleteSpending(categorizedSpending.get(i).getId());
        }
    }

    private void copySalaryToArchive(int idPeriod) {
        TotalSalary salary = databaseStore.getSalaries();
        databaseStore.insertArchiveSalary(new ArchiveSalary(null, idPeriod, salary.getLastMonthRemainingFunds().getName(),
                salary.getLastMonthRemainingFunds().getQuota(), salary.getLastMonthRemainingFunds().getSave()));
        if (salary.getFromSavings().getQuota() > 0.0) {
            Salary salaryFromSavings = salary.getFromSavings();
            databaseStore.insertArchiveSalary(new ArchiveSalary(null, idPeriod, salaryFromSavings.getName(),
                    salaryFromSavings.getQuota(), salaryFromSavings.getSave()));
            databaseStore.updateSalary(new Salary(salaryFromSavings.getId(),
                    salaryFromSavings.getName(), 0.0, salaryFromSavings.getSave()));
        }
        for (int i = 0; i < salary.getSalaryListSize(); i++) {
            databaseStore.insertArchiveSalary(new ArchiveSalary(null, idPeriod, salary.get(i).getName(),
                    salary.get(i).getQuota(), salary.get(i).getSave()));
        }
    }

    private void updateSavings(double savingsSum) {
        preferenceStore.setSavings(savingsSum);
    }

    private void copyFixedOutgoingsToArchive(int idPeriod) {
        Calendar dueDate;
        boolean pay;
        for (FixedOutgoing fixedOutgoing : databaseStore.getFixedOutgoings()) {
            dueDate = fixedOutgoing.getDueDate();
            pay = fixedOutgoing.getIsPaid();
            if (!pay) {
                if (dueDate == null) {
                    continue;
                }
                if (!dueDate.before(today)) {
                    continue;
                }
            }
            databaseStore.insertArchiveFixedOutgoing(idPeriod, fixedOutgoing);
            if (pay) {
                if (dueDate != null) {
                    moveFixedOutgoingsDueDate(fixedOutgoing);
                }
            }
        }
    }

    private void moveFixedOutgoingsDueDate(FixedOutgoing fixedOutgoing) {
        Calendar dueDate = fixedOutgoing.getDueDate();
        dueDate.add(Calendar.MONTH, 1);
        while (dueDate.get(Calendar.YEAR) < nextPeriodStartDate.get(Calendar.YEAR)) {
            dueDate.add(Calendar.MONTH, 1);
        }
        while (dueDate.get(Calendar.MONTH) < nextPeriodStartDate.get(Calendar.MONTH)) {
            dueDate.add(Calendar.MONTH, 1);
        }
        fixedOutgoing.setIsPaid(false);
        fixedOutgoing.setDueDate(dueDate);
        fixedOutgoing.setPaymentDate(null);
        databaseStore.updateFixedOutgoing(fixedOutgoing);

    }

    private void createNotificationsFixedOutgoings() {
        FixedOutgoingsWorkerManager fixedOutgoingsWorkerManager = FixedOutgoingsWorkerManager.getInstance();
        fixedOutgoingsWorkerManager.cancelAll();
        List<FixedOutgoing> fixedOutgoingList = databaseStore.getFixedOutgoings();
        Calendar paymentDate;
        for (int i = 0; i < fixedOutgoingList.size(); i++) {
            paymentDate = fixedOutgoingList.get(i).getDueDate();
            if (paymentDate != null && !fixedOutgoingList.get(i).getIsPaid()) {
                fixedOutgoingsWorkerManager.create(
                        fixedOutgoingList.get(i).getId(),
                        paymentDate, preferenceStore.getRemindingTimeFixedOutgoings(),
                        fixedOutgoingList.get(i).getName(),
                        amountFormatter.format(fixedOutgoingList.get(i).getOutcome()));
            }
        }
    }

    private boolean isAnyPeriodExist() {
        return periodsList.size() != 0;
    }

    private boolean isThatPeriodExist(Calendar periodStartDate) {
        for (Period period : periodsList) {
            if (DateManger.compareDates(period.getStartDate(), periodStartDate)) {
                return true;
            }
        }
        return false;
    }

    private double getAvailableFunds() {
        return databaseStore.getSalarySum() - databaseStore.getSpendingSum() - databaseStore.getPaidFixedOutgoingsSum() - preferenceStore.getMonthlySavings();
    }

    private void updateLastPeriod(Calendar endDate, double savings, Variants variant) {
        endDate.add(Calendar.DAY_OF_MONTH, -1);
        databaseStore.updatePeriod(new Period(lastPeriod.getId(),
                lastPeriod.getStartDate(), endDate,
                variant.toString(), savings));
        endDate.add(Calendar.DAY_OF_MONTH, 1);
    }

}