package com.katarzynakrzemien.finance.managing;

import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.Salary;

public class ManagingPresenter implements ManagingContract.Presenter {

    private ManagingContract.View view;

    @Override
    public void fetchVariant(PreferenceStore preferenceStore) {
        view.setVariant(preferenceStore.getVariant());
    }

    @Override
    public void fetchDayOfNewMonth(PreferenceStore preferenceStore) {
        view.setDayOfNewMonth(preferenceStore.getDayOfNewMonth());
    }

    @Override
    public void fetchAvailableFunds(DatabaseStore databaseStore, PreferenceStore preferenceStore) {
        view.setAvailableFunds(databaseStore.getSalarySum() -
                databaseStore.getSpendingSum() -
                databaseStore.getPaidFixedOutgoingsSum() -
                preferenceStore.getMonthlySavings());
    }

    @Override
    public void fetchTotalSpending(DatabaseStore databaseStore) {
        view.setTotalSpending(databaseStore.getPaidFixedOutgoingsSum() + databaseStore.getSpendingSum());
    }

    @Override
    public void updateSalaryLastMonthRemainingFundsSavedStatus(DatabaseStore databaseStore, boolean savedStatus) {
        Salary salary = databaseStore.getLastMonthRemainingFunds();
        salary.setSave(savedStatus);
        databaseStore.updateSalary(salary);
    }

    @Override
    public void attachView(ManagingContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }
}
