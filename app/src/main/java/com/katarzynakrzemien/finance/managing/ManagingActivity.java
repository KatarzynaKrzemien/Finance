package com.katarzynakrzemien.finance.managing;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.ArraySet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;
import com.katarzynakrzemien.finance.App;
import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.database.DatabaseQuery;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceFromDatabase;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.variants.Variants;
import com.katarzynakrzemien.finance.dialog.DialogLargeAndSmallText;
import com.katarzynakrzemien.finance.dialog.DialogReaction;
import com.katarzynakrzemien.finance.managing.action.ActionAppListener;
import com.katarzynakrzemien.finance.managing.action.Clicked;
import com.katarzynakrzemien.finance.themes.Themes;
import com.katarzynakrzemien.finance.themes.ThemesManager;
import com.katarzynakrzemien.finance.utils.formatter.amount.AmountFormatter;
import com.katarzynakrzemien.finance.utils.formatter.date.DateManger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Set;

import static com.katarzynakrzemien.finance.utils.formatter.date.DateManger.*;
import static com.katarzynakrzemien.finance.utils.formatter.date.DateManger.getDayTitleStringId;

public class ManagingActivity extends AppCompatActivity
        implements ManagingContract.View, OnStartAppShowingDialogs, ActionAppListener {

    public static final String TagKey = "TAG";
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private NavController navController;
    private NavigationView navigationView;
    private AppBarConfiguration config;
    private Calendar calendar = Calendar.getInstance();
    private String spendingLabel = new SimpleDateFormat("EEE  |  dd MMM yyyy", Locale.getDefault()).format(calendar.getTime());
    private DateFormat headerNavigationDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
    private String headerNavigationDate = headerNavigationDateFormat.format(calendar.getTime());
    private ThemesManager themesManager = new ThemesManager();
    private AmountFormatter amountFormatter;
    private ManagingContract.Presenter presenter = new ManagingPresenter();
    private DatabaseStore databaseStore;
    private PreferenceStore preferenceStore;
    private TextView financialCondition, financialConditionTitle, daysToNextMonthCounter, daysTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (ContextCompat.checkSelfPermission(ManagingActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ManagingActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1);
        }
        setActivityTheme();
        super.onCreate(savedInstanceState);
        presenter.attachView(this);
        amountFormatter = ((App) getApplicationContext()).amountFormatter;
        databaseStore = new DatabaseQuery(this);
        preferenceStore = new PreferenceFromDatabase(this);
        setContentView(R.layout.activity_managing);
        new PeriodManager(databaseStore, preferenceStore, this, amountFormatter).manage();
        setupNavigation();
    }

    private void setActivityTheme() {
        int idPref = PreferenceManager.getDefaultSharedPreferences(this).getInt("style", Themes.DARK.getIdPref());
        setTheme(themesManager.getThemes(idPref).getIdTheme());
    }

    private void setupNavigation() {
        navigationView = findViewById(R.id.navigationView);
        drawerLayout = findViewById(R.id.drawer_layout);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        config = new AppBarConfiguration.Builder(getSet()).setDrawerLayout(drawerLayout).build();
        setupSpendingFragmentLabel();
        NavigationUI.setupWithNavController(navigationView, navController);
        NavigationUI.setupActionBarWithNavController(this, navController, config);
        setupHeaderNavigationDrawerElements();
    }

    private Set<Integer> getSet() {
        Set<Integer> set = new ArraySet<>();
        set.add(R.id.spending_fragment);
        set.add(R.id.fixed_outgoings_fragment);
        set.add(R.id.archive_fragment);
        set.add(R.id.charts_fragment);
        set.add(R.id.current_summary_fragment);
        set.add(R.id.settings_fragment);
        set.add(R.id.help_fragment);
        return set;
    }

    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(navController, config);
    }

    private void setupSpendingFragmentLabel() {
        navController.getGraph().findNode(R.id.spending_fragment).setLabel(spendingLabel);
    }

    public void setupHeaderNavigationDrawerElements() {
        View headerNavigationView = navigationView.getHeaderView(0);
        financialCondition = headerNavigationView.findViewById(R.id.financial_condition);
        financialConditionTitle = headerNavigationView.findViewById(R.id.financial_condition_title);
        daysToNextMonthCounter = headerNavigationView.findViewById(R.id.days_to_next_month_counter);
        daysTitle = headerNavigationView.findViewById(R.id.days_title);
        ((TextView) headerNavigationView.findViewById(R.id.current_date)).setText(headerNavigationDate);

        presenter.fetchVariant(preferenceStore);
        presenter.fetchDayOfNewMonth(preferenceStore);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.list_add:
                listAddButtonSelected();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void listAddButtonSelected() {
        if (navigationView.getCheckedItem().getItemId() == navController.getGraph().findNode(R.id.spending_fragment).getId()) {
            itemClicked(Clicked.CATEGORIES);
        } else if (navigationView.getCheckedItem().getItemId() == navController.getGraph().findNode(R.id.fixed_outgoings_fragment).getId()) {
            itemClicked(Clicked.FIXED_OUTGOINGS);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void setVariant(Variants variant) {
        variantChange(variant);
    }

    private void setupNavigationDrawerElementsDependentMonitoringVariant() {
        navigationView.getMenu().findItem(R.id.spending_fragment).setIcon(R.drawable.ic_monitoring);
        presenter.fetchTotalSpending(databaseStore);
        financialConditionTitle.setText(R.string.totalSpending);
    }

    private void setupNavigationDrawerElementsDependentPlanningVariant() {
        navigationView.getMenu().findItem(R.id.spending_fragment).setIcon(R.drawable.ic_planning);
        presenter.fetchAvailableFunds(databaseStore, preferenceStore);
        financialConditionTitle.setText(R.string.available_funds);
    }

    @Override
    public void setDayOfNewMonth(int dayOfNewMonth) {
        long days = getRemainingDaysToNextMonth(dayOfNewMonth);
        daysToNextMonthCounter.setText(String.valueOf(days));
        daysTitle.setText(getString(getDayTitleStringId(days)));
    }

    @Override
    public void setAvailableFunds(double availableFunds) {
        financialCondition.setText(amountFormatter.format(availableFunds));
    }

    @Override
    public void setTotalSpending(double totalSpending) {
        financialCondition.setText(amountFormatter.format(totalSpending));
    }

    @Override
    protected void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }

    @Override
    public void showSavingAvailableFundsDialog(double availableFunds) {
        DialogLargeAndSmallText dialogCreator = new DialogLargeAndSmallText(this, getString(R.string.newMonth), R.drawable.ic_help);
        dialogCreator.setLargeTextView(amountFormatter.format(availableFunds));
        dialogCreator.setSmallTextView(getString(R.string.whatDoWithRemainingMoney));
        dialogCreator.setPositiveButton(R.string.addToSavings, new DialogReaction() {
            @Override
            public void onClick() {
                presenter.updateSalaryLastMonthRemainingFundsSavedStatus(databaseStore, true);
                setupHeaderNavigationDrawerElements();
            }
        });
        dialogCreator.setNeutralButton(R.string.addToSalary, new DialogReaction() {
            @Override
            public void onClick() {
                presenter.updateSalaryLastMonthRemainingFundsSavedStatus(databaseStore, false);
                setupHeaderNavigationDrawerElements();
            }
        });
        dialogCreator.showDialog();
    }

    @Override
    public void showNewPeriodCreatedDialog(int todayDay) {
        long days = getRemainingDaysToNextMonth(todayDay);
        final DialogLargeAndSmallText dialogCreator = new DialogLargeAndSmallText(this, getString(R.string.newMonth), R.drawable.ic_caution);
        dialogCreator.setLargeTextView(days + "");
        dialogCreator.setSmallTextView(getString(getDayTitleStringId(days)) + " " + getString(R.string.leftToNextMonth));
        dialogCreator.setExitButton();
        dialogCreator.showDialog();
    }

    @Override
    public void fundsChange() {
        presenter.fetchAvailableFunds(databaseStore, preferenceStore);
    }

    @Override
    public void variantChange(Variants variant) {
        if (variant.equals(Variants.MONITORING)) {
            setupNavigationDrawerElementsDependentMonitoringVariant();
        } else {
            setupNavigationDrawerElementsDependentPlanningVariant();
        }
    }

    @Override
    public void dayOfNewMonthChange() {
        presenter.fetchDayOfNewMonth(preferenceStore);
    }

    @Override
    public void itemClicked(Clicked itemClicked) {
        navController.navigate(itemClicked.getNavigateId());
    }
}


