package com.katarzynakrzemien.finance.managing.action;

import com.katarzynakrzemien.finance.R;

public enum Clicked {

    SALARY(R.id.action_settings_fragment_to_salary_fragment),
    SAVINGS(R.id.action_settings_fragment_to_savings_fragment),
    SPENDING_PLANNING(R.id.action_settings_fragment_to_spending_planning_fragment),
    FIXED_OUTGOINGS(R.id.action_fixed_outgoings_fragment_to_categorized_fixed_outgoings_fragment),
    CATEGORIES(R.id.action_spending_fragment_to_categories_fragment);

    private int navigateId;

    Clicked(int navigateId) {
        this.navigateId = navigateId;
    }

    public int getNavigateId() {
        return navigateId;
    }
}
