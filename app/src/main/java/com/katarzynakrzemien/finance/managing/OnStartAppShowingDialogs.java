package com.katarzynakrzemien.finance.managing;

public interface OnStartAppShowingDialogs {
    void showSavingAvailableFundsDialog(double availableFunds);

    void showNewPeriodCreatedDialog(int todayDay);
}
