package com.katarzynakrzemien.finance.spending.monitoring;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.spending.BaseSpendingFragment;

public class SpendingFragment extends BaseSpendingFragment<SpendingAdapter> {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_monitoring, container, false);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        adapter = new SpendingAdapter(categorizedSpendingList, context,this);
    }
}