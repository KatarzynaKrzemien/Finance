package com.katarzynakrzemien.finance.spending;

import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.CategorizedSpending;
import com.katarzynakrzemien.finance.database.models.Spending;

import java.util.List;

public class SpendingPresenter implements SpendingContract.Presenter {
    private SpendingContract.View view;

    @Override
    public void attachView(SpendingContract.View view) {
        this.view = view;
    }

    @Override
    public void fetchList(DatabaseStore databaseStore) {
        view.renderList(databaseStore.getCurrentSpendings());
    }


    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void fetchTotalSpending(DatabaseStore databaseStore) {
        view.renderTotalSpending(databaseStore.getSpendingSum());
    }

    @Override
    public void fetchAvailableFunds(DatabaseStore databaseStore, PreferenceStore preferenceStore) {
        view.renderAvailableFunds(databaseStore.getSalarySum() - databaseStore.getPaidFixedOutgoingsSum() - databaseStore.getSpendingSum() - preferenceStore.getMonthlySavings(), databaseStore.getSalarySum());
    }

    @Override
    public void addListItem(DatabaseStore databaseStore, CategorizedSpending model) {

        databaseStore.insertSpending(model.getCategoryName(), new Spending(null, model.get(0).getDate(), model.get(0).getQuota()));
        fetchList(databaseStore);
    }

    @Override
    public void deleteListItem(DatabaseStore databaseStore, int id) {
        databaseStore.deleteSpending(id);

    }

    @Override
    public void isAnyActiveCategory(DatabaseStore databaseStore) {
        List<CategorizedSpending> categorizedSpendingList = databaseStore.getCurrentSpendings();
        view.setIsAnyActiveCategory(!categorizedSpendingList.isEmpty());
    }
}
