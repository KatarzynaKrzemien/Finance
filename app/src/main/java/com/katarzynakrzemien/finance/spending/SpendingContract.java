package com.katarzynakrzemien.finance.spending;

import com.katarzynakrzemien.finance.BaseRecyclerContract;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.CategorizedSpending;

public class SpendingContract {

    public interface View extends BaseRecyclerContract.View<CategorizedSpending> {

        void renderTotalSpending(double totalSpending);

        void renderAvailableFunds(double availableFunds, double income);

        void setIsAnyActiveCategory(boolean isAnyActiveCategory);

    }

    public interface Presenter extends BaseRecyclerContract.Presenter<View> {

        void fetchTotalSpending(DatabaseStore databaseStore);

        void fetchAvailableFunds(DatabaseStore databaseStore, PreferenceStore preferenceStore);

        void addListItem(DatabaseStore databaseStore, CategorizedSpending model);

        void deleteListItem(DatabaseStore databaseStore, int id);

        void isAnyActiveCategory(DatabaseStore databaseStore);

    }
}
