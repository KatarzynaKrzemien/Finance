package com.katarzynakrzemien.finance.spending.planning;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.util.TypedValue;

import com.katarzynakrzemien.finance.R;

public class AvailableFundsColorEvaluator {
    private int warningColorId, minusColorId;
    private Resources.Theme theme;

    public AvailableFundsColorEvaluator(Context context) {
        warningColorId = context.getResources().getColor(R.color.warning);
        minusColorId = context.getResources().getColor(R.color.minus);
        theme = context.getTheme();
    }

    public int getTextColor(double availableFunds, double income) {
        double percent = (availableFunds * 100) / income;
        if (percent <= 20.0 && percent > 0.0) {
            return warningColorId;
        } else if (availableFunds < 0.0) {
            return minusColorId;
        }
        TypedValue typedValue = new TypedValue();
        theme.resolveAttribute(android.R.attr.textColorPrimary, typedValue, true);
        return typedValue.data;
    }


    public int getBackgroundColor(double availableFunds) {
        if (availableFunds < 0.0) {
            return minusColorId;
        }
        return Color.TRANSPARENT;
    }
}
