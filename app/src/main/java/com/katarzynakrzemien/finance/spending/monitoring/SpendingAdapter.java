package com.katarzynakrzemien.finance.spending.monitoring;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.katarzynakrzemien.finance.App;
import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.database.models.CategorizedSpending;
import com.katarzynakrzemien.finance.spending.BaseSpendingAdapter;
import com.katarzynakrzemien.finance.utils.formatter.amount.AmountFormatter;
import com.katarzynakrzemien.finance.utils.formatter.date.DateFormatPattern;
import com.katarzynakrzemien.finance.utils.formatter.date.DateFormatter;
import com.katarzynakrzemien.finance.utils.formatter.date.SimpleDateFormatter;
import com.katarzynakrzemien.finance.utils.recycler.ItemClickListener;

import java.util.List;

public class SpendingAdapter extends BaseSpendingAdapter {

    protected AmountFormatter amountFormatter;
    private List<CategorizedSpending> spendingList;
    private LayoutInflater layoutInflater;
    private ItemClickListener itemClickListener;
    private DateFormatter dateFormatter = new SimpleDateFormatter(DateFormatPattern.DAY_MONTH_YEAR);

    public SpendingAdapter(List<CategorizedSpending> spendingList, Context context, ItemClickListener itemClickListener) {
        this.spendingList = spendingList;
        this.layoutInflater = LayoutInflater.from(context);
        this.amountFormatter = ((App) context.getApplicationContext()).amountFormatter;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onBindViewHolder(@NonNull SpendingViewHolder holder, int position) {
        CategorizedSpending categorizedSpending = spendingList.get(position);
        setupExpandableRowViews(holder, categorizedSpending);
        createExpandingRows(holder, categorizedSpending);
    }

    protected void setupExpandableRowViews(SpendingViewHolder holder, CategorizedSpending categorizedSpending) {
        holder.name.setText(categorizedSpending.getCategoryName());
        holder.outcome.setText(amountFormatter.format(categorizedSpending.getSum()));
        holder.rowLayout.setOnLongClickListener(getOpenCloseExpandingRowsClick(holder));
    }

    private void createExpandingRows(SpendingViewHolder holder, final CategorizedSpending categorizedSpending) {
        holder.expandableLayout.removeAllViews();
        for (int i = 0; i < categorizedSpending.getSpendingListSize(); i++) {
            View childView = layoutInflater.inflate(R.layout.expendable_main, (ViewGroup) holder.itemView, false);
            ((TextView) childView.findViewById(R.id.date_row)).setText(dateFormatter.formatToString(categorizedSpending.get(i).getDate()));
            ((TextView) childView.findViewById(R.id.quota_row)).setText(amountFormatter.format(categorizedSpending.get(i).getQuota()));
            holder.expandableLayout.addView(childView);
        }
    }

    private View.OnLongClickListener getOpenCloseExpandingRowsClick(final SpendingViewHolder holder) {
        return new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (holder.expandableLayout.getVisibility() == View.GONE) {
                    holder.expandableLayout.setVisibility(View.VISIBLE);
                } else {
                    holder.expandableLayout.setVisibility(View.GONE);
                }
                return true;
            }
        };
    }

    @Override
    public int getItemCount() {
        return spendingList.size();
    }

    @Override
    protected int getAvailableFundsVisibility() {
        return View.GONE;
    }

    @Override
    protected int getExpandingLayoutVisibility() {
        return View.VISIBLE;
    }

    @Override
    protected View.OnClickListener getOnItemClick(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClick(v, position);
            }
        };
    }


}