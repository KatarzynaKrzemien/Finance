package com.katarzynakrzemien.finance.spending;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.katarzynakrzemien.finance.R;

public abstract class BaseSpendingAdapter extends RecyclerView.Adapter<BaseSpendingAdapter.SpendingViewHolder> {


    @NonNull
    @Override
    public SpendingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_spending, parent, false);
        return new SpendingViewHolder(itemView);
    }

    protected abstract int getAvailableFundsVisibility();

    protected abstract int getExpandingLayoutVisibility();

    protected abstract View.OnClickListener getOnItemClick(int position);

    public class SpendingViewHolder extends RecyclerView.ViewHolder {

        public TextView name, outcome, availableFunds;
        public LinearLayout rowLayout, expandableLayout;

        public SpendingViewHolder(View view) {
            super(view);
            rowLayout = view.findViewById(R.id.row_header_layout);
            expandableLayout = view.findViewById(R.id.expandable_layout);
            expandableLayout.setVisibility(getExpandingLayoutVisibility());
            name = view.findViewById(R.id.name_row);
            outcome = view.findViewById(R.id.spending_row);
            availableFunds = view.findViewById(R.id.remaining_row);
            availableFunds.setVisibility(getAvailableFundsVisibility());
            rowLayout.setOnClickListener(getOnItemClick(getAdapterPosition()));
        }

    }
}
