package com.katarzynakrzemien.finance.spending.planning.fullplanning;

import com.katarzynakrzemien.finance.spending.planning.PlannedSpendingFragment;

import java.util.Objects;

public class FullPlanningSpendingFragment extends PlannedSpendingFragment {

    @Override
    public void onStart() {
        adapter = new FullPlannedSpendingAdapter(categorizedSpendingList,
                Objects.requireNonNull(getContext()),
                this, availableFundsColorEvaluator);
        super.onStart();
    }
}
