package com.katarzynakrzemien.finance.spending.planning;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.dialog.DialogQuotaCreator;
import com.katarzynakrzemien.finance.spending.BaseSpendingFragment;
import com.katarzynakrzemien.finance.spending.planning.fullplanning.FullPlannedSpendingAdapter;

public class PlannedSpendingFragment extends BaseSpendingFragment<FullPlannedSpendingAdapter> {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_planning, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.fetchAvailableFunds(databaseStore, preferenceStore);
    }

    @Override
    protected void positiveButtonClick(DialogQuotaCreator dialogQuotaCreator, String categoryName) {
        super.positiveButtonClick(dialogQuotaCreator, categoryName);
        presenter.fetchAvailableFunds(databaseStore, preferenceStore);
    }
}