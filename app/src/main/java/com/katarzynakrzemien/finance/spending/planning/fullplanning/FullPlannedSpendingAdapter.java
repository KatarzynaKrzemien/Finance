package com.katarzynakrzemien.finance.spending.planning.fullplanning;

import android.content.Context;
import android.view.View;

import com.katarzynakrzemien.finance.database.models.CategorizedSpending;
import com.katarzynakrzemien.finance.spending.monitoring.SpendingAdapter;
import com.katarzynakrzemien.finance.spending.planning.AvailableFundsColorEvaluator;
import com.katarzynakrzemien.finance.utils.recycler.ItemClickListener;

import java.util.List;

public class FullPlannedSpendingAdapter extends SpendingAdapter {

    private AvailableFundsColorEvaluator availableFundsColorEvaluator;

    FullPlannedSpendingAdapter(List<CategorizedSpending> spendingList, Context context, ItemClickListener itemClickListener, AvailableFundsColorEvaluator availableFundsColorEvaluator) {
        super(spendingList, context, itemClickListener);
        this.availableFundsColorEvaluator = availableFundsColorEvaluator;
    }

    @Override
    protected void setupExpandableRowViews(SpendingViewHolder holder, CategorizedSpending categorizedSpending) {
        super.setupExpandableRowViews(holder, categorizedSpending);
        setupAvailableFundsView(holder, categorizedSpending);
    }

    private void setupAvailableFundsView(SpendingViewHolder holder, CategorizedSpending categorizedSpending) {
        double availableFunds = categorizedSpending.getIncome() - categorizedSpending.getSum();
        holder.availableFunds.setText(amountFormatter.format(availableFunds));
        holder.itemView.setBackgroundColor(availableFundsColorEvaluator.getBackgroundColor(availableFunds));
        holder.availableFunds.setTextColor(availableFundsColorEvaluator.getTextColor(availableFunds, categorizedSpending.getIncome()));
    }

    @Override
    protected int getAvailableFundsVisibility() {
        return View.VISIBLE;
    }
}
