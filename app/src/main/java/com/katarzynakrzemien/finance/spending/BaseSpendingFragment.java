package com.katarzynakrzemien.finance.spending;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.katarzynakrzemien.finance.App;
import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.database.DatabaseQuery;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceFromDatabase;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.CategorizedSpending;
import com.katarzynakrzemien.finance.database.models.Spending;
import com.katarzynakrzemien.finance.dialog.DialogQuotaCreator;
import com.katarzynakrzemien.finance.dialog.DialogReaction;
import com.katarzynakrzemien.finance.managing.ManagingActivity;
import com.katarzynakrzemien.finance.managing.action.ActionAppListener;
import com.katarzynakrzemien.finance.managing.action.Clicked;
import com.katarzynakrzemien.finance.spending.monitoring.SpendingAdapter;
import com.katarzynakrzemien.finance.spending.planning.AvailableFundsColorEvaluator;
import com.katarzynakrzemien.finance.utils.Keyboard;
import com.katarzynakrzemien.finance.utils.formatter.amount.AmountFormatter;
import com.katarzynakrzemien.finance.utils.recycler.ItemClickListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public abstract class BaseSpendingFragment<T extends SpendingAdapter> extends Fragment implements SpendingContract.View, ItemClickListener {

    public static final String TAG = "MAIN";

    protected View view = null;
    protected List<CategorizedSpending> categorizedSpendingList = new ArrayList<>();
    protected T adapter;
    protected SpendingContract.Presenter presenter = new SpendingPresenter();
    protected DatabaseStore databaseStore;
    protected PreferenceStore preferenceStore;
    protected AmountFormatter amountFormatter;
    protected AvailableFundsColorEvaluator availableFundsColorEvaluator;
    private ActionAppListener actionAppListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        amountFormatter = ((App) context.getApplicationContext()).amountFormatter;
        databaseStore = new DatabaseQuery(context);
        preferenceStore = new PreferenceFromDatabase(context);
        presenter.attachView(this);
        actionAppListener = ((ManagingActivity) context);
        availableFundsColorEvaluator = new AvailableFundsColorEvaluator(context);
    }

    @Override
    public void onStart() {
        super.onStart();
        view = getView();
        if (view != null) {
            openCategorizedSpendingFragmentWhenNoActiveCategory();
            RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
            recyclerView.setAdapter(adapter);
            presenter.fetchList(databaseStore);
            presenter.fetchTotalSpending(databaseStore);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.animate();
        }
    }

    public void renderList(List<CategorizedSpending> list) {
        categorizedSpendingList.clear();
        categorizedSpendingList.addAll(list);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void renderTotalSpending(double totalSpending) {
        if (view != null) {
            ((TextView) view.findViewById(R.id.spending_sum)).setText(amountFormatter.format(totalSpending));
        }
    }

    @Override
    public void renderAvailableFunds(double availableFunds, double income) {
        if (view != null) {
            TextView availableFundsView = view.findViewById(R.id.available_funds);
            availableFundsView.setText(amountFormatter.format(availableFunds));
            availableFundsView.setBackgroundColor(availableFundsColorEvaluator.getBackgroundColor(availableFunds));
            availableFundsView.setTextColor(availableFundsColorEvaluator.getTextColor(availableFunds, income));
        }
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }

    @Override
    public void onItemClick(View view, int position) {
        final String categoryName = categorizedSpendingList.get(position).getCategoryName();
        final DialogQuotaCreator dialogQuotaCreator = new DialogQuotaCreator(getContext(), categoryName);
        dialogQuotaCreator.setPositiveButton(R.string.add, new DialogReaction() {
            @Override
            public void onClick() {
                positiveButtonClick(dialogQuotaCreator, categoryName);
                Keyboard.hide(getContext());
            }
        });
        dialogQuotaCreator.showDialog();
        Keyboard.show(getContext());
    }

    protected void positiveButtonClick(DialogQuotaCreator dialogQuotaCreator, String categoryName) {
        String spendingText = dialogQuotaCreator.getDialogEditText().getText().toString();
        if (spendingText.length() > 0) {
            double spending = Double.parseDouble(spendingText);
            List<Spending> spendingList = new ArrayList<>();
            spendingList.add(new Spending(null,
                    Calendar.getInstance(), spending));
            presenter.addListItem(databaseStore,
                    new CategorizedSpending(categoryName, 0.0, spendingList));
            presenter.fetchList(databaseStore);
            presenter.fetchTotalSpending(databaseStore);
            fundsChanged();
        }
    }

    @Override
    public void onItemLongClick(View view, int position) {
    }

    @Override
    public void setIsAnyActiveCategory(boolean isAnyActiveCategory) {
        if (!isAnyActiveCategory) {
            Snackbar.make(view, getString(R.string.pickCategory), Snackbar.LENGTH_LONG).show();
            actionAppListener.itemClicked(Clicked.CATEGORIES);
        }
    }

    private void openCategorizedSpendingFragmentWhenNoActiveCategory() {
        presenter.isAnyActiveCategory(databaseStore);
    }

    private void fundsChanged() {
        actionAppListener.fundsChange();
    }
}