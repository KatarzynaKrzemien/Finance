package com.katarzynakrzemien.finance;

import android.app.Application;

import com.katarzynakrzemien.finance.database.DatabaseQuery;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.utils.formatter.amount.DecimalAmountFormatter;
import com.katarzynakrzemien.finance.utils.formatter.amount.AmountFormatter;

public class App extends Application {
    public DatabaseStore databaseStore;
    public AmountFormatter amountFormatter = new DecimalAmountFormatter();

    @Override
    public void onCreate() {
        super.onCreate();
        databaseStore = new DatabaseQuery(this);
    }

}
