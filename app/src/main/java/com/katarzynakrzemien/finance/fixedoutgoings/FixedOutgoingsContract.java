package com.katarzynakrzemien.finance.fixedoutgoings;

import com.katarzynakrzemien.finance.BaseRecyclerContract;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.CategorizedFixedOutgoing;
import com.katarzynakrzemien.finance.database.models.FixedOutgoing;

import java.util.Calendar;

public class FixedOutgoingsContract {

    public interface View extends BaseRecyclerContract.View<CategorizedFixedOutgoing> {

        void setRemindFixedOutgoings(boolean remindFixedOutgoings);

        void setTimeRemindFixedOutgoings(Calendar timeRemindFixedOutgoings);

        void setIsAnyActiveFixedOutgoing(boolean isAnyActiveFixedOutgoing);

        void renderPaidSum(double paidSum);

        void renderUnpaidSum(double unpaidSum);


    }

    public interface Presenter extends BaseRecyclerContract.Presenter<View> {
        void fetchPaidSum(DatabaseStore databaseStore);

        void fetchUnpaidSum(DatabaseStore databaseStore);

        void updateItemList(DatabaseStore databaseStore, FixedOutgoing fixedOutgoing);

        void fetchRemindFixedOutgoings(PreferenceStore preferenceStore);

        void fetchTimeRemindFixedOutgoings(PreferenceStore preferenceStore);

        void isAnyActiveFixedOutgoing(DatabaseStore databaseStore);
    }
}
