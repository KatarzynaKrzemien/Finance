package com.katarzynakrzemien.finance.fixedoutgoings;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.CategorizedFixedOutgoing;
import com.katarzynakrzemien.finance.database.models.FixedOutgoing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FixedOutgoingsPresenter implements FixedOutgoingsContract.Presenter {

    private FixedOutgoingsContract.View view;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void fetchList(DatabaseStore databaseStore) {
        view.renderList(createSortedCategorizedFixedOutgoingList(groupFixedOutgoingsByName(databaseStore.getFixedOutgoings())));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private Map<String, List<FixedOutgoing>> groupFixedOutgoingsByName(List<FixedOutgoing> dbFixedOutgoingList) {
        Map<String, List<FixedOutgoing>> fixedOutgoingsListMap = new HashMap<>();
        for (FixedOutgoing item : dbFixedOutgoingList) {
            String key = item.getName();
            if (fixedOutgoingsListMap.containsKey(key)) {
                List<FixedOutgoing> list = fixedOutgoingsListMap.get(key);
                assert list != null;
                list.add(item);
                fixedOutgoingsListMap.replace(key, list);
            } else {
                List<FixedOutgoing> list = new ArrayList<>();
                list.add(item);
                fixedOutgoingsListMap.put(key, list);
            }
        }
        return fixedOutgoingsListMap;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private List<CategorizedFixedOutgoing> createSortedCategorizedFixedOutgoingList(Map<String, List<FixedOutgoing>> fixedOutgoingsListMap) {
        List<CategorizedFixedOutgoing> categorizedFixedOutgoingList = new ArrayList<>();
        for (Map.Entry<String, List<FixedOutgoing>> entry : fixedOutgoingsListMap.entrySet()) {
            List<FixedOutgoing> list = entry.getValue();
            list.sort(new FixedOutgoing.DueDateCompare());
            list.sort(new FixedOutgoing.PaidStatusCompare());
            categorizedFixedOutgoingList.add(new CategorizedFixedOutgoing(entry.getKey(), entry.getValue()));
        }
        categorizedFixedOutgoingList.sort(new CategorizedFixedOutgoing.DueDateCompare());
        categorizedFixedOutgoingList.sort(new CategorizedFixedOutgoing.PaidStatusCompare());
        return categorizedFixedOutgoingList;
    }

    @Override
    public void attachView(FixedOutgoingsContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void fetchPaidSum(DatabaseStore databaseStore) {
        view.renderPaidSum(databaseStore.getPaidFixedOutgoingsSum());
    }

    @Override
    public void fetchUnpaidSum(DatabaseStore databaseStore) {
        view.renderUnpaidSum(databaseStore.getUnpaidFixedOutgoingsSum());
    }

    @Override
    public void updateItemList(DatabaseStore databaseStore, FixedOutgoing fixedOutgoing) {
        databaseStore.updateFixedOutgoing(fixedOutgoing);
    }

    @Override
    public void fetchRemindFixedOutgoings(PreferenceStore preferenceStore) {
        view.setRemindFixedOutgoings(preferenceStore.getRemindingFixedOutgoings());
    }

    @Override
    public void fetchTimeRemindFixedOutgoings(PreferenceStore preferenceStore) {
        view.setTimeRemindFixedOutgoings(preferenceStore.getRemindingTimeFixedOutgoings());
    }

    @Override
    public void isAnyActiveFixedOutgoing(DatabaseStore databaseStore) {
        view.setIsAnyActiveFixedOutgoing(!databaseStore.getFixedOutgoings().isEmpty());
    }
}