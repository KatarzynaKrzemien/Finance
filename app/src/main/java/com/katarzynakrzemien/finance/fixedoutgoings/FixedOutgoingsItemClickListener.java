package com.katarzynakrzemien.finance.fixedoutgoings;

import android.view.View;

import com.katarzynakrzemien.finance.database.models.FixedOutgoing;

public interface FixedOutgoingsItemClickListener {

    void onOutcomeClick(View view, FixedOutgoing fixedOutgoing);

    void onDueDateClick(View view, FixedOutgoing fixedOutgoing);

    void onPaymentDateClick(View view, FixedOutgoing fixedOutgoing);

    void onPayClick(View view, FixedOutgoing fixedOutgoing);
}
