package com.katarzynakrzemien.finance.fixedoutgoings.reminder;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.fixedoutgoings.FixedOutgoingsFragment;
import com.katarzynakrzemien.finance.managing.ManagingActivity;
import com.katarzynakrzemien.finance.utils.formatter.date.SimpleDateFormatter;
import com.katarzynakrzemien.finance.utils.formatter.date.DateFormatPattern;
import com.katarzynakrzemien.finance.utils.formatter.date.DateFormatter;
import com.katarzynakrzemien.finance.utils.formatter.date.DateManger;

import java.util.Calendar;

public class FixedOutgoingsWorker extends Worker {

    private String quota, date, title;
    private int id;
    private Context context;
    private DateFormatter dateFormatter = new SimpleDateFormatter(DateFormatPattern.DAY_MONTH_YEAR);

    public FixedOutgoingsWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        Data data = workerParams.getInputData();
        this.id = data.getInt(FixedOutgoingsWorkerManager.ID, 0);
        this.title = data.getString(FixedOutgoingsWorkerManager.TITLE);
        this.date = data.getString(FixedOutgoingsWorkerManager.DATE);
        this.quota = data.getString(FixedOutgoingsWorkerManager.QUOTA);
        this.context = context;
    }

    @NonNull
    @Override
    public Result doWork() {
        createNotification(id, title, quota, date);
        return Result.success();
    }

    private void createNotification(int id, String title, String quota, String date) {
        StringBuilder message = new StringBuilder();
        message.append(createMessage(date));
        Intent intent = new Intent(getApplicationContext(), ManagingActivity.class);
        intent.putExtra(ManagingActivity.TagKey, FixedOutgoingsFragment.TAG);
        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
        taskStackBuilder.addParentStack(ManagingActivity.class);
        taskStackBuilder.addNextIntent(intent);
        PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(id, PendingIntent.FLAG_UPDATE_CURRENT);
        Intent intentAction = new Intent(context, FixedOutgoingsReminderReceiver.class);
        intentAction.putExtra(FixedOutgoingsWorkerManager.ID, id);
        PendingIntent pendingIntentAction = PendingIntent.getBroadcast(context, id, intentAction, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Action action = new Notification.Action(R.drawable.ic_done, context.getString(R.string.paid), pendingIntentAction);
        Notification notification = new Notification.Builder(getApplicationContext())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .setContentTitle(context.getString(R.string.app_name))
                .setPriority(Notification.PRIORITY_MAX)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentIntent(pendingIntent)
                .addAction(action)
                .setStyle(new Notification.BigTextStyle().bigText(message.append("\n").append(title).append(" : ").append(quota).toString()))
                .setContentText(message.append(". ").append(title).append(" : ").append(quota).toString())
                .build();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context
                .NOTIFICATION_SERVICE);
        notificationManager.notify(id, notification);
    }

    private String createMessage(String date) {
        if (DateManger.compareDates(Calendar.getInstance(), dateFormatter.parseToCalendar(date))) {
            return context.getString(R.string.payTheBill);
        } else {
            return context.getString(R.string.unpaidBillDated) + " " + date;
        }
    }
}
