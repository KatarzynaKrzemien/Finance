package com.katarzynakrzemien.finance.fixedoutgoings;

import android.content.Context;
import android.view.View;

import com.katarzynakrzemien.finance.database.models.CategorizedFixedOutgoing;
import com.katarzynakrzemien.finance.database.models.FixedOutgoing;

import java.util.List;

public class FixedOutgoingsAdapter extends BaseFixedOutgoingsAdapter<CategorizedFixedOutgoing> {

    private FixedOutgoingsItemClickListener itemClickListener;

    FixedOutgoingsAdapter(List<CategorizedFixedOutgoing> fixedOutgoingsList, Context context, FixedOutgoingsItemClickListener itemClickListener) {
        super(fixedOutgoingsList, context);
        this.itemClickListener = itemClickListener;
    }

    @Override
    protected View.OnClickListener getDueDateClick(final FixedOutgoing fixedOutgoing) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onDueDateClick(v, fixedOutgoing);
            }
        };
    }

    @Override
    protected View.OnClickListener getPaymentDateClick(final FixedOutgoing fixedOutgoing) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onPaymentDateClick(v, fixedOutgoing);
            }
        };
    }

    @Override
    protected View.OnClickListener getPaidClick(final FixedOutgoing fixedOutgoing) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onPayClick(v, fixedOutgoing);
            }
        };
    }

    @Override
    protected View.OnClickListener getOutcomeClick(final FixedOutgoing fixedOutgoing) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onOutcomeClick(v, fixedOutgoing);
            }
        };
    }
}