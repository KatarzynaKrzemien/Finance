package com.katarzynakrzemien.finance.fixedoutgoings.reminder;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.katarzynakrzemien.finance.App;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.models.FixedOutgoing;

public class FixedOutgoingsReminderReceiver extends BroadcastReceiver {

    private DatabaseStore databaseStore;
    private int idFixedOutgoing;
    private NotificationManager notificationManager;

    @Override
    public void onReceive(Context context, Intent intent) {

        idFixedOutgoing = intent.getIntExtra(FixedOutgoingsWorkerManager.ID, 0);
        databaseStore = ((App) context.getApplicationContext()).databaseStore;
        notificationManager = (NotificationManager) context.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        paidFixedOutgoing();
        cancelNotificationsFixedOutgoing();
    }

    private void paidFixedOutgoing() {
        FixedOutgoing fixedOutgoing = databaseStore.getFixedOutgoing(idFixedOutgoing);
        fixedOutgoing.setIsPaid(true);
        databaseStore.updateFixedOutgoing(fixedOutgoing);
    }

    private void cancelNotificationsFixedOutgoing() {
        notificationManager.cancel(idFixedOutgoing);

    }
}
