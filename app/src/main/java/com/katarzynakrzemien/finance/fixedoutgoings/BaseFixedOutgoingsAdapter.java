package com.katarzynakrzemien.finance.fixedoutgoings;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;
import androidx.recyclerview.widget.RecyclerView;

import com.katarzynakrzemien.finance.App;
import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.database.models.CategorizedFixedOutgoing;
import com.katarzynakrzemien.finance.database.models.FixedOutgoing;
import com.katarzynakrzemien.finance.utils.formatter.amount.AmountFormatter;
import com.katarzynakrzemien.finance.utils.formatter.date.DateFormatPattern;
import com.katarzynakrzemien.finance.utils.formatter.date.DateFormatter;
import com.katarzynakrzemien.finance.utils.formatter.date.SimpleDateFormatter;

import java.util.Calendar;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public abstract class BaseFixedOutgoingsAdapter<T extends CategorizedFixedOutgoing> extends RecyclerView.Adapter<BaseFixedOutgoingsAdapter<T>.FixedOutgoingsViewHolder> {

    private List<T> fixedOutgoingsList;
    private AmountFormatter amountFormatter;
    private Resources.Theme theme;
    private String noDate;
    private String paid;
    private String unPaid;
    private int activeColorId, noActiveColorId, minusColorId;
    private LayoutInflater layoutInflater;
    private DateFormatter dateFormatter = new SimpleDateFormatter(DateFormatPattern.DAY_MONTH_YEAR);

    public BaseFixedOutgoingsAdapter(List<T> fixedOutgoingsList, Context context) {
        this.fixedOutgoingsList = fixedOutgoingsList;
        this.amountFormatter = ((App) context.getApplicationContext()).amountFormatter;
        this.theme = context.getTheme();
        this.layoutInflater = LayoutInflater.from(context);
        this.noDate = context.getString(R.string.noDate);
        this.unPaid = context.getString(R.string.unpaid);
        this.paid = context.getString(R.string.paid);
        this.activeColorId = context.getResources().getColor(R.color.active);
        this.noActiveColorId = context.getResources().getColor(R.color.noactive);
        this.minusColorId = context.getResources().getColor(R.color.minus);
    }

    @NonNull
    @Override
    public FixedOutgoingsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_fixed_outgoings, parent, false);
        return new FixedOutgoingsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final FixedOutgoingsViewHolder holder, final int position) {
        final CategorizedFixedOutgoing categorizedFixedOutgoing = fixedOutgoingsList.get(position);
        holder.name.setText(categorizedFixedOutgoing.getName());
        if (categorizedFixedOutgoing.getFixedOutgoingsListSize() == 1) {
            setupNoExpandableRowViews(holder, categorizedFixedOutgoing.get(0));
        } else {
            setupExpandableRowViews(holder, categorizedFixedOutgoing);
            createExpandingRows(holder, categorizedFixedOutgoing);
        }
    }

    private void setupNoExpandableRowViews(FixedOutgoingsViewHolder holder, FixedOutgoing fixedOutgoing) {
        adjustNoExpandableRowLayout(holder);
        int textColor = getTextColor(fixedOutgoing.getIsPaid(), fixedOutgoing.getDueDate());
        setupCheckBox(holder.paid, fixedOutgoing.getIsPaid(), getPaidClick(fixedOutgoing));
        setupTextView(holder.paidStatus, getPaidStatusText(fixedOutgoing.getIsPaid()), textColor, getPaymentDateClick(fixedOutgoing));
        setupTextView(holder.paymentDate, getPaymentDateText(fixedOutgoing.getPaymentDate()), textColor, getPaymentDateClick(fixedOutgoing));
        setupTextView(holder.dueDate, getDueDateText(fixedOutgoing.getDueDate()), textColor, getDueDateClick(fixedOutgoing));
        setupTextView(holder.dueDateTitle, null, textColor, getDueDateClick(fixedOutgoing));
        setupTextView(holder.outcome, amountFormatter.format(fixedOutgoing.getOutcome()), null, getOutcomeClick(fixedOutgoing));
        setupView(holder.listRow, getBackgroundColor(fixedOutgoing.getIsPaid(), fixedOutgoing.getDueDate()), null);
    }

    private void setupExpandableRowViews(FixedOutgoingsViewHolder holder, CategorizedFixedOutgoing categorizedFixedOutgoing) {
        boolean allPaid = categorizedFixedOutgoing.getCountOfPaidFixedOutgoings() == categorizedFixedOutgoing.getFixedOutgoingsListSize();
        Calendar closestDueDate = getClosestDueDateUnpaidFO(categorizedFixedOutgoing);
        int textColor = getTextColor(allPaid, closestDueDate);
        setupView(holder.listRow, getBackgroundColor(allPaid, closestDueDate), getOpenCloseExpandingRowsClick(holder));
        setupCheckBox(holder.paid, allPaid, null);
        setupTextView(holder.paidStatus, getPaidStatusText(allPaid), textColor, null);
        if (!allPaid) {
            holder.datesGroup.setVisibility(VISIBLE);
            setupTextView(holder.dueDateTitle, null, textColor, null);
            setupTextView(holder.dueDate, getDueDateText(closestDueDate), textColor, null);
        } else {
            holder.datesGroup.setVisibility(GONE);
        }
        setupTextView(holder.counterSplit, null, textColor, null);
        setupTextView(holder.countOfFO, String.valueOf(categorizedFixedOutgoing.getFixedOutgoingsListSize()), textColor, null);
        setupTextView(holder.countOfPaidFO, String.valueOf(categorizedFixedOutgoing.getCountOfPaidFixedOutgoings()), textColor, null);
        setupTextView(holder.outcome, amountFormatter.format(categorizedFixedOutgoing.getSum()), null, null);
        adjustExpandableRowLayout(holder);
    }

    private void adjustExpandableRowLayout(FixedOutgoingsViewHolder holder) {
        holder.counterOfFOPaidGroup.setVisibility(View.VISIBLE);
        closeExpandingRows(holder);
    }

    private void adjustNoExpandableRowLayout(FixedOutgoingsViewHolder holder) {
        holder.counterOfFOPaidGroup.setVisibility(GONE);
        holder.openExpandingRows.setVisibility(GONE);
        holder.closeExpandingRows.setVisibility(GONE);
    }

    private void createExpandingRows(FixedOutgoingsViewHolder holder, final CategorizedFixedOutgoing categorizedFixedOutgoing) {
        holder.expandingRows.removeAllViews();
        for (int i = 0; i < categorizedFixedOutgoing.getFixedOutgoingsListSize(); i++) {
            FixedOutgoing fixedOutgoing = categorizedFixedOutgoing.get(i);
            int textColor = getTextColor(fixedOutgoing.getIsPaid(), fixedOutgoing.getDueDate());
            View childView = layoutInflater.inflate(R.layout.list_fixed_outgoing_row, (ViewGroup) holder.itemView, false);
            setupView(childView, getBackgroundColor(fixedOutgoing.getIsPaid(), fixedOutgoing.getDueDate()), null);
            setupCheckBox((CheckBox) childView.findViewById(R.id.paid_check), fixedOutgoing.getIsPaid(), getPaidClick(fixedOutgoing));
            setupTextView((TextView) childView.findViewById(R.id.paid_status), getPaidStatusText(fixedOutgoing.getIsPaid()), textColor, getPaymentDateClick(fixedOutgoing));
            setupTextView((TextView) childView.findViewById(R.id.payment_date), getPaymentDateText(fixedOutgoing.getPaymentDate()), textColor, getPaymentDateClick(fixedOutgoing));
            setupTextView((TextView) childView.findViewById(R.id.due_date), getDueDateText(fixedOutgoing.getDueDate()), textColor, getDueDateClick(fixedOutgoing));
            setupTextView((TextView) childView.findViewById(R.id.due_date_title), null, textColor, getDueDateClick(fixedOutgoing));
            setupTextView((TextView) childView.findViewById(R.id.outcome_edit), amountFormatter.format(fixedOutgoing.getOutcome()), textColor, getOutcomeClick(fixedOutgoing));
            holder.expandingRows.addView(childView);
        }
    }

    private void closeExpandingRows(FixedOutgoingsViewHolder holder) {
        holder.closeExpandingRows.setVisibility(GONE);
        holder.openExpandingRows.setVisibility(View.VISIBLE);
        holder.expandingRows.setVisibility(GONE);
    }

    private void openExpandingRows(FixedOutgoingsViewHolder holder) {
        holder.closeExpandingRows.setVisibility(View.VISIBLE);
        holder.openExpandingRows.setVisibility(View.GONE);
        holder.expandingRows.setVisibility(View.VISIBLE);
    }

    private void setupTextView(TextView view, String text, Integer textColor, View.OnClickListener onClickListener) {
        if (text != null) {
            view.setText(text);
        }
        if (onClickListener != null) {
            view.setOnClickListener(onClickListener);
        }
        if (textColor != null) {
            view.setTextColor(textColor);
        }
    }

    private void setupView(View view, Integer backgroundColor, View.OnClickListener onClickListener) {
        if (onClickListener != null) {
            view.setOnClickListener(onClickListener);
        }
        if (backgroundColor != null) {
            view.setBackgroundColor(backgroundColor);
        }
    }

    private void setupCheckBox(CheckBox checkBox, boolean isChecked, View.OnClickListener onClickListener) {
        if (onClickListener != null) {
            checkBox.setOnClickListener(onClickListener);
        } else {
            checkBox.setClickable(false);
        }
        checkBox.setChecked(isChecked);
    }

    private String getDueDateText(Calendar dueDate) {
        if (dueDate != null) {
            return dateFormatter.formatToString(dueDate);
        } else {
            return noDate;
        }
    }

    private String getPaymentDateText(Calendar paymentDate) {
        if (paymentDate != null) {
            return dateFormatter.formatToString(paymentDate);
        } else {
            return "";
        }
    }

    private String getPaidStatusText(boolean isPaid) {
        if (isPaid) {
            return paid;
        } else {
            return unPaid;
        }
    }

    private Calendar getClosestDueDateUnpaidFO(CategorizedFixedOutgoing categorizedFixedOutgoing) {
        for (int i = 0; i < categorizedFixedOutgoing.getFixedOutgoingsListSize(); i++) {
            if (!categorizedFixedOutgoing.get(i).getIsPaid()) {
                return categorizedFixedOutgoing.get(i).getDueDate();
            }
        }
        return null;
    }

    private int getTextColor(Boolean isPaid, Calendar dueDate) {
        if (isPaid) {
            return activeColorId;
        } else if (!isPaymentDateBeforeToday(dueDate)) {
            return noActiveColorId;
        } else {
            TypedValue typedValue = new TypedValue();
            theme.resolveAttribute(android.R.attr.textColorPrimary, typedValue, true);
            return typedValue.data;
        }
    }

    private int getBackgroundColor(Boolean isPaid, Calendar dueDate) {
        if (!isPaid && isPaymentDateBeforeToday(dueDate)) {
            return minusColorId;
        } else {
            return Color.TRANSPARENT;
        }
    }

    private Boolean isPaymentDateBeforeToday(Calendar date) {
        if (date != null) {
            Calendar today = Calendar.getInstance();
            return date.before(today) || date.equals(today);
        }
        return false;
    }

    protected abstract View.OnClickListener getDueDateClick(final FixedOutgoing fixedOutgoing);

    protected abstract View.OnClickListener getPaymentDateClick(final FixedOutgoing fixedOutgoing);

    protected abstract View.OnClickListener getPaidClick(final FixedOutgoing fixedOutgoing);

    protected abstract View.OnClickListener getOutcomeClick(final FixedOutgoing fixedOutgoing);

    private View.OnClickListener getOpenCloseExpandingRowsClick(final FixedOutgoingsViewHolder holder) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.openExpandingRows.getVisibility() == VISIBLE) {
                    openExpandingRows(holder);
                } else {
                    closeExpandingRows(holder);
                }
            }
        };
    }

    @Override
    public int getItemCount() {
        return fixedOutgoingsList.size();
    }

    class FixedOutgoingsViewHolder extends RecyclerView.ViewHolder {

        LinearLayout expandingRows;
        ConstraintLayout listRow;
        TextView name, outcome, countOfPaidFO, counterSplit, countOfFO, paidStatus, dueDateTitle, paymentDate, dueDate;
        CheckBox paid;
        Group counterOfFOPaidGroup, datesGroup;
        ImageView closeExpandingRows, openExpandingRows;

        FixedOutgoingsViewHolder(View view) {
            super(view);
            listRow = view.findViewById(R.id.fixed_outgoing_list_row);
            expandingRows = view.findViewById(R.id.expanding_rows);
            name = view.findViewById(R.id.name_fixed_outgoing);
            outcome = view.findViewById(R.id.outcome_edit);
            paid = view.findViewById(R.id.paid_check);
            paidStatus = view.findViewById(R.id.paid_status);
            dueDate = view.findViewById(R.id.due_date);
            dueDateTitle = view.findViewById(R.id.due_date_title);
            paymentDate = view.findViewById(R.id.payment_date);
            countOfPaidFO = view.findViewById(R.id.count_of_paid_fixed_outgoings);
            counterSplit = view.findViewById(R.id.split);
            countOfFO = view.findViewById(R.id.count_of_fixed_outgoings);
            closeExpandingRows = view.findViewById(R.id.close_expanding_rows);
            openExpandingRows = view.findViewById(R.id.open_expanding_rows);
            counterOfFOPaidGroup = view.findViewById(R.id.counter_of_fo_paid_group);
            datesGroup = view.findViewById(R.id.dates_group);
        }
    }
}