package com.katarzynakrzemien.finance.fixedoutgoings.reminder;

import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.katarzynakrzemien.finance.utils.formatter.date.SimpleDateFormatter;
import com.katarzynakrzemien.finance.utils.formatter.date.DateFormatPattern;
import com.katarzynakrzemien.finance.utils.formatter.date.DateFormatter;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class FixedOutgoingsWorkerManager {

    static final String ID = "id";
    static final String TITLE = "title";
    static final String DATE = "date";
    static final String QUOTA = "quota";
    private static final FixedOutgoingsWorkerManager ourInstance = new FixedOutgoingsWorkerManager();
    private DateFormatter inputDateNotificationFormatter = new SimpleDateFormatter(DateFormatPattern.DAY_MONTH_YEAR);


    private FixedOutgoingsWorkerManager() {
    }

    public static FixedOutgoingsWorkerManager getInstance() {
        return ourInstance;
    }

    public void create(int id, Calendar date, Calendar time, String title, String quota) {

        Data inputData = new Data.Builder().putInt(ID, id).
                putString(TITLE, title).
                putString(DATE, inputDateNotificationFormatter.formatToString(date)).
                putString(QUOTA, quota).
                build();

        OneTimeWorkRequest notificationWork = new OneTimeWorkRequest.Builder(FixedOutgoingsWorker.class)
                .setInitialDelay(createDateTimeCalendar(date, time).getTimeInMillis() - Calendar.getInstance().getTimeInMillis(), TimeUnit.MILLISECONDS)
                .setInputData(inputData)
                .addTag(String.valueOf(id))
                .build();
        WorkManager.getInstance().enqueue(notificationWork);

    }

    public void cancel(int id) {
        WorkManager.getInstance().cancelAllWorkByTag(String.valueOf(id));
    }

    public void cancelAll() {
        WorkManager.getInstance().cancelAllWork();
    }

    private Calendar createDateTimeCalendar(Calendar date, Calendar time) {
        date.set(Calendar.HOUR, time.get(Calendar.HOUR));
        date.set(Calendar.MINUTE, time.get(Calendar.MINUTE));
        date.set(Calendar.SECOND, time.get(Calendar.SECOND));
        return date;
    }

}

