package com.katarzynakrzemien.finance.fixedoutgoings;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.katarzynakrzemien.finance.App;
import com.katarzynakrzemien.finance.R;
import com.katarzynakrzemien.finance.database.DatabaseQuery;
import com.katarzynakrzemien.finance.database.DatabaseStore;
import com.katarzynakrzemien.finance.database.PreferenceFromDatabase;
import com.katarzynakrzemien.finance.database.PreferenceStore;
import com.katarzynakrzemien.finance.database.models.CategorizedFixedOutgoing;
import com.katarzynakrzemien.finance.database.models.FixedOutgoing;
import com.katarzynakrzemien.finance.dialog.DialogDatePickerCreator;
import com.katarzynakrzemien.finance.dialog.DialogQuotaCreator;
import com.katarzynakrzemien.finance.dialog.DialogReaction;
import com.katarzynakrzemien.finance.fixedoutgoings.reminder.FixedOutgoingsWorkerManager;
import com.katarzynakrzemien.finance.managing.ManagingActivity;
import com.katarzynakrzemien.finance.managing.action.ActionAppListener;
import com.katarzynakrzemien.finance.managing.action.Clicked;
import com.katarzynakrzemien.finance.utils.Keyboard;
import com.katarzynakrzemien.finance.utils.formatter.amount.AmountFormatter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

public class FixedOutgoingsFragment extends Fragment implements FixedOutgoingsContract.View, FixedOutgoingsItemClickListener {

    public static final String TAG = "FIXEDOUTGOINGS";

    private List<CategorizedFixedOutgoing> fixedOutgoingsList = new ArrayList<>();
    private FixedOutgoingsContract.Presenter presenter = new FixedOutgoingsPresenter();
    private DatabaseStore databaseStore;
    private PreferenceStore preferenceStore;
    private FixedOutgoingsWorkerManager fixedOutgoingsWorkerManager = FixedOutgoingsWorkerManager.getInstance();
    private FixedOutgoingsAdapter adapter;
    private AmountFormatter amountFormatter;
    private View view;
    private Boolean remindFixedOutgoings;
    private Calendar timeRemindFixedOutgoings;
    private ActionAppListener actionAppListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        amountFormatter = ((App) context.getApplicationContext()).amountFormatter;
        databaseStore = new DatabaseQuery(context);
        preferenceStore = new PreferenceFromDatabase(context);
        adapter = new FixedOutgoingsAdapter(fixedOutgoingsList, context, this);
        actionAppListener = ((ManagingActivity) context);
        presenter.attachView(this);
        presenter.fetchRemindFixedOutgoings(preferenceStore);
        presenter.fetchTimeRemindFixedOutgoings(preferenceStore);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fixed_outgoings, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        view = getView();
        if (view != null) {
            openCategorizedFixedOutgoingsFragmentWhenNoActiveFixedOutgoings();
            RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
            recyclerView.setAdapter(adapter);
            presenter.fetchList(databaseStore);
            renderSummary();
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.animate();
        }
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }


    @Override
    public void renderList(List<CategorizedFixedOutgoing> list) {
        fixedOutgoingsList.clear();
        fixedOutgoingsList.addAll(list);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void setRemindFixedOutgoings(boolean remindFixedOutgoings) {
        this.remindFixedOutgoings = remindFixedOutgoings;
    }

    @Override
    public void setTimeRemindFixedOutgoings(Calendar timeRemindFixedOutgoings) {
        this.timeRemindFixedOutgoings = timeRemindFixedOutgoings;
    }

    @Override
    public void setIsAnyActiveFixedOutgoing(boolean isAnyActiveFixedOutgoing) {
        if (!isAnyActiveFixedOutgoing) {
            Snackbar.make(view, getString(R.string.pickFixedOutgoings), Snackbar.LENGTH_LONG).show();
            actionAppListener.itemClicked(Clicked.FIXED_OUTGOINGS);
        }
    }

    @Override
    public void renderPaidSum(double paidSum) {
        ((TextView) view.findViewById(R.id.paid_sum)).setText(amountFormatter.format(paidSum));

    }

    @Override
    public void renderUnpaidSum(double unpaidSum) {
        ((TextView) view.findViewById(R.id.unpaid_sum)).setText(amountFormatter.format(unpaidSum));

    }

    private void renderSummary() {
        presenter.fetchPaidSum(databaseStore);
        presenter.fetchUnpaidSum(databaseStore);
    }

    @Override
    public void onOutcomeClick(View view, final FixedOutgoing fixedOutgoing) {
        final DialogQuotaCreator dialogQuotaCreator = new DialogQuotaCreator(getContext(), fixedOutgoing.getName());
        dialogQuotaCreator.setPositiveButton(new DialogReaction() {
            @Override
            public void onClick() {
                String outcomeText = Objects.requireNonNull(dialogQuotaCreator.getDialogEditText().getText()).toString();
                if (outcomeText.length() > 0) {
                    double outcome = Double.parseDouble(outcomeText);
                    if (outcome > 0.0) {
                        fixedOutgoing.setOutcome(outcome);
                        presenter.updateItemList(databaseStore, fixedOutgoing);
                        presenter.fetchList(databaseStore);
                        renderSummary();
                        actionAppListener.fundsChange();
                    }
                }
                Keyboard.hide(getContext());
            }

        });
        dialogQuotaCreator.showDialog();
        Keyboard.show(getContext());
    }

    @Override
    public void onDueDateClick(View view, final FixedOutgoing fixedOutgoing) {
        final DialogDatePickerCreator dialogDatePickerCreator = new DialogDatePickerCreator(getContext(), getString(R.string.pickDueDate) + " " + fixedOutgoing.getName());
        dialogDatePickerCreator.setPositiveButton(new DialogReaction() {
            @Override
            public void onClick() {
                Calendar dueDate = dialogDatePickerCreator.getDate();
                updateDueDate(dueDate, fixedOutgoing);
                if (remindFixedOutgoings && !fixedOutgoing.getIsPaid()) {
                    fixedOutgoingsWorkerManager.create(fixedOutgoing.getId(),
                            dueDate, timeRemindFixedOutgoings,
                            fixedOutgoing.getName(),
                            amountFormatter.format(fixedOutgoing.getOutcome()));
                }
            }
        });
        dialogDatePickerCreator.setNeutralButton(R.string.noDate, new DialogReaction() {
            @Override
            public void onClick() {
                updateDueDate(null, fixedOutgoing);
                fixedOutgoingsWorkerManager.cancel(fixedOutgoing.getId());
            }
        });
        dialogDatePickerCreator.showDialog();
    }

    private void updateDueDate(Calendar paymentDate, FixedOutgoing fixedOutgoing) {
        fixedOutgoing.setDueDate(paymentDate);
        presenter.updateItemList(databaseStore, fixedOutgoing);
        presenter.fetchList(databaseStore);
    }

    @Override
    public void onPaymentDateClick(View view, final FixedOutgoing fixedOutgoing) {
        createAndShowPaymentDateDialog(fixedOutgoing);
    }

    private void createAndShowPaymentDateDialog(final FixedOutgoing fixedOutgoing) {
        final DialogDatePickerCreator dialogDatePickerCreator = new DialogDatePickerCreator(getContext(), getString(R.string.pickPaymentDate) + " " + fixedOutgoing.getName());
        dialogDatePickerCreator.setPositiveButton(new DialogReaction() {
            @Override
            public void onClick() {
                Calendar paymentDate = dialogDatePickerCreator.getDate();
                fixedOutgoing.setPaymentDate(paymentDate);
                presenter.updateItemList(databaseStore, fixedOutgoing);
                presenter.fetchList(databaseStore);
            }
        });
        dialogDatePickerCreator.showDialog();
    }

    @Override
    public void onPayClick(View view, final FixedOutgoing fixedOutgoing) {
        fixedOutgoing.setIsPaid(!fixedOutgoing.getIsPaid());
        if (fixedOutgoing.getIsPaid()) {
            createAndShowPaymentDateDialog(fixedOutgoing);
            if (remindFixedOutgoings) {
                fixedOutgoingsWorkerManager.cancel(fixedOutgoing.getId());
            }
        } else {
            fixedOutgoing.setPaymentDate(null);
            if (remindFixedOutgoings) {
                if (!(fixedOutgoing.getDueDate() == null)) {
                    fixedOutgoingsWorkerManager.create(fixedOutgoing.getId(),
                            fixedOutgoing.getDueDate(), timeRemindFixedOutgoings,
                            fixedOutgoing.getName(),
                            amountFormatter.format(fixedOutgoing.getOutcome()));
                }
            }
        }
        presenter.updateItemList(databaseStore, fixedOutgoing);
        presenter.fetchList(databaseStore);
        renderSummary();
        actionAppListener.fundsChange();
    }

    private void openCategorizedFixedOutgoingsFragmentWhenNoActiveFixedOutgoings() {
        presenter.isAnyActiveFixedOutgoing(databaseStore);
    }
}
